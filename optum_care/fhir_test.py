test = {
  "resourceType": "Bundle",
  "type": "searchset",
  "total": 6,
  "link": [
    {
      "relation": "self",
      "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
    }
  ],
  "entry": [
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/T1kK.xqvU20cEJe860G4aKgB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/T1kK.xqvU20cEJe860G4aKgB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Condition",
        "dateRecorded": "2015-08-24",
        "clinicalStatus": "active",
        "onsetDateTime": "2015-08-24",
        "verificationStatus": "confirmed",
        "id": "T1kK.xqvU20cEJe860G4aKgB",
        "patient": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "asserter": {
          "display": "MOORE, NICK",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
        },
        "code": {
          "text": "Agoraphobia",
          "coding": [
            {
              "system": "urn:oid:2.16.840.1.113883.6.90",
              "code": "F40.00",
              "display": "Agoraphobia"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "70691001",
              "display": "Agoraphobia (disorder)"
            }
          ]
        },
        "category": {
          "text": "Diagnosis",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "29308-4",
              "display": "Diagnosis"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "439401001",
              "display": "Diagnosis"
            },
            {
              "system": "http://hl7.org/fhir/condition-category",
              "code": "diagnosis",
              "display": "Diagnosis"
            },
            {
              "system": "http://argonautwiki.hl7.org/extension-codes",
              "code": "problem",
              "display": "Problem"
            }
          ]
        },
        "severity": {
          "text": "Medium"
        }
      }
    },
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/ToXGIl7BNrvoF6BVybVSoawB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/ToXGIl7BNrvoF6BVybVSoawB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Condition",
        "dateRecorded": "2015-08-24",
        "clinicalStatus": "active",
        "onsetDateTime": "2015-08-24",
        "verificationStatus": "confirmed",
        "id": "ToXGIl7BNrvoF6BVybVSoawB",
        "patient": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "asserter": {
          "display": "MOORE, NICK",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
        },
        "code": {
          "text": "Chronic cough",
          "coding": [
            {
              "system": "urn:oid:2.16.840.1.113883.6.90",
              "code": "R05",
              "display": "Chronic cough"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "68154008",
              "display": "Chronic cough (finding)"
            }
          ]
        },
        "category": {
          "text": "Diagnosis",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "29308-4",
              "display": "Diagnosis"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "439401001",
              "display": "Diagnosis"
            },
            {
              "system": "http://hl7.org/fhir/condition-category",
              "code": "diagnosis",
              "display": "Diagnosis"
            },
            {
              "system": "http://argonautwiki.hl7.org/extension-codes",
              "code": "problem",
              "display": "Problem"
            }
          ]
        },
        "severity": {
          "text": "Medium"
        }
      }
    },
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/T14QqO8NyASby4jGtzuSA6gB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/T14QqO8NyASby4jGtzuSA6gB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Condition",
        "dateRecorded": "2015-08-24",
        "clinicalStatus": "active",
        "onsetDateTime": "2015-08-24",
        "verificationStatus": "confirmed",
        "id": "T14QqO8NyASby4jGtzuSA6gB",
        "patient": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "asserter": {
          "display": "MOORE, NICK",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
        },
        "code": {
          "text": "Asthma",
          "coding": [
            {
              "system": "urn:oid:2.16.840.1.113883.6.90",
              "code": "J45.909",
              "display": "Asthma"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "195967001",
              "display": "Asthma (disorder)"
            }
          ]
        },
        "category": {
          "text": "Diagnosis - Health Concern",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "29308-4",
              "display": "Diagnosis"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "439401001",
              "display": "Diagnosis"
            },
            {
              "system": "http://hl7.org/fhir/condition-category",
              "code": "diagnosis",
              "display": "Diagnosis"
            },
            {
              "system": "http://argonautwiki.hl7.org/extension-codes",
              "code": "problem",
              "display": "Problem"
            },
            {
              "system": "http://loinc.org",
              "code": "75310-3",
              "display": "Health concerns"
            },
            {
              "system": "http://argonautwiki.hl7.org/extension-codes",
              "code": "health-concern",
              "display": "Health Concern"
            }
          ]
        },
        "severity": {
          "text": "Low"
        }
      }
    },
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/TOVSAAQTrMr9e45nCzc8S5wB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/TOVSAAQTrMr9e45nCzc8S5wB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Condition",
        "dateRecorded": "2016-02-11",
        "clinicalStatus": "active",
        "onsetDateTime": "2016-02-11",
        "verificationStatus": "confirmed",
        "id": "TOVSAAQTrMr9e45nCzc8S5wB",
        "patient": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "asserter": {
          "display": "MOORE, NICK",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
        },
        "code": {
          "text": "Hemoglobin A1c above reference range",
          "coding": [
            {
              "system": "urn:oid:2.16.840.1.113883.6.90",
              "code": "R79.89",
              "display": "Hemoglobin A1c above reference range"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "444751005",
              "display": "Hemoglobin A1c above reference range (finding)"
            }
          ]
        },
        "category": {
          "text": "Diagnosis",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "29308-4",
              "display": "Diagnosis"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "439401001",
              "display": "Diagnosis"
            },
            {
              "system": "http://hl7.org/fhir/condition-category",
              "code": "diagnosis",
              "display": "Diagnosis"
            },
            {
              "system": "http://argonautwiki.hl7.org/extension-codes",
              "code": "problem",
              "display": "Problem"
            }
          ]
        }
      }
    },
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/TajPohVuPYfDdU-wGcm.KMgB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/TajPohVuPYfDdU-wGcm.KMgB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Condition",
        "dateRecorded": "2016-04-23",
        "clinicalStatus": "active",
        "onsetDateTime": "2016-04-23",
        "verificationStatus": "confirmed",
        "id": "TajPohVuPYfDdU-wGcm.KMgB",
        "patient": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "asserter": {
          "display": "MOORE, NICK",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
        },
        "code": {
          "text": "TB (pulmonary tuberculosis)",
          "coding": [
            {
              "system": "urn:oid:2.16.840.1.113883.6.90",
              "code": "A15.0",
              "display": "TB (pulmonary tuberculosis)"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "154283005",
              "display": "Pulmonary tuberculosis (disorder)"
            }
          ]
        },
        "category": {
          "text": "Diagnosis",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "29308-4",
              "display": "Diagnosis"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "439401001",
              "display": "Diagnosis"
            },
            {
              "system": "http://hl7.org/fhir/condition-category",
              "code": "diagnosis",
              "display": "Diagnosis"
            },
            {
              "system": "http://argonautwiki.hl7.org/extension-codes",
              "code": "problem",
              "display": "Problem"
            }
          ]
        }
      }
    },
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/T-HzNQh62IyOLOuPFOaTtDgB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition/T-HzNQh62IyOLOuPFOaTtDgB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Condition",
        "dateRecorded": "2016-04-23",
        "clinicalStatus": "active",
        "onsetDateTime": "2016-04-23",
        "verificationStatus": "confirmed",
        "id": "T-HzNQh62IyOLOuPFOaTtDgB",
        "patient": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "asserter": {
          "display": "MOORE, NICK",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
        },
        "code": {
          "text": "Zika virus disease",
          "coding": [
            {
              "system": "urn:oid:2.16.840.1.113883.6.90",
              "code": "A92.8",
              "display": "Zika virus disease"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "3928002",
              "display": "Zika virus disease (disorder)"
            }
          ]
        },
        "category": {
          "text": "Diagnosis",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "29308-4",
              "display": "Diagnosis"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "439401001",
              "display": "Diagnosis"
            },
            {
              "system": "http://hl7.org/fhir/condition-category",
              "code": "diagnosis",
              "display": "Diagnosis"
            },
            {
              "system": "http://argonautwiki.hl7.org/extension-codes",
              "code": "problem",
              "display": "Problem"
            }
          ]
        }
      }
    }
  ]
}

test = {
  "resourceType": "Bundle",
  "type": "searchset",
  "total": 3,
  "link": [
    {
      "relation": "self",
      "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB&code=8310-5"
    }
  ],
  "entry": [
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation/Tnf7t0.SP6znu2Dc1kPsron.8Qlu-yjOF792bUBX3SIbqfiRJTmZfK.seS16W01szB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation/Tnf7t0.SP6znu2Dc1kPsron.8Qlu-yjOF792bUBX3SIbqfiRJTmZfK.seS16W01szB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Observation",
        "effectiveDateTime": "2016-04-19T15:02:00Z",
        "status": "final",
        "id": "Tnf7t0.SP6znu2Dc1kPsron.8Qlu-yjOF792bUBX3SIbqfiRJTmZfK.seS16W01szB",
        "code": {
          "text": "Temp",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "8310-5",
              "display": "Body temperature"
            },
            {
              "system": "http://loinc.org",
              "code": "8716-3",
              "display": "Vital Signs grouping"
            }
          ]
        },
        "valueQuantity": {
          "value": 37,
          "unit": "Cel",
          "code": "Cel",
          "system": "http://unitsofmeasure.org"
        },
        "subject": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "performer": [
          {
            "display": "MOORE, NICK",
            "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
          }
        ],
        "category": {
          "text": "Vital Signs",
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "vital-signs",
              "display": "Vital Signs"
            }
          ]
        }
      }
    },
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation/Tnf7t0.SP6znu2Dc1kPsroonALFGbtOt6b2yu6wZ4ivT3-rV2HHjSYO76FPbBHWwbB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation/Tnf7t0.SP6znu2Dc1kPsroonALFGbtOt6b2yu6wZ4ivT3-rV2HHjSYO76FPbBHWwbB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Observation",
        "effectiveDateTime": "2015-08-26T22:00:00Z",
        "status": "final",
        "id": "Tnf7t0.SP6znu2Dc1kPsroonALFGbtOt6b2yu6wZ4ivT3-rV2HHjSYO76FPbBHWwbB",
        "code": {
          "text": "Temp",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "8310-5",
              "display": "Body temperature"
            },
            {
              "system": "http://loinc.org",
              "code": "8716-3",
              "display": "Vital Signs grouping"
            }
          ]
        },
        "valueQuantity": {
          "value": 36.7,
          "unit": "Cel",
          "code": "Cel",
          "system": "http://unitsofmeasure.org"
        },
        "subject": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "performer": [
          {
            "display": "MOORE, NICK",
            "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
          }
        ],
        "category": {
          "text": "Vital Signs",
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "vital-signs",
              "display": "Vital Signs"
            }
          ]
        }
      }
    },
    {
      "fullUrl": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation/Tnf7t0.SP6znu2Dc1kPsrog5qrfqH33vJQOxAByVedwC93TwE2NmYRNSmcPUkg7JsB",
      "link": [
        {
          "relation": "self",
          "url": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Observation/Tnf7t0.SP6znu2Dc1kPsrog5qrfqH33vJQOxAByVedwC93TwE2NmYRNSmcPUkg7JsB"
        }
      ],
      "search": {
        "mode": "match"
      },
      "resource": {
        "resourceType": "Observation",
        "effectiveDateTime": "2015-08-25T22:46:00Z",
        "status": "final",
        "id": "Tnf7t0.SP6znu2Dc1kPsrog5qrfqH33vJQOxAByVedwC93TwE2NmYRNSmcPUkg7JsB",
        "code": {
          "text": "Temp",
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "8310-5",
              "display": "Body temperature"
            },
            {
              "system": "http://loinc.org",
              "code": "8716-3",
              "display": "Vital Signs grouping"
            }
          ]
        },
        "valueQuantity": {
          "value": 36.7,
          "unit": "Cel",
          "code": "Cel",
          "system": "http://unitsofmeasure.org"
        },
        "subject": {
          "display": "Jason Argonaut",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
        },
        "performer": [
          {
            "display": "MOORE, NICK",
            "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/TItWfhjChtlo0pFh9nzctSQB"
          }
        ],
        "category": {
          "text": "Vital Signs",
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "vital-signs",
              "display": "Vital Signs"
            }
          ]
        }
      }
    }
  ]
}

values = test.get('entry', [])
for i in values:
    value = i.get('resource').get('code').get('coding')
    for x in value:
        print(x.get('code'))