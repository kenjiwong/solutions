'''
Author: Kenji Wong
Date: 8/30/2018
Purpose: Create testing script for Alliance
'''

import hmac
from Crypto.Hash import SHA256
import datetime
import requests
import time

public_key = 'e544be9e9ec949faa16ce27865d94956a4fe6ab1'
private_key = '9275c83ccd031490814d4b76badb4eb2621de83c'
orchestration_id = '7wJWAUtHkdkMpmbay2bCJvyQ'
execution_block_id = '6dCdTU2g9xMJSTHQqW4XxWNQ'
input_adapter_id = '3Qw8szshkWmrTPAytArU8X4Q'
output_adapter_id = '7vrav2bhjrfJ2UhF8QrXxdpQ'


# CH
# public_key = '03699da18dd0a41ff90e380f713ee0caefe91e51'
# private_key = '9275c83ccd031490814d4b76badb4eb2621de83c'
# orchestration_id = '5ZyjTtZ83uAbPWuJA7wCsP3Q'
# execution_block_id = '4HVVvDPCJCKUJBbXJhAjzW5Q'
# input_adapter_id = '3xKRmP3jzTTm5DJgMfUyG3qQ'
# output_adapter_id = '6dsQn9CHVNQMkPJSP2abs29Q'
# subjectid = 'ABC123'

class ApiClient(object):

    def __init__(self):
        # Initialize attributes on object
        self.api_key = public_key
        self.private_key = private_key
        self.http_method = 'https://'
        self.url = 'optumapi.apervita.net'
        # self.url = 'optum.apervita.net'

    def pathway_single_subject_calculator(self, json, orchestrationid, executionid, subjectid, inputadapterid, outputadapterid):
        # Designate POST parameters for signature
        rest_type = 'post'
        path = '/api/v2/pathway/json'

        # response = self.assemble_call(rest_type=rest_type, path=path, json=json, insightid=insightid, populationid=populationid)
        response = self.assemble_call(rest_type=rest_type, path=path, json=json, orchestrationid=orchestrationid, inputadapterid=inputadapterid, outputadapterid=outputadapterid,  already_ssjson=False, executionblockid=executionid, subjectid=subjectid)

        return response

    # def pathway_single_subject_calculator(self, json, insightid, inputadapterid):
    #     # Designate POST parameters for signature
    #     rest_type = 'post'
    #     path = '/api/v2/pathway/json'
    #
    #     response = self.assemble_call(rest_type=rest_type, path=path, json=json, inputadapterid=inputadapterid, insightid=insightid)
    #
    #     return response

    def single_subject_calculator(self, json, insightid):
        # Designate POST parameters for signature
        rest_type = 'post'
        path = '/api/v2/subject/calculate/json'

        response = self.assemble_call(rest_type=rest_type, path=path, json=json, insightid=insightid)

        return response

    def assemble_call(self, rest_type, path, file_source_path=None, json=None, **kwargs):
        # Get current time stamp
        current_timestamp = datetime.datetime.utcnow().replace(microsecond=0).isoformat()

        # Create base parameters in every call
        parameters = {'api_key': self.api_key, 'timestamp': current_timestamp}

        for key, value in kwargs.iteritems():
            parameters[key] = value

        response = self.make_call(rest_type, path, parameters, file_source_path, json)

        return response

    def make_call(self, rest_type, path, parameters, file_source_path=None, json=None):
        dashes = '-' * 150
        print(dashes)

        # Create signature
        url_parameters = self.add_signature(rest_type, path, parameters)

        # Create URL with parameters and signature
        url = self.create_url(path, url_parameters)

        # Make either GET or POST
        if rest_type == 'post':
            print('Method: POST')
            print('Final URL: {0}'.format(url))

            # Open up file and make post request
            if file_source_path:
                upload_file = open(file_source_path, 'rb')
                response = requests.post(url, files={'uploadfile': upload_file})
            elif json:
                response = requests.post(url, json=json, auth=('apervita.optum', 'Optum@ssistant'))
            else:
                response = requests.post(url)

            # print response
            print('Request Headers: ' + str(response.request.headers))
            print('Response: ' + response.text)
        elif rest_type == 'get':
            print('Method: GET')
            print('Final URL: {0}'.format(url))

            # Make get request
            response = requests.get(url)

            # Print response
            print('Request Headers: ' + str(response.request.headers))
            print('Response: ' + response.text)
        else:
            response = None
            print('Unknown method')

        print(dashes)

        return response

    def add_signature(self, rest_type, path, parameters):
        # Create parameters list in alphabetical order
        parameters_list = ['{0}={1}'.format(key, parameters[key]) for key in sorted(parameters.keys())]

        # Join parameters and rest of the URL
        signature_list = [rest_type, self.url, path, '&'.join(parameters_list)]

        # Join by pipes and lowercase inputs
        prep_sign = '|'.join(signature_list).lower()
        print('Prepare Signature: {0}'.format(prep_sign))

        # Create signature SHA-256
        signature = hmac.new(self.private_key, prep_sign, SHA256).hexdigest()

        # Add signature to dict
        parameters['signature'] = signature

        return parameters

    def create_url(self, path, parameters):
        # Sort parameters
        parameters_list = ['{0}={1}'.format(key, parameters[key]) for key in sorted(parameters.keys())]

        # Join with the rest of parameters
        url_list = [self.http_method, self.url, path+'?', '&'.join(parameters_list)]

        # Create URL
        url = ''.join(url_list)


        return url

# test_json = {
#             'questionnaire_name': 'StressTestForStableCAD',
#             'known_answers':{
#                'StressTestForStableCAD.HistoryOfCAD': 'Yes',
#                'StressTestForStableCAD.HistoryOfCAD.Yes.CardiacSymptoms':'Yes',
#                 "StressTestForStableCAD.AbnormalBaselineEKG.No.FunctionalCapacity": "No",
#                 "StressTestForStableCAD.AbnormalBaselineEKG": "No"
#
#             }
# }
#
# test_json = {
#     'questionnaire_name': 'StressTestForStableCAD',
#     'known_answers': {
#                       },
# }

test_json = {
            'questionnaire_name': 'CardiologyPalpitationsReferrals',
            'known_answers':{
                "CardiologyPalpitationsReferrals.Palpitations": "Yes",
                "CardiologyPalpitationsReferrals.Palpitations.Yes.RuledOutMetabolicCauses": "Yes",
                "CardiologyPalpitationsReferrals.Palpitations.Yes.RuledOutMetabolicCauses.Yes.RiskFactorsPresent": "Yes",
                "CardiologyPalpitationsReferrals.Palpitations.Yes.RuledOutMetabolicCauses.Yes.RiskFactorsPresent.Yes.SuspectedHeartDisease": "Structural Heart Disease",
            }
}

subjectid = 'ABC124'

if __name__ == '__main__':
    # Initialize client
    test_client = ApiClient()
    start_time = time.time()
    # Make single subject call
    # test_client.pathway_single_subject_calculator(test_json, orchestration_id, execution_block_id, subjectid, input_adapter_id, output_adapter_id)
    test_client.pathway_single_subject_calculator(test_json, orchestration_id, execution_block_id, subjectid,
                                                  input_adapter_id, output_adapter_id)
    # test_client.pathway_single_subject_calculator(alliance_test_patient_3, cds_analytic_id, cds_input_adapter_id)
    print('Call Duration: {}'.format(time.time()-start_time))
