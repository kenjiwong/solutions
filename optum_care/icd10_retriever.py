from pymedtermino.icd10 import *

def get_children_specific(icd_list):
    output = []
    for code in icd_list:
        output.append(code)
        for item in ICD10[code].children:
            output.append(item.code)
    return output

def get_children_range(icd_list):
    output = []
    for code in icd_list:
        for item in ICD10[code].children:
            output.append(item.code)
            for sub_item in item.children:
                output.append(sub_item.code)
    return output

with open('output.txt', 'w') as f:
    result = get_children_specific(["R00",
                            "R01",
                            "R03",
                            "R04",
                            "R05",
                            "R06",
                            "R07",
                            "R09",])
    f.write('code\n')
    for i in result:
        f.write('{}\n'.format(i))

with open('output2.txt', 'w') as f:
    result = get_children_range(["R00-R09"])
    f.write('code\n')
    for i in result:
        f.write('{}\n'.format(i))
