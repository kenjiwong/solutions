from utils import _CCDUtils, IMMUNIZATION_TEMPLATE_IDS


class ImmunizationParser(_CCDUtils):
    section = 'Immunization'
    columns = [
        'event_class',
        'immunization_id',
        'mood_code',
        'negated',
        'start_date',
        'code',
        'code_system'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = IMMUNIZATION_TEMPLATE_IDS
        super(ImmunizationParser, self).__init__(ccd_tree, ns)

    def parse(self):
        immun_data = []
        for entry in self.iter_entries():

            substance_el = self.get_CDA_child(entry, 'substanceAdministration')
            mood_code = substance_el.attrib.get('moodCode', '')

            id = self.extract_id(self.get_CDA_child(substance_el, 'id'))

            negated = 'true' if (substance_el.attrib.get('inversionInd', 'false').lower() == 'true') else 'false'

            eff_time = self.get_CDA_child(substance_el, 'effectiveTime')
            start_date = self.get_range_low(eff_time)

            consumable_el = self.get_CDA_child(substance_el, 'consumable')
            product_el = self.get_CDA_child(consumable_el, 'manufacturedProduct')
            material_el = self.get_CDA_child(product_el, 'manufacturedMaterial')
            material_code_el = self.get_CDA_child(material_el, 'code')
            if not self.is_null(material_code_el):
                medication_code = material_code_el.attrib.get('code', '')
                medication_code_system = material_code_el.attrib.get('codeSystem', '')

            immun_data.append({
                'event_class': self.section,
                'immunization_id': id,
                'mood_code': mood_code,
                'negated': negated,
                'start_date': start_date,
                'code': medication_code,
                'code_system': medication_code_system
            })

        return immun_data
