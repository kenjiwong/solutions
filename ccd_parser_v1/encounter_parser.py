from utils import _CCDUtils, ENCOUNTER_TEMPLATE_IDS


class EncounterParser(_CCDUtils):
    section = 'Encounter'
    columns = [
        'event_class',
        'encounter_id',
        'mood_code',
        'start_date',
        'end_date',
        'code',
        'code_system',
        'discharge_disposition_code',
        'discharge_disposition_code_system'

    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = ENCOUNTER_TEMPLATE_IDS
        super(EncounterParser, self).__init__(ccd_tree, ns)

    def parse(self):
        encounter_data = []

        for entry in self.iter_entries():

            encounter_el = self.get_CDA_child(entry, 'encounter')
            mood_code = encounter_el.attrib.get('moodCode', '')

            id = self.extract_id(self.get_CDA_child(encounter_el, 'id'))

            eff_time = self.get_CDA_child(encounter_el, 'effectiveTime')
            start_date, end_date = self.get_date_range(eff_time)

            code_el = self.get_CDA_child(encounter_el, 'code')
            if not self.is_null(code_el):
                code = code_el.attrib.get('code', '')
                code_system = code_el.attrib.get('codeSystem', '')

            discharge_disposition_code = ''
            discharge_disposition_code_system = ''
            discharge_disposition_el = self.get_child(encounter_el, 'dischargeDispositionCode', self.ns['SDTC'])
            if not self.is_null(discharge_disposition_el):
                discharge_disposition_code = discharge_disposition_el.attrib.get('code', '')
                discharge_disposition_code_system = discharge_disposition_el.attrib.get('codeSystem', '')


            encounter_data.append({
                'event_class': self.section,
                'encounter_id': id,
                'mood_code': mood_code,
                'start_date': start_date,
                'end_date': end_date,
                'code': code,
                'code_system': code_system,
                'discharge_disposition_code': discharge_disposition_code,
                'discharge_disposition_code_system': discharge_disposition_code_system
            })

        return encounter_data
