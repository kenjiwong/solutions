import re

from utils import _CCDUtils


class PatientParser(_CCDUtils):
    section = 'Patient'
    columns = [
        'event_class',
        'address',
        'city',
        'state',
        'postal',
        'country',
        'first',
        'last',
        'date_of_birth',
        'gender_code',
        'gender_code_system',
        'race_code',
        'race_code_system',
        'ethnic_code',
        'ethnic_code_system'
    ]

    def get_root_el(self, el):
        target_el = self.get_CDA_child(el, 'recordTarget')
        return self.get_CDA_child(target_el, 'patientRole')

    def parse(self):

        patient_role_el = self.root_el

        patient_data = {}
        patient_data['event_class'] = self.section

        address = city = state = zip = country = ''
        addr_els = self.get_CDA_children(patient_role_el, 'addr')
        for addr_el in addr_els:
            address, city, state, zip, country = self.get_addr(addr_el)

        patient_el = self.get_CDA_child(patient_role_el, 'patient')

        date_of_birth_el = self.get_CDA_child(patient_el, 'birthTime')
        date_of_birth = '' if self.is_null(date_of_birth_el) else date_of_birth_el.attrib.get('value', '')

        gender_code = gender_code_system = ''
        race_code = race_code_system = ''
        ethnic_code = ethnic_code_system = ''

        gender_code_el = self.get_CDA_child(patient_el, 'administrativeGenderCode')
        if not self.is_null(gender_code_el):
            gender_code = gender_code_el.attrib.get('code', '')
            gender_code_system = gender_code_el.attrib.get('codeSystem', '')

        race_code_el = self.get_CDA_child(patient_el, 'raceCode')
        if not self.is_null(race_code_el):
            race_code = race_code_el.attrib.get('code', '')
            race_code_system = race_code_el.attrib.get('codeSystem', '')

        ethnic_code_el = self.get_CDA_child(patient_el, 'ethnicGroupCode')
        if not self.is_null(ethnic_code_el):
            ethnic_code = ethnic_code_el.attrib.get('code', '')
            ethnic_code_system = ethnic_code_el.attrib.get('codeSystem', '')

        given_name = ''
        family_name = ''

        name_el = self.get_CDA_child(patient_el, 'name')
        for given_name_el in self.get_CDA_child(name_el, 'given'):
            given_name += ' ' + given_name_el.text
        for family_name_el in self.get_CDA_child(name_el, 'family'):
            family_name += ' ' + family_name_el.text

        patient_data['address'] = address
        patient_data['city'] = city
        patient_data['state'] = state
        patient_data['postal'] = zip
        patient_data['country'] = country
        patient_data['first'] = given_name
        patient_data['last'] = family_name
        patient_data['date_of_birth'] = date_of_birth
        patient_data['gender_code'] = gender_code
        patient_data['gender_code_system'] = gender_code_system
        patient_data['race_code'] = race_code
        patient_data['race_code_system'] = race_code_system
        patient_data['ethnic_code'] = ethnic_code
        patient_data['ethnic_code_system'] = ethnic_code_system

        return [patient_data]
