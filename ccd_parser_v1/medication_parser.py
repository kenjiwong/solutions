from utils import _CCDUtils, MEDICATION_TEMPLATE_IDS


class MedicationParser(_CCDUtils):
    section = 'Medication'
    columns = [
        'event_class',
        'medication_id',
        'mood_code',
        'start_date',
        'end_date',
        'period_value',
        'period_unit',
        'event_code',
        'route_code',
        'route_code_system',
        'unit_code',
        'unit_code_system',
        'dose_quantity_min',
        'dose_quantity_max',
        'code',
        'code_system'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = MEDICATION_TEMPLATE_IDS
        super(MedicationParser, self).__init__(ccd_tree, ns)

    def parse(self):
        medication_data = []
        for entry in self.iter_entries():

            substance_el = self.get_CDA_child(entry, 'substanceAdministration')
            mood_code = substance_el.attrib.get('moodCode', '')

            id = self.extract_id(self.get_CDA_child(substance_el, 'id'))

            start_date = ''
            end_date = ''
            period_value = ''
            period_unit = ''
            event_code = ''
            route_code = ''
            route_code_system = ''
            unit_code = ''
            unit_code_system = ''
            dose_quantity_min = ''
            dose_quantity_max = ''
            medication_code = ''
            medication_code_system = ''

            # substanceAdministration uses combined intervals, so there
            # can be multiple effectiveTime Elements
            # http://www.cdapro.com/know/24997
            for eff_time in self.get_CDA_children(substance_el, 'effectiveTime'):

                # Note 'TS' default (http://www.cdapro.com/know/26722)
                interval_type = eff_time.attrib.get(self.ns_tag('type', self.ns['XSI']), 'TS')
                if interval_type == "TS":
                    pass
                elif interval_type == "IVL_TS":
                    start_date, end_date = self.get_date_range(eff_time)
                elif interval_type == "PIVL_TS":
                    period_el = self.get_CDA_child(eff_time, 'period')
                    if not self.is_null(period_el):
                        period_value = period_el.attrib.get('value', '')
                        period_unit = period_el.attrib.get('unit', '')
                elif interval_type == "EIVL_TS":
                    event_el = self.get_CDA_child(eff_time, 'event')
                    if not self.is_null(event_el):
                        event_code = period_el.attrib.get('code', '')


            route_code_el = self.get_CDA_child(substance_el, 'routeCode')
            if not self.is_null(route_code_el):
                route_code = route_code_el.attrib.get('code', '')
                route_code_system = route_code_el.attrib.get('codeSystem', '')

            unit_code_el = self.get_CDA_child(substance_el, 'administrationUnitCode')
            if not self.is_null(unit_code_el):
                unit_code = route_code_el.attrib.get('code', '')
                unit_code_system = unit_code.attrib.get('codeSystem', '')

            dose_quantity_el = self.get_CDA_child(substance_el, 'doseQuantity')
            dose_quantity_min = self.get_range_low(dose_quantity_el, True)
            dose_quantity_max = self.get_range_high(dose_quantity_el, True)

            consumable_el = self.get_CDA_child(substance_el, 'consumable')
            product_el = self.get_CDA_child(consumable_el, 'manufacturedProduct')
            material_el = self.get_CDA_child(product_el, 'manufacturedMaterial')
            material_code_el = self.get_CDA_child(material_el, 'code')
            if not self.is_null(material_code_el):
                medication_code = material_code_el.attrib.get('code', '')
                medication_code_system = material_code_el.attrib.get('codeSystem', '')

            medication_data.append({
                'event_class': self.section,
                'medication_id': id,
                'mood_code': mood_code,
                'start_date': start_date,
                'end_date': end_date,
                'period_value': period_value,
                'period_unit': period_unit,
                'event_code': event_code,
                'route_code': route_code,
                'route_code_system': route_code_system,
                'unit_code': unit_code,
                'unit_code_system': unit_code_system,
                'dose_quantity_min': dose_quantity_min,
                'dose_quantity_max': dose_quantity_max,
                'code': medication_code,
                'code_system': medication_code_system
            })

        return medication_data
