from utils import _CCDUtils, COGNITIVE_TEMPLATE_IDS


class CognitiveParser(_CCDUtils):
    section = 'Cognitive'
    columns = [
        'event_class',
        'code_code',
        'code_code_system',
        'code_code_system_name',
        'obs_mood_code',
        'status_code_code',
        'eff_time_low_el_text',
        'eff_time_high_el_text',
        'value_type',
        'value_value'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = COGNITIVE_TEMPLATE_IDS
        super(CognitiveParser, self).__init__(ccd_tree, ns)

    def parse(self):

        # Set list to store parsed data
        cogn_data = []

        # Iterate entry elements
        for entry_el in self.iter_entries():

            # Find entry attrs
            code_code = self.attr_val(entry_el, 'code', 'code')
            code_code_system = self.attr_val(entry_el, 'codeSystem', 'code')
            code_code_system_name = self.attr_val(entry_el, 'codeSystemName', 'code')
            obs_mood_code = self.attr_val(entry_el, 'moodCode', 'observation')
            status_code_code = self.attr_val(entry_el, 'code', 'statusCode')
            eff_time_low_el_text, eff_time_high_el_text = self.get_eff_time_low_high(entry_el)
            value_type = self.attr_val(entry_el, 'type', 'value', 'XSI')
            value_value = self.attr_val(entry_el, 'value', 'value')

            # Store entry attrs in dict
            entry_data = dict(
                event_class=self.section,
                code_code=code_code,
                code_code_system=code_code_system,
                code_code_system_name=code_code_system_name,
                obs_mood_code=obs_mood_code,
                status_code_code=status_code_code,
                eff_time_low_el_text=eff_time_low_el_text,
                eff_time_high_el_text=eff_time_high_el_text,
                value_type=value_type,
                value_value=value_value
            )

            # Add entry attrs dict to output data
            cogn_data.append(entry_data)

        return cogn_data
