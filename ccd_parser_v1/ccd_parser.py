import argparse
import csv
import datetime
import os
import time
import uuid
from io import StringIO
import pprint

from lxml import etree

from utils import _CCDUtils
# from allergy_parser import AllergyParser
from condition_parser import ConditionParser
# from cognitive_parser import CognitiveParser
from immunization_parser import ImmunizationParser
from medication_parser import MedicationParser
from observation_parser import ObservationParser
from procedure_parser import ProcedureParser
from payer_parser import PayerParser
from patient_parser import PatientParser
from encounter_parser import EncounterParser


class CCDParser(object):

    def __init__(self, parsers=None):

        # Set namespace
        self.ns = {'PD': 'urn:epic-com:Common.2013.Services', 'CDA': 'urn:hl7-org:v3',
                      'XSI': "http://www.w3.org/2001/XMLSchema-instance", 'SDTC': 'urn:hl7-org:sdtc'}

        # Set dict to reference available parsers
        available_section_parsers = {}
#        available_section_parsers[AllergyParser.section] = AllergyParser
        available_section_parsers[ConditionParser.section] = ConditionParser
#        available_section_parsers[CognitiveParser.section] = CognitiveParser
        available_section_parsers[ImmunizationParser.section] = ImmunizationParser
        available_section_parsers[MedicationParser.section] = MedicationParser
        available_section_parsers[ObservationParser.section] = ObservationParser
        available_section_parsers[ProcedureParser.section] = ProcedureParser
        available_section_parsers[PayerParser.section] = PayerParser
        available_section_parsers[PatientParser.section] = PatientParser
        available_section_parsers[EncounterParser.section] = EncounterParser

        # Set section parses to empty list
        if not parsers:
            parsers = ['Condition', 'Immunization', 'Medication', 'Observation', 'Procedure', 'Payer', 'Encounter', 'Patient']

        self.section_parsers = list()

        # Iterate list of parser names
        for parser in parsers:
            try:

                # Set instance parsers
                self.section_parsers.append(available_section_parsers[parser])

            except KeyError:

                # Raise info about invalid parser choice
                raise AttributeError(
                    "Invalid parser choice '{0}'. Available parsers include {1}.".format(parser, list(
                        available_section_parsers.keys())))

    def parse_dir(self, source, destination):

        # Start parse time
        start_parse = time.time()
        print('Beginning parse...')
        print('The {0} sections will be parsed.'.format(', '.join([parser.section for parser in self.section_parsers])))

        # Set list to append parsed ccd
        events = dict()

        try:
            source_dir = os.listdir(source)
        except OSError:
            print('Source directory {} not found.'.format(source))
            return

        print('{0} source files found.'.format(len(source_dir)))

        # First time through:
        append = False

        # Iterate files in source
        for filename in source_dir:

            # Process source xml
            append = self.parse_file(filename, destination, append)

        # Time of completion
        end_parse = time.time()

        print('Parse completed in {} seconds'.format(end_parse - start_parse))

        return events


    def parse_file(self, filename, output_folder, append):

        doc_data = []

        try:
            ccd_tree = etree.parse(os.path.join(source, filename)).getroot()
        except:
            print('{} could not be parsed. Continuing.'.format(filename))
            return append

        # Find clinical document element
        clinical_document_tree = self.get_clinical_document(ccd_tree)

        # Find the patient id
        patient_id_els = clinical_document_tree.xpath(
            'CDA:recordTarget/CDA:patientRole/CDA:id', namespaces=self.ns)

        # Only take SSN if no other id is provided
        patient_id = ''
        for patient_id_el in patient_id_els:
            if patient_id == '' or patient_id_el.attrib.get('root', '') != '2.16.840.1.113883.4.1':
                patient_id = patient_id_el.attrib.get('extension', '')

        # Create a unique id for this file
        submission_id = uuid.uuid4().hex

        # Iterate section parsers
        for parser in self.section_parsers:

            # Init parser section instance
            sec_parser = parser(clinical_document_tree, self.ns)

            # Don't bother with sections that have nullFlavor
            if 'nullFlavor' in sec_parser.root_el.attrib:
                continue

            # Do parse!
            section_data = sec_parser.parse()
            if section_data:
                doc_data.extend(section_data)

        # Clear folder
        if append is False:
            for file_name in os.listdir(output_folder):
                file_path = os.path.join(output_folder, file_name)
                if os.path.isfile(file_path) and file_path.endswith('.txt'):
                    os.remove(file_path)

        # Figure file open mode
        fileopenmode = 'w'
        if append is True:
            fileopenmode = 'a+'

        # Build set of event classes present in output
        eventClasses = set()
        for r in doc_data:
            eventClasses.add(r['event_class'])

        # Output eventClasses one by one
        for eventClass in eventClasses:

            # Get fieldnames array, which controls the order of columns in output,
            # based upon event class
            commonColumns = ['patient_id', 'submission_id']
            commonValues = {'patient_id': patient_id, 'submission_id': submission_id}
            fieldnames = []
            if eventClass == 'Patient':
                fieldnames = commonColumns + PatientParser.columns
            #elif eventClass == 'Allergy':
            #    fieldnames = commonColumns + AllergyParser.columns
            #elif eventClass == 'Cognitive':
            #    fieldnames = commonColumns + CognitiveParser.columns
            elif eventClass == 'Condition':
                fieldnames = commonColumns + ConditionParser.columns
            elif eventClass == 'Immunization':
                fieldnames = commonColumns + ImmunizationParser.columns
            elif eventClass == 'Medication':
                fieldnames = commonColumns + MedicationParser.columns
            elif eventClass == 'Observation':
                fieldnames = commonColumns + ObservationParser.columns
            elif eventClass == 'Procedure':
                fieldnames = commonColumns + ProcedureParser.columns
            elif eventClass == 'Payer':
                fieldnames = commonColumns + PayerParser.columns
            elif eventClass == 'Encounter':
                fieldnames = commonColumns + EncounterParser.columns

            # Output to folder
            output_filename = os.path.join(output_folder, eventClass + '.txt')
            output_file_exists = os.path.isfile(output_filename)
            with open(output_filename, fileopenmode) as csvfile:

                # Declare DictWriter
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='|', quoting=csv.QUOTE_NONE,
                                        escapechar='\\')

                # Write header row...
                if not append:
                    # ...if we're not in append mode (file is always created), or
                    writer.writeheader()
                elif not output_file_exists:
                    # ...if we're in append mode but file is being created
                    writer.writeheader()

                # Write output for current eventClass
                for r in doc_data:
                    if eventClass == r['event_class']:
                        r.update(commonValues)
                        writer.writerow(r)

        return True

    def get_clinical_document(self, ccd_el):
        el_return = None
        if 'ClinicalDocument' in ccd_el.tag:
            return ccd_el
        for child_el in ccd_el:
            child_tag = child_el.tag
            if 'ClinicalDocument' in child_tag:
                el_return = child_el
                break
            elif len(child_el):
                el_return = self.get_clinical_document(child_el)
                if el_return is not None:
                    break
        return el_return


if __name__ == "__main__":

    # Init arguement parser
    arg_parser = argparse.ArgumentParser(description='CCD Parser')

    # Set arguements
    arg_parser.add_argument('-s', '--source', required=False, default=os.path.join(os.getcwd(), 'raw_ccd'),
                        help='path to source directory')
    arg_parser.add_argument('-d', '--destination', required=False, default=os.path.join(os.getcwd(), 'parsed_ccd'),
                        help='path to save parsed ccd')
    arg_parser.add_argument('-p', '--parsers', required=False, nargs='*',
                        help='section parsers')

    # Parse arguements
    args = arg_parser.parse_args()

    # Set parsed arguements
    source = args.source
    destination = args.destination
    section_parsers = args.parsers

    # Init ccd parser
    ccd_parser = CCDParser(section_parsers)

    # Call parse
    events = ccd_parser.parse_dir(source, destination)
