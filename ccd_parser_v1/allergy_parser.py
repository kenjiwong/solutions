from utils import _CCDUtils, ALLERGY_TEMPLATE_IDS


class AllergyParser(_CCDUtils):
    section = 'Allergy'
    columns = [
        'event_class',
        'status_code',
        'effective_time_low',
        'effective_time_high',
        'value_code',
        'value_code_sys',
        'code_code',
        'code_code_sys',
        'code_code_sys_name', 
        'code_display_name'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = ALLERGY_TEMPLATE_IDS
        super(AllergyParser, self).__init__(ccd_tree, ns)

    def parse(self):
        allergy_data = list()
        for entry in self.iter_entries():
            entry_rel_el = self.rec_find(entry, 'entryRelationship')

            status_code_text = self.attr_val(entry_rel_el, 'code', 'statusCode')
            code_code = self.attr_val(entry_rel_el, 'code', 'code')
            code_code_sys = self.attr_val(entry_rel_el, 'codeSystem', 'code')
            code_code_sys_name = self.attr_val(entry_rel_el, 'codeSystemName', 'code')
            code_display_name = self.attr_val(entry_rel_el, 'displayName', 'code')

            eff_time_el = self.rec_find(entry, 'effectiveTime')
            eff_time_low_el_text = self.get_range_low(eff_time_el)
            eff_time_low_el_text = self.parse_datetime_str(eff_time_low_el_text)
            eff_time_high_el_text = self.get_range_low(eff_time_el)
            eff_time_high_el_text = self.parse_datetime_str(eff_time_high_el_text)

            val_code_text = self.attr_val(entry_rel_el, 'code', 'value')
            val_code_sys_text = self.attr_val(entry_rel_el, 'codeSystemName', 'value')

            entry_data = dict(
                event_class=self.section,
                status_code=status_code_text,
                effective_time_low=eff_time_low_el_text,
                effective_time_high=eff_time_high_el_text,
                value_code=val_code_text,
                value_code_sys=val_code_sys_text,
                code_code=code_code,
                code_code_sys=code_code_sys,
                code_code_sys_name=code_code_sys_name,
                code_display_name=code_display_name
            )

            if not entry_data['value_code'] == None:
                allergy_data.append(entry_data)

        return allergy_data
