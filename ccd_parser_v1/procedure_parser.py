from utils import _CCDUtils, PROCEDURE_TEMPLATE_IDS


class ProcedureParser(_CCDUtils):
    section = 'Procedure'
    columns = [
        'event_class',
        'procedure_id',
        'mood_code',
        'start_date',
        'end_date',
        'code',
        'code_system'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = PROCEDURE_TEMPLATE_IDS
        super(ProcedureParser, self).__init__(ccd_tree, ns)

    def parse(self):
        procedure_data = []

        for entry in self.iter_entries():

            procedure_el = self.get_CDA_child(entry, 'procedure')
            mood_code = procedure_el.attrib.get('moodCode', '')

            id = self.extract_id(self.get_CDA_child(procedure_el, 'id'))

            eff_time = self.get_CDA_child(procedure_el, 'effectiveTime')
            start_date, end_date = self.get_date_range(eff_time)

            code_el = self.get_CDA_child(procedure_el, 'code')
            if not self.is_null(code_el):
                code = code_el.attrib.get('code', '')
                code_system = code_el.attrib.get('codeSystem', '')

            procedure_data.append({
                'event_class': self.section,
                'procedure_id': id,
                'mood_code': mood_code,
                'start_date': start_date,
                'end_date': end_date,
                'code': code,
                'code_system': code_system
            })

        return procedure_data
