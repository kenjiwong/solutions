from utils import _CCDUtils, OBSERVATION_TEMPLATE_IDS


class ObservationParser(_CCDUtils):
    section = 'Observation'
    columns = [
        'event_class',
        'observation_id',
        'start_date',
        'end_date',
        'code',
        'code_system',
        'result_type',
        'result',
        'result_unit'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = OBSERVATION_TEMPLATE_IDS
        super(ObservationParser, self).__init__(ccd_tree, ns)


    def recursive_parse(self, entry, observation_data):

        for organizer_or_observation in entry:
            if self.has_CDA_tag(organizer_or_observation, 'organizer'):
                for component in self.get_CDA_children(organizer_or_observation, 'component'):
                    self.recursive_parse(component, observation_data)

            elif self.has_CDA_tag(organizer_or_observation, 'observation'):
                obs_el = organizer_or_observation

                id = self.extract_id(self.get_CDA_child(obs_el, 'id'))

                eff_time_el = self.get_CDA_child(obs_el, 'effectiveTime')
                start_date, end_date = self.get_date_range(eff_time_el)

                code = code_system = ''
                code_el = self.get_CDA_child(obs_el, 'code')
                if not self.is_null(code_el):
                    code = code_el.attrib.get('code', '')
                    code_system = code_el.attrib.get('codeSystem', '')

                value_el = self.get_CDA_child(obs_el, 'value')
                value_type = value_el.attrib.get(self.ns_tag('type', self.ns['XSI']), '')
                value = value_el.attrib.get('value', '')
                value_unit = value_el.attrib.get('unit', '')

                observation_data.append({
                        'event_class': self.section,
                        'observation_id': id,
                        'start_date': start_date,
                        'end_date': end_date,
                        'code': code,
                        'code_system': code_system,
                        'result_type': value_type,
                        'result': value,
                        'result_unit': value_unit
                })


    def parse(self):

        observation_data = []

        for entry in self.iter_entries():
            self.recursive_parse(entry, observation_data)

        return observation_data
