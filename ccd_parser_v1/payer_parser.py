from utils import _CCDUtils, PAYER_TEMPLATE_IDS


class PayerParser(_CCDUtils):
    section = 'Payer'
    columns = [
        'event_class',
        'payer_id',
        'mood_code'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = PAYER_TEMPLATE_IDS
        super(PayerParser, self).__init__(ccd_tree, ns)

    def parse(self):
        payer_data = []

        for entry in self.iter_entries():

            act_el = self.get_CDA_child(entry, 'act')
            mood_code = act_el.attrib.get('moodCode', '')

            payer_id = self.extract_id(self.get_CDA_child(act_el, 'id'))

            performer_el = self.get_CDA_child(act_el, 'performer')
            entity_el = self.get_CDA_child(performer_el, 'assignedEntity')
            if not self.is_null(entity_el):
                payer_id = self.extract_id(self.get_CDA_child(entity_el, 'id'))

            payer_data.append({
                'event_class': self.section,
                'payer_id': payer_id,
                'mood_code': mood_code
            })

        return payer_data
