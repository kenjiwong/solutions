from utils import _CCDUtils


class AuthorParser(_CCDUtils):

    def get_root_el(self, el):
        return self.rec_find(el, 'assignedAuthor')

    def parse(self):
        author_el = self.root_el

        addr = self.el_val(self.rec_find(author_el, 'addr'))
        tele_value_texts = ['{0}-{1}'.format(self.attr_val(tele_el, 'use'), self.attr_val(tele_el, 'value')) for tele_el
                            in self.findall(author_el, 'CDA:telecom') if tele_el is not None]
        manu_model = self.el_val(self.rec_find(author_el, 'manufacturerModelName'))
        software = self.el_val(self.rec_find(author_el, 'softwareName'))

        rep_org_el = self.rec_find(author_el, 'representedOrganization')
        org_id = self.attr_val(rep_org_el, 'extension', 'id')
        org_name = self.el_val(self.rec_find(rep_org_el, 'name'))
        org_tel = ['{0}-{1}'.format(self.attr_val(tele_el, 'use'), self.attr_val(tele_el, 'value')) for tele_el
                            in self.findall(rep_org_el, 'CDA:telecom') if tele_el is not None]
        org_address = self.el_val(self.rec_find(rep_org_el, 'streetAddressLine'))
        org_state = self.el_val(self.rec_find(rep_org_el, 'state'))
        org_postal = self.el_val(self.rec_find(rep_org_el, 'postalCode'))

        author_data = dict(
            addr=addr,
            tele_value_texts=tele_value_texts,
            manu_model=manu_model,
            software=software,
            org_id=org_id,
            org_name=org_name,
            org_tel=org_tel,
            org_address=org_address,
            org_state=org_state,
            org_postal=org_postal
        )

        return [author_data]
