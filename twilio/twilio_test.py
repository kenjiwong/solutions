# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = 'AC35d0f5d811139abe12deb686ad17e05b'
auth_token = '064e8395368b76d5f99cc063797755f2'
client = Client(account_sid, auth_token)

message = client.messages.create(
                     body="Apervita - Pick up your medication and schedule follow-up visit",
                     from_='+12194070656',
                     to='+17082673808'
                 )


print(message.sid)