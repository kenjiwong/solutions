import pandas as pd
import json


class LoginCounter(object):

    def __init__(self, file, delimiter):
        self.data_frame = pd.read_csv(file, delimiter)

    def get_distinct(self):
        unique_orgs = set()
        for idx, row in self.data_frame.iterrows():
            result = json.loads(str(self.data_frame.loc[idx, 'payload']))
            unique_orgs.add(result['orgid'])
        print(list(unique_orgs))
        print(len(unique_orgs))

    def main(self):
        self.get_distinct()

if __name__ == '__main__':
    test = LoginCounter('query_result_2019-07-23T17_09_50.100Z.txt', '\t')
    test.main()