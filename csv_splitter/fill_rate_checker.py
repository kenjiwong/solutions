import csv

def fill_rate_checker(file_name):
    with open(file_name, 'rb') as f:
        reader = csv.reader(f)
        print('Reading in CSV')
        event_dict = {}
        for idx, row in enumerate(reader):
            if idx == 0:
                header = row
            else:
                row_length = len(row)
                for row_index in range(row_length):
                    event_dict.setdefault(header[row_index], []).append(row[row_index])
        print('Done iterating through rows and assigning them to dict')
        for header, column in event_dict.iteritems():
            print('Setting {0}'.format(header))
            event_dict[header] = set(column)
    print(event_dict)

fill_rate_checker('ecqm_encounter_31AUG18.csv.DEID.csv')


