import csv
import operator

def combine_columns(file_name):
    with open(file_name, 'rb') as f:
        reader = csv.reader(f)
        with open('encounters.csv', 'wb') as results:
            wtr = csv.writer(results)
            for r in reader:
                wtr.writerow((r[1], r[0], r[28]))
                # wtr.writerow((r[0], r[36], r[31]))

def write_to_one_file():
        with open('result.csv', 'wb') as results:
            wtr = csv.writer(results)
            with open('encounters.csv', 'rb') as fileA:
                reader = csv.reader(fileA)
                for r in reader:
                    wtr.writerow((r[0], r[1], r[2]))

            with open('labs.csv', 'rb') as fileB:
                reader = csv.reader(fileB)
                for idx, r in enumerate(reader):
                    if not idx == 1:
                        wtr.writerow((r[0], r[1], r[2]))

def sort_file():
    with open('result.csv','rb') as f:
        reader = csv.reader(f)
        sortedlist = sorted(reader, key=operator.itemgetter(0), reverse=True)
        event_dict = dict()
        for event in sortedlist:
            event_dict.setdefault(event[0], []).append(event)

        print(len(event_dict.keys()))

        for patient_id, events in event_dict.iteritems():
            dob_set = set()
            for event in events:
                dob_set.add(event[2])
            if not len(dob_set) == 1:
                print('DOB Mismatch for this patient ID: {}'.format(patient_id))



# combine_columns('ecqm_labs_31AUG18.csv.DEID.csv')
# combine_columns('ecqm_encounter_31AUG18.csv.DEID.csv')
# write_to_one_file()
sort_file()