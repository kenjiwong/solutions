import csv

def remove_columns(file_name):
    with open(file_name, 'rb') as f:
        reader = csv.reader(f)
        with open('result.csv', 'wb') as results:
            previous_encounter_id = None
            start_date_set = set()
            end_date_set = set()
            start_date_list = []
            end_date_list = []
            results.write('start_date_set, end_date_set, len_start_date_list, len_end_date_list\n')
            for idx, r in enumerate(reader):
                # wtr.writerow((r[0], r[1], r[4], r[5], r[16], r[24], r[28], r[29]))
                if not idx==0:
                    if previous_encounter_id == r[0] or idx==1:
                        start_date = r[4]
                        end_date = r[5]
                        start_date_set.add(start_date)
                        end_date_set.add(end_date)
                        start_date_list.append(start_date)
                        end_date_list.append(end_date)
                    else:
                        results.write('{4}, {0}, {1}, {2}, {3}\n'.format(str(start_date_set), str(end_date_set), len(start_date_list), len(end_date_list), str(previous_encounter_id)))
                        if len(start_date_set) == 1 and len(end_date_set) == 1:
                            if start_date_set == end_date_set:
                                pass
                            else:
                                print('FAIL START DOES NOT MATCH END {0}|{1}'.format(start_date_set, end_date_set))
                        else:
                            print('FAIL SET DOES NOT =1 {0}|{1}'.format(start_date_set, end_date_set))
                        start_date_set = set()
                        end_date_set = set()
                        start_date_list = []
                        end_date_list = []
                        start_date = r[4]
                        end_date = r[5]
                        start_date_set.add(start_date)
                        end_date_set.add(end_date)
                        start_date_list.append(start_date)
                        end_date_list.append(end_date)
                        previous_encounter_id = r[0]
                else:
                    print('Header')



def check_start_and_end(file_name):
    with open(file_name, 'rb') as f:
        reader = csv.reader(f)
        for r in reader:
            if r[2] == r[3]:
                pass
            else:
                print(r[1])

# check_start_and_end('result.csv')
remove_columns('ecqm_encounter_31AUG18.csv.DEID.csv')
