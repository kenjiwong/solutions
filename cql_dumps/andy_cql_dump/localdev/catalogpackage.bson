&  _id 	   32fr-323 Category    Safety PublisherName    The Leapfrog Group Name    Leapfrog Hospital Safety Score 
Author Specifications r  
<p>
A numerical safety score based on 28 measures is calculated for
all eligible hospitals in the U.S. Hospitals, excluding critical
access hospitals, specialty hospitals, pediatric hospitals, hospitals
in Maryland and territories exempt from public reporting to CMS, and
others.</P>
<P><U>Measures</U></P>
<P>Process/structural measures include:</P>
<ul>
<li>CPOE (Computerized Physician Order Entry) - Leapfrog</li>
<li>IPS (ICU Physician Staffing) - Leapfrog</li>
<li>8 Safe Practices - Leapfrog</li>
<li>5 SCIP (Surgical Care Improvement Project) measures - CMS</li>
</ul>
<P>Outcome measures include:</P>
<ul>
<li>7 HACs (Hospital Acquired Conditions) - CMS and Leapfrog</li>
<li>6 PSIs (Patient Safety Indicators) - CMS</li>
</ul>
<P>Safety Score Letter Grade Assignment</P>
<P>The numerical safety score is then converted into one of five
letter grades. &quot;A&quot; denotes the best hospital safety
performance, followed in order by &quot;B&quot;, &quot;C&quot;, &quot;D&quot;,
and &quot;F&quot;.</P>
<P>Hospitals with a numerical safety score 0.6 standard deviations or
more above the mean earn an &quot;A&quot;. Hospitals with a numerical
safety score at the mean (2.962) up to 0.6 standard deviations above
the mean earn a &quot;B&quot;, Hospitals 1.5 standard deviations
below the mean to the mean earn a &quot;C&quot;, Hospitals 3.0
standard deviations below the mean to 1.5 standard deviations below
the mean earn a &quot;D&quot;, and hospitals more than 3.0 standard
deviations below the mean earn an &quot;F&quot;,</P>
<P>This spring, the numerical scores ranged between 1.3943 and 3.7517
with an average score very close to 3.0.

 SubmitterProfileId    325d-32p.32j5-328 Tags �   0    patient safety 1    medical errors 2    medication errors 3    patient harm 4    patient risk 5    hospital acquired infections 6    hospital acquired injuries 7    hospital mortality  Rank �  Evidence 4  
<p>
The Hospital Safety Score uses national performance measures from
the Leapfrog Hospital Survey, the Agency for Healthcare Research and
Quality (AHRQ), the Centers for Disease Control and Prevention (CDC),
and the Centers for Medicare and Medicaid Services (CMS) to produce a
single score representing a hospital's overall performance in keeping
patients safe from preventable harm and medical errors.</P>
<P>In addition, secondary data from the American Hospital
Association's Annual Survey was used to give hospitals as much credit
as possible toward their safety score.</P>
<P><U>Measures</U></P>
<P>The Leapfrog Blue Ribbon Expert Panel of patient safety experts
selected 28 measures of publicly available hospital safety data,
analyzed the data and determined the weight of each measure based on
evidence, opportunity for improvement and impact. Information from
secondary sources supplemented any missing data to give hospitals as
much credit as possible toward their safety score.</P>
<P>The Hospital Safety Score places each measure into one of two
domains: (1) Process/Structural Measures or (2) Outcome Measures,
each weighted at 50 percent of the overall score.</P>
<P>Process Measures represent how often a hospital gives patients
recommended treatment for a given medical condition or procedure. For
example, SCIP-INF-1 measures how often a hospital adheres to the
process of giving patients an antibiotic within one hour before a
surgical incision.</P>
<P>Structural Measures represent the environment in which patients
receive care. For example, CPOE represents whether a hospital uses a
computerized physician order entry system to prevent medication
errors.</P>
<P>Outcome Measures represent what happens to a patient while
receiving care. For example, Foreign Object Retained after Surgery
measures how many times a patient undergoing surgery had a foreign
object left in his or her body.</P>
<P><U>Supporting References:</U></P>
<P>Leapfrog worked under the guidance of the eight-member Leapfrog
Blue Ribbon Expert Panel to select measures and develop a scoring
methodology. The Panel is made up of patient safety experts from
across the country, including:</P>
<p>
<ul>
<li>Arnold Milstein, M.D., M.P.H., Stanford University</li>
<li>John Birkmeyer, M.D., University of Michigan</li>
<li>Ashish Jha, M.D., M.P.H., Harvard University</li>
<li>Peter Pronovost, M.D., Ph.D., F.C.C.M, Johns Hopkins School
of Medicine</li>
<li>Patrick Romano, M.D., M.P.H., University of California,
Davis</li>
<li>Sara Singer, Ph.D., Harvard University</li>
<li>Tim Vogus, Vanderbilt University</li>
<li>Robert Wachter, M.D., University of California, San
Francisco</li>
</ul>
</p>
<P><U>Publication:</U></P>
<P><SPAN>J. Matthew Austin, Guy
D'Andrea, John D. Birkmeyer MD, Lucian L. Leape MD, Arnold Milstein
MD, Peter J. Pronovost MD, Patrick S. Romano MD, Sara J. Singer,
Timothy J. Vogus, and Robert M. Wachter. </SPAN><I><SPAN><B>Safety
in Numbers: The Development of Leapfrog's Composite Patient Safety
Score for U.S. Hospitals</B></SPAN></I><SPAN>.</SPAN><SPAN>
Journal of Patient Safety. 2013:9:1-9.</SPAN>
</p>
 Metrics w   0 #   0    2,539 1 
   HOSPITALS  1    0    28 1 	   MEASURES  2 '   0    EDITION 1    Spring 2014   PublisherLogo    lflogo.png Featured META �   typeid �.  handle    catalogpackage/32fr-323 persistence_type    Node 	lmt ����c  path    catalogpackage xfer  	createdatetime ����c  identity 	   32fr-323  VendorOrgId 	   332c-32c Preview     
ShortDescription 
Thumbnail Identity 	   32fr-323 Description   
<p>
The Hospital Safety Score is an A, B, C, D, or F letter grade
reflecting how safe hospitals are for patients. The score empowers
patients to make informed decisions about the safety of their
hospital care. A hospital may have the best surgeons and greatest
technology in the world, but unless it is preventing infections and
eliminating medical and medication errors and injuries, it is not
delivering on a very basic premise: ensuring the safety of you and
your loved ones. The goal of the Hospital Safety Score is to reduce
the more than 400,000 yearly deaths from hospital errors and injuries
by publicly recognizing safety and exposing harm, including
infections and acquired injuries (such as bedsores) to medication and
other errors, some potentially fatal.
</p>
    _id 	   32fr-324 
Category PublisherName 	   Apervita Name    Simple Algo 
Author 
Specifications SubmitterProfileId    325d-32q.32j5-327 Tags    0    Simple 1    Test  Rank �  
Evidence Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured  META �   typeid �.  handle    catalogpackage/32fr-324 persistence_type    Node 	lmt ���c  path    catalogpackage xfer  	createdatetime ���c  identity 	   32fr-324  VendorOrgId 	   332c-323 Preview     ShortDescription H   A simple algorithm to demonstrate the power of the apervita technology. 
Thumbnail Identity 	   32fr-324 Description 0   <p>Super Simple test algorithm to play with</p>  �  _id 	   32fr-325 Category    Intensive Care PublisherName 	   Apervita Name    Base Population Analytic 
Author 
Specifications SubmitterProfileId    325d-32q.32j5-327 Tags    0    Simple 1    Test  Rank �  
Evidence Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured META �   typeid �.  handle    catalogpackage/32fr-325 persistence_type    Node 	lmt ���c  path    catalogpackage xfer  	createdatetime ���c  identity 	   32fr-325  VendorOrgId 	   332c-323 Preview     ShortDescription    Test Pop Analytic 
Thumbnail Identity 	   32fr-325 Description    <p>Test Pop Analytic</p>     _id 	   32fr-326 Category    Emergency Care PublisherName 
   Medal.org Name    APACHE II Score 
Author Specifications   
<p>
    It is calculated from 12 routine physiological measurements:
</p>
<ol>
    <li>
        <p>
            Age
        </p>
    </li>
    <li>
        <p>
            Temperature (rectal)
        </p>
    </li>
    <li>
        <p>
            Mean arterial pressure
        </p>
    </li>
    <li>
        <p>
            pH arterial
        </p>
    </li>
    <li>
        <p>
            Heart rate
        </p>
    </li>
    <li>
        <p>
            Respiratory rate
        </p>
    </li>
    <li>
        <p>
            Sodium (serum)
        </p>
    </li>
    <li>
        <p>
            Potassium (serum)
        </p>
    </li>
    <li>
        <p>
            Creatinine
        </p>
    </li>
    <li>
        <p>
            Hematocrit
        </p>
    </li>
    <li>
        <p>
            White blood cell count
        </p>
    </li>
    <li>
        <p>
            Glasgow Coma Scale
        </p>
    </li>
</ol>
<p>
Interpretation of the score:
</p>
<style type="text/css">
.tftable {font-size:12px;color:#333333;width:400px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
.tftable tr {background-color:#ffffff;}
.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
</style>

<table class="tftable" border="1">
<tr><th>Score</th><th>Death Rate %</th></tr>
<tr><td>0 - 4</td><td>4</td></tr>
<tr><td>5 - 9</td><td>8</td></tr>
<tr><td>10 - 14</td><td>15</td></tr>
<tr><td>15 - 19</td><td>25</td></tr>
<tr><td>20 - 24</td><td>40</td></tr>
<tr><td>25 - 29</td><td>55</td></tr>
<tr><td>30 - 34</td><td>75</td></tr>
<tr><td> >34</td><td>85</td></tr>
</table>
 SubmitterProfileId    325d-32q.32j5-327 Tags �  0    Acute Disease 1    Adult 2    Age Factors 3    Chronic Disease 4 9   Coronary Artery Bypass/mortality Costs and Cost Analysis 5    Critical Care/methods 6    Diagnosis-Related Groups 7    Disease/classification 8    Disease/physiopathology 9    Middle Aged 10    Patient Admission 11 
   Prognosis 12    Risk 13 )   Surgical Procedures, Operative/mortality  Rank �  Evidence w  
<p>
The APACHE II scoring system was released in 1985 and incorporated a number of changes from the original APACHE. These included a reduction in the number of variables to 12 by eliminating infrequently measured variables such as lactate and osmolality. The weighting of other variables were altered; most notably, the weightings for Glasgow Coma Score and acute renal failure were increased. In addition, weightings were added for end-organ dysfunction and points given for emergency or non-operative admissions. The APACHE II severity score has shown a good calibration and discriminatory value across a range of disease processes, and remains the most commonly used international severity scoring system worldwide.<br/>
<br/>
References:<br/>
Knaus WA, Draper EA, Wagner DP. APACHE II: a severity of disease classification system. Critical Care Medicine. 1985;13(10):818-29.<br/>
 Metrics �   0 (   0    ICU 1    DISEASE SEVERITY  1 #   0 
   Mortality 1    Score  2 ,   0    12 1    PHYSIOLOGIC VARIABLES   PublisherLogo 
   medal.png Featured  META �   typeid �.  handle    catalogpackage/32fr-326 persistence_type    Node 	lmt x���c  path    catalogpackage xfer  	createdatetime x���c  identity 	   32fr-326  VendorOrgId 	   332c-323 Preview     ShortDescription >   Acute Physiology and Chronic Health Evaluation II (APACHE II) 
Thumbnail Identity 	   32fr-326 Description 5  The Apache (Acute Physiology And Chronic Health Evaluation) II score is a severity of disease classification system.   The score goes from 0 to 71.  A higher score is associated with an increasing risk of hospital death.  Each variable is weighted from 0 to 4, with higher scores denoting an increasing deviation from normal. Age is weighted from 0 to 6.  The APACHE II is measured during the first 24 h of ICU admission; the maximum score is 71. A score of 25 represents a predicted mortality of 50% and a score of over 35 represents a predicted mortality of 80%.  B  _id 	   32fr-327 
Category PublisherName 	   Apervita Name    CAPRA Score 
Author Specifications �   The following parameters are required: (1) Age
(2) PSA (ng/mL)
(3) Gleason Score (Primary/Secondary) (4) Clinical Stage
(5) % of Positive Biopsy Cores SubmitterProfileId    325d-32q.32j5-327 Tags     Rank �  Evidence �  CAPRA was developed using medical charts drawn from the CAPSURE network of 40 community-based urology practices across the U.S. After the formula was developed, it was tested so see how it worked on over 10,000 men being treated for prostate cancer. Researchers found that the tool accurately predicted the likelihood of metastasis, prostate cancer-specific survival and overall survival. Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured  META �   typeid �.  handle    catalogpackage/32fr-327 persistence_type    Node 	lmt `���c  path    catalogpackage xfer  	createdatetime `���c  identity 	   32fr-327  VendorOrgId 	   332c-323 Preview     ShortDescription 5   Cancer of the Prostate Risk Assessment (CAPRA) Score 
Thumbnail Identity 	   32fr-327 Description h  UCSF developed the Cancer of the Prostate Risk Assessment (CAPRA) score. CAPRA is a straightforward 0 to 10 score. It is easy to calculate, yet with accuracy comparable to the best nomograms. A CAPRA score is valid across multiple treatment approaches and it predicts an individual's likelihood of metastasis, cancer-specific mortality, and overall mortality.  %  _id 	   32fr-328 
Category PublisherName 	   Apervita Name &   Cardiac Risk Index with Goldman Score 
Author 
Specifications SubmitterProfileId    325d-32q.32j5-327 Tags 0   0    Score 1    Cardiac 2    Goldman  Rank �  
Evidence Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured  META �   typeid �.  handle    catalogpackage/32fr-328 persistence_type    Node 	lmt H��c  path    catalogpackage xfer  	createdatetime H��c  identity 	   32fr-328  VendorOrgId 	   332c-323 Preview     ShortDescription &   Cardiac Risk Index with Goldman Score 
Thumbnail Identity 	   32fr-328 Description 9  <p>This is a multi-factorial index of cardiac risk in the non-cardiac surgical setting. It was developed for preoperative identification of patients at risk from major perioperative cardiovascular complications. The data were derived retrospectively in 1977 from 1001 patients undergoing non-cardiac surgery.</p>  �  _id 	   32fr-329 
Category PublisherName 	   Apervita Name    CharlesonComorbidityScore 
Author 
Specifications SubmitterProfileId    325d-32q.32j5-327 Tags R   0 	   Charlson 1    Comorbidity 2    Risk 3    Score 4 
   Mortality  Rank �  
Evidence Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured  META �   typeid �.  handle    catalogpackage/32fr-329 persistence_type    Node 	lmt 0��c  path    catalogpackage xfer  	createdatetime 0��c  identity 	   32fr-329  VendorOrgId 	   332c-323 Preview     ShortDescription <   The Charlson comorbidity Index (CCI) for ten-year mortality 
Thumbnail Identity 	   32fr-329 Description �  <p>The Charlson comorbidity Index (CCI) predicts the ten-year mortality for a patient who may have a range of comorbid conditions, such as heart disease, AIDS, or cancer (a total of 22 conditions).</p>
<p>Each condition is assigned a score of 1, 2, 3, or 6, depending on the risk of dying associated with each one. Scores are summed to provide a total score to predict mortality.</p>    _id 	   32fr-32a Category 
   Mortality PublisherName 
   Medal.org Name $   Charlson Comorbidity Index with Age 
Author Specifications �  
Scoring is based on the presence of the following parameters: <br/>
<ol>
<li>Myocardial infarction
<li>Congestive heart failure
<li>Peripheral vascular disease
<li>Cerebrovascular disease
<li>Dementia
<li>Chronic obstructive pulmonary disease
<li>Connective tissue disease
<li>Peptic ulcer disease
<li>Diabetes mellitus
<li>Chronic kidney disease - moderate to severe
<li>Hemiplegia
<li>Leukemia
<li>Malignant lymphoma
<li>Solid tumor
<li>Liver disease
<li>AIDS
<li>Patient age
</ol>
<br/>
<br/>
Score ranges from 0-41; the higher the index score, the greater the risk of death.<br/>
<br/>
Result:<br/>
Weighted index score for risk of death.<br/>
 SubmitterProfileId    325d-32q.32j5-327 Tags �   0    Actuarial Analysis 1    Age Factors 2    Breast Neoplasms/epidemiology 3    Epidemiologic Methods 4 
   Morbidity 5 
   Prognosis 6    Prospective Studies 7    Risk  Rank �  Evidence n  
A weighted index that takes into account the number and the seriousness of comorbid disease developed in a cohort of 559 medical patients.  It was later successfully tested again in a cohort of 685 patients and in several further studies.<br/>
<br/>
References:<br/>
Charlson ME, Pompei P, Ales KL, MacKenzie CR. A new method of classifying prognostic comorbidity in longitudinal studies: Development and validation. Journal of Chronic Disease. 1987; 40: 373-383.<br/>
Charlson M, Szatrowski TP, Peterson J, Gold J. Validation of a combined comorbidity index. Journal of Clinical Epidemiology. 1994; 47: 1245-1251.<br/>
 Metrics �   0 %   0    Comorbidity 1    INDEX  1 *   0    10 Year 1    SURVIVAL INDEX  2 &   0    559 1    PATIENT COHORT   PublisherLogo 
   medal.png Featured  META �   typeid �.  handle    catalogpackage/32fr-32a persistence_type    Node 	lmt 	��c  path    catalogpackage xfer  	createdatetime 	��c  identity 	   32fr-32a  VendorOrgId 	   332c-323 Preview     
ShortDescription 
Thumbnail Identity 	   32fr-32a Description �  
The Charlson Comorbidity Index is a weighted index score for evaluating whether a patient will live long enough to benefit from a specific screening measure or medical intervention. It is helpful in deciding how aggressively to treat a condition.<br/>
<br/>
The index is based on age and a range of comorbid conditions, such as heart disease, AIDS, diabetes, cancer, and others (a total of 19 conditions). Each condition is assigned a score of 1, 2, 3, or 6, depending on the risk of dying associated with each one. Scores are summed to provide a total score to predict mortality.<br/>
<br/>
The higher the score, the higher the risk of death from comorbid disease.  �  _id 	   32fr-32b 
Category PublisherName 	   Apervita Name     eMerge Type 2 Diabetes Mellitus 
Author 
Specifications SubmitterProfileId    325d-32q.32j5-327 Tags     0 	   Diabetes 1    EWS  Rank �  
Evidence Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured  META �   typeid �.  handle    catalogpackage/32fr-32b persistence_type    Node 	lmt ���c  path    catalogpackage xfer  	createdatetime ���c  identity 	   32fr-32b  VendorOrgId 	   332c-323 Preview     ShortDescription )   Screening score Type 2 Diabetes Mellitus 
Thumbnail Identity 	   32fr-32b Description �   <p>Diabetes is the fastest growing chronic disease worldwide. Evaluation of diagnostic tests using as fasting plasma glucose, oral glucose tolerance test and glycosylated hemoglobin, gives an accurate diagnosis of Type 2 diabetes mellitus.</p>    _id 	   32fr-32c Category    Nephrology PublisherName B   National Institutes of Diabetes and Digestive and Kidney Diseases Name 1   MDRD Glomerular Filtration Rate (GFR) for Adults 
Author Specifications �  
<p>
    The formula includes these variables:
</p>
<ol>
    <li>
        <p>
            Age
        </p>
    </li>
    <li>
        <p>
            Creatinine measurement
        </p>
    </li>
    <li>
        <p>
            Gender
        </p>
    </li>
    <li>
        <p>
            Ethnicity
        </p>
    </li>
</ol>

<style type="text/css">
.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
.tftable tr {background-color:#ffffff;}
.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
</style>

<table class="tftable" border="1">
<tr><th>Stage</th><th>Description</th><th>Glomerular Filtration Rate (GFR)</th></tr>
<tr><td>At increased risk</td>
    <td>Risk factors for kidney disease (e.g., diabetes, high blood pressure, family history, older age, ethnic group)</td>
    <td>More than 90</td></tr>
<tr><td>1</td><td>Kidney damage (protein in the urine) and normal GFR</td><td>More than 90</td></tr>
<tr><td>2</td><td>Kidney damage and mild decrease in GFR</td><td>60 to 89</td></tr>
<tr><td>3</td><td>Moderate decrease in GFR</td><td>30 to 59</td></tr>
<tr><td>4</td><td>Severe decrease in GFR</td><td>15 to 29</td></tr>
<tr><td>5</td><td>Kidney failure (dialysis or kidney transplant needed)</td><td>Less than 15</td></tr>
</table>
<br/>
<p>
Normal GFR for healthy adults is >= 90mL/min/1.73 m<sup>2</sup>.<br/>
Normal GFR for elderly adults without kidney damage is >= 60mL/min/1.73 m<sup>2</sup>.<br/>
<br/>
Result:<br/>
Filtration rate in mL/min/1.73 m<sup>2</sup>.<br/>
</p>
 SubmitterProfileId    325d-32q.32j5-327 Tags    0    Adult 1    Biological Markers/blood 2    Creatinine/blood 3    Glomerular Filtration Rate 4    Middle Aged Biological Models 5 #   Renal Insufficiency, Chronic/blood 6 *   Renal Insufficiency, Chronic/diet therapy 7 -   Renal Insufficiency, Chronic/physiopathology  Rank �  Evidence d  
<p>
References:<br/>
Levey AS, Coresh J, Greene T, Stevens LA, Zhang YL, Hendriksen S, Kusek JW, Van Lente F; Chronic Kidney Disease Epidemiology Collaboration. Using standardized serum creatinine values in the modification of diet in renal disease study equation for estimating glomerular filtration rate.Ann Intern Med. 2006 Aug 15;145(4):247-54.
</p>
 Metrics �   0 .   0    Kidney 1    ADULT RENAL DISEASE  1 (   0 
   CKD Stage 1    COMPATIBLE  2 =   0    Normalized 1    1.73 METER<sup>2</sup> SURFACE   PublisherLogo    niddks.png Featured  META �   typeid �.  handle    catalogpackage/32fr-32c persistence_type    Node 	lmt ���c  path    catalogpackage xfer  	createdatetime ���c  identity 	   32fr-32c  VendorOrgId 	   332c-323 Preview     
ShortDescription 
Thumbnail Identity 	   32fr-32c Description   
<p>
The Modification of Diet in Renal Disease (MDRD) Glomerular Filtration Rate (GFR) measures how well your kidneys are filtering a waste called creatinine, which is produced by the muscles. When the kidneys aren't working as well as they should, creatinine builds up in the blood.  GFR measures the level of kidney function and potential presence of poor kidney function and/or failure.<br/>
<br/>
The MDRD GFR is only applicable to individuals 18 years and older, and does not require weight, because it presumes a normalized accepted average adult body surface area of 1.73 m<sup>2</sup>.<br/>
<br/>
GFR values are typically reported as greater than or equal to 60 mL/min/1.73 m<sup>2</sup>. Generally, lower filtration rates signify lower kidney function.<br/>
</p>
  �  _id 	   32fr-32d Category    Intensive Care PublisherName 
   Medal.org Name (   Sepsis related Organ Failure Assessment Author    Dr. Ian Wotkun, PHD, LMNOP Specifications n  
<p>
The formula includes these variables:<br/>
<ul>
<li>Serum bilirubin</li>
<li>Serum creatinine</li>
<li>Glasgow coma score</li>
<li>Platelet count</li>
<li>PaO2 to FIO2 ratio</li>
<li>Hypotension</li>
</ul>
<p>
Interpretation of the score:
</p>
<style type="text/css">
.tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
.tftable tr {background-color:#ffffff;}
.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}
</style>

<table class="tftable" border="1">
<tr><th></th><th colspan='5'>Mortality Rate by SOFA Score</th></tr>
<tr><th>Organ System</th><th>0</th><th>1</th><th>2</th><th>3</th><th>4</th></tr>
<tr><td>Coagulation</td><td>35%</td><td>35%</td><td>35%</td><td>64%</td><td>64%</td>
<tr><td>Respiration</td><td>20%</td><td>27%</td><td>32%</td><td>46%</td><td>64%</td>
<tr><td>Liver</td><td>32%</td><td>34%</td><td>50%</td><td>53%</td><td>56%</td>
<tr><td>Cariovascular</td><td>22%</td><td>32%</td><td>55%</td><td>55%</td><td>55%</td>
<tr><td>Central Nervous System</td><td>26%</td><td>35%</td><td>46%</td><td>56%</td><td>70%</td>
<tr><td>Renal</td><td>25%</td><td>40td><td>46%</td><td>56%</td><td>64%</td>
</table>
 SubmitterProfileId    325d-32q.32j5-327 Tags P  0 &   Multiple Organ Failure/classification 1 !   Multiple Organ Failure/diagnosis 2 $   Multiple Organ Failure/microbiology 3 !   Multiple Organ Failure/mortality 4    Reproducibility of Results 5 )   Risk Factors
Sensitivity and Specificity 6    Sepsis/complications 7    Severity of Illness Index 8    Time Factors  Rank �  Evidence �  
<p>
The SOFA score takes measures of the following organ systems:<br/>
<ul>
<li>Coagulation</li>
<li>Respiration</li>
<li>Liver</li>
<li>Cardiovascular</li>
<li>Central Nervous System</li>
<li>Renal</li>
</ul>
<p>
References:<br/>
Vincent JL, Moreno R, et al. The SOFA (Sepsis-related Organ Failure Assessment) score to describe organ dysfunction/failure. Intensive Care Medicine. 1996; 22: 707-710.<br/>
</p>
 Metrics �   0 ,   0    ICU Sepsis 1    ORGAN FAILURE  1 1   0    Multi-Organ 1    DYSFUNCTION SCORE  2 +   0    6 1    PHYSIOLOGIC VARIABLES   PublisherLogo 
   medal.png Featured  META �   typeid �.  handle    catalogpackage/32fr-32d persistence_type    Node 	lmt ���c  path    catalogpackage xfer  	createdatetime ���c  identity 	   32fr-32d  VendorOrgId 	   332c-323 Preview     
ShortDescription 
Thumbnail Identity 	   32fr-32d Description �  
The Sepsis-related Organ Failure Assessment (SOFA) score is a score for evaluating multiorgan failure in intensive care patients with the sepsis syndrome. It is intended to be easy to calculate and to describe the sequence of complications in a critically ill patient rather than to predict outcome. It was developed by the Working Group on Sepsis-related Problems of the European Society of Intensive Care Medicine.
  f	  _id 	   32fr-32f Category    Endocrinology PublisherName 
   Medal.org Name    Wilson Diabetes 8-Year Risk 
Author Specifications �  
<p>
Variables required:<br/>
<br/>
<ol>
<li>body mass index
<li>parental history of diabetes mellitus
<li>blood pressure
<li>fasting serum glucose
<li>serum HDL cholesterol
<li>serum triglyceride concentration
<li>gender
</ol>
<br/>
Score ranges from 0-28; the higher the score the greater the risk of developing Type 2 diabetes within 8 years.<br/>
<br/>
Result:<br/>
Percentage (%) risk of developing Type 2 diabetes within 8 years
 SubmitterProfileId    325d-32q.32j5-327 Tags �   0    Aged, Blood Glucose/metabolism 1    Cholesterol, HDL/metabolism 2     Diabetes Mellitus, Type 2/blood 3 '   Diabetes Mellitus, Type 2/epidemiology 4 %   Diabetes Mellitus, Type 2/metabolism 5    Middle Aged 6    Risk Factors  Rank �  Evidence �  
<p>
3140 men and women who attended the fifth clinic examination of the Framingham Offspring Study in the mid-1990s.<br/>
<br/>
This population sample is 99% white and non-Hispanic, middle aged adults with a mean age of 54 years.<br/>
<br/>
References:<br/>
Wilson PW, Meigs JB, Sullivan L, Fox CS, Nathan DM, D'Agostino RB Sr. Prediction of incident diabetes mellitus in middle-aged adults. The Framingham Offspring Study. Archives of Internal Medicine 2007; 167: 1068-1074.<br/>
</p>
 Metrics y   0 ,   0    Type 2 1    DIABETES MELLITUS  1 !   0    8 Year 1    RISK %  2    0    600+ 1    USERS   PublisherLogo 
   medal.png Featured  META �   typeid �.  handle    catalogpackage/32fr-32f persistence_type    Node 	lmt ���c  path    catalogpackage xfer  	createdatetime ���c  identity 	   32fr-32f  VendorOrgId 	   332c-323 Preview     
ShortDescription 
Thumbnail Identity 	   32fr-32f Description 1  
<p>
The Wilson Diabetes 8-Year Risk predicts the risk of developing Type 2 diabetes mellitus within 8 years for adults. The score is based on predictors from the Framingham Offspring Study with a cohort of 3140 men and women.<br/>
<br/>
The higher the score, the greater the risk of developing Type 2 diabetes within 8 years. Modifiable factors that can reduce risk include body weight, serum HDL cholesterol and triglycerides.<br/>
<br/>
This insight provides the percentage (%) risk that an individual will develop Type 2 Diabetes within 8 years.<br/>
</p>
  t  _id 	   32fr-32g 
Category PublisherName 	   Apervita Name    Wilson Diabetes Score 
Author 
Specifications SubmitterProfileId    325d-32q.32j5-327 Tags $   0 	   Diabetes 1    Wilsons  Rank �  
Evidence Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured  META �   typeid �.  handle    catalogpackage/32fr-32g persistence_type    Node 	lmt ���c  path    catalogpackage xfer  	createdatetime ���c  identity 	   32fr-32g  VendorOrgId 	   332c-323 Preview     ShortDescription    Wilson Diabetes Score 
Thumbnail Identity 	   32fr-32g Description �   <p>Diabetes is the fastest growing chronic disease worldwide.</p>
<p>This algorithm generates a score that is externally evaluated to determine the risk of diabetes in 8 years</p>  �  _id 	   32fr-32h 
Category PublisherName 	   Apervita Name    Patient Health Questionnaire 9 
Author Specifications �   The following topics are covered: (1) Level of interest
(2) Level of depression
(3) Ability to sleep
(4) Feeling tired
(5) Poor appetite
(6) Feeling bad about self
(7) Trouble concentrating
(8) Moving or speaking slowly
(9) Self-harm
 SubmitterProfileId    325d-32q.32j5-327 Tags     Rank �  Evidence �   A total of 664 high-risk patients aged between 18 and 70 years selected from patients enrolled at 6 health centres with 23 GPs and falling into one of 3 high-risk groups for depressive disorder. PHQ-9 proved to be highly effective for screening patients. Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured  META �   typeid �.  handle    catalogpackage/32fr-32h persistence_type    Node 	lmt � ��c  path    catalogpackage xfer  	createdatetime � ��c  identity 	   32fr-32h  VendorOrgId 	   332c-323 Preview     ShortDescription '   Patient Health Questionnaire 9 (PHQ-9) 
Thumbnail Identity 	   32fr-32h Description !  The PHQ-9 is a multipurpose instrument for screening, monitoring, and measuring the severity of depression. The PHQ-9 can be completed by patients in minutes. The PHQ-9 can also be administered repeatedly, which can reflect improvement or worsening of depression in response to treatment.  �  _id 	   32fr-32j Category    Intensive Care PublisherName 	   Apervita Name    Fever Temperature Sample 
Author 
Specifications SubmitterProfileId    325d-32q.32j5-327 Tags    0    Simple 1    Test  Rank �  
Evidence Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured META �   typeid �.  handle    catalogpackage/32fr-32j persistence_type    Node 	lmt � ��c  path    catalogpackage xfer  	createdatetime � ��c  identity 	   32fr-32j  VendorOrgId 	   332c-323 Preview     ShortDescription    Test Algo. 
Thumbnail Identity 	   32fr-32j Description    <p>Fever test algorithm</p>    _id 	   32fr-32k 
Category PublisherName 	   Apervita Name "   CART Algorithm for Cardiac Arrest 
Author 
Specifications SubmitterProfileId    325d-32q.32j5-327 Tags %   0    Cardiac 1 
   Inpatient  Rank �  
Evidence Metrics q   0 #   0    650+ 1    Unit Tests  1    0    600+ 1    Users  2 "   0    10 1    Little Pigs   PublisherLogo    apervita_logo.png Featured  META �   typeid �.  handle    catalogpackage/32fr-32k persistence_type    Node 	lmt @,��c  path    catalogpackage xfer  	createdatetime @,��c  identity 	   32fr-32k  VendorOrgId 	   332c-323 Preview     ShortDescription "   CART Algorithm for Cardiac Arrest 
Thumbnail Identity 	   32fr-32k Description *   <p>CART Algorithm paragraph goes here</p>  