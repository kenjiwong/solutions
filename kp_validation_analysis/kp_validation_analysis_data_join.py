import pandas as pd
import datetime
import numpy


class ValidationAnalysisJoiner(object):

    def __init__(self, file1, delimiter1, file2, delimiter2):
        self.data_frame_1 = pd.read_csv(file1, delimiter1)
        self.data_frame_2 = pd.read_csv(file2, delimiter2)

    def main(self):
        self.merge_ids()
        self.change_measure_results_to_uniqueid()
        self.drop_duplicate_fields()
        self.write_to_csv()

    def merge_ids(self):
        self.final_dataframe = pd.DataFrame.merge(self.data_frame_1, self.data_frame_2, left_on='PatientID', right_on='PatientID', how='left')

    def change_measure_results_to_uniqueid(self):
        measure_populations = ['NotinPopulation', 'InitialPopulation', 'DoesNotMeet', 'MeasurePopulation', 'DenominatorExclusion', 'DenominatorException', 'Denominator', 'CasesFailingMeasure', 'Numerator']
        print(len(self.final_dataframe.index))
        for idx, row in self.final_dataframe.iterrows():
            print(idx)
            for measure_population in measure_populations:
                self.change_field(idx, measure_population, row)

    def change_field(self, index, field, row):
        result = self.final_dataframe.loc[index, field]
        self.final_dataframe.loc[index, '{0}Boolean'.format(field)] = row[field]
        if result:
            self.final_dataframe.loc[index, field] = row['unique_patient_id']
        else:
            self.final_dataframe.loc[index, field] = None


    def drop_duplicate_fields(self):
        self.final_dataframe.drop_duplicates(subset='unique_index')

    def write_to_csv(self):
        self.final_dataframe.index.name = 'Index'
        self.final_dataframe.to_csv('Validation_Analysis_{}.csv'.format(str(datetime.datetime.utcnow())))

if __name__ == '__main__':
    validation_analysis = ValidationAnalysisJoiner('profile_result_set_new.csv', ',', 'kp_measure_results.csv', ',')
    validation_analysis.main()