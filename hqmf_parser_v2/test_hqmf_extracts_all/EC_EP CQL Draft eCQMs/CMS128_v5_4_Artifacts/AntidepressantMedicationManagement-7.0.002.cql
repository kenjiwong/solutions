library AntidepressantMedicationManagement version '7.0.002'

using QDM version '5.3'

include Hospice version '0.1.000' called Hospice
include MATGlobalCommonFunctions version '2.0.000' called Global

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Annual Wellness Visit": 'urn:oid:2.16.840.1.113883.3.526.3.1240'
valueset "Antidepressant Medication": 'urn:oid:2.16.840.1.113883.3.464.1003.196.12.1213'
valueset "Home Healthcare Services": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1016'
valueset "Major Depression": 'urn:oid:2.16.840.1.113883.3.464.1003.105.12.1007'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Preventive Care Services - Established Office Visit, 18 and Up": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1025'
valueset "Preventive Care Services-Initial Office Visit, 18 and Up": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1023'
valueset "Psych Visit - Diagnostic Evaluation": 'urn:oid:2.16.840.1.113883.3.526.3.1492'
valueset "Psych Visit - Psychotherapy": 'urn:oid:2.16.840.1.113883.3.526.3.1496'
valueset "Telephone Management": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1053'
valueset "Telephone Evaluation": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1082'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Numerator One":
	"Cumulative Medication Duration Greater Than or Equal to 84 Days"

define "Numerator Two":
	"Cumulative Medication Duration Greater Than or Equal to 180 Days"

define "Denominator":
	"Initial Population"

define "Has Initial Major Depression Diagnosis":
	First(["Diagnosis": "Major Depression"] MajorDepression
			with "Antidepressant Dispensed 270 days before or 90 days after Start of Measurement Period" AntidepressantDispensed
				such that MajorDepression.prevalencePeriod starts 60 days or less before or on day of AntidepressantDispensed.authorDatetime
					or MajorDepression.prevalencePeriod starts 60 days or less after day of AntidepressantDispensed.authorDatetime
			sort by start of prevalencePeriod
	)is not null

define "Qualifying Encounters":
	( ["Encounter, Performed": "Office Visit"]
		union ["Encounter, Performed": "Preventive Care Services - Established Office Visit, 18 and Up"]
		union ["Encounter, Performed": "Preventive Care Services-Initial Office Visit, 18 and Up"]
		union ["Encounter, Performed": "Home Healthcare Services"]
		union ["Encounter, Performed": "Telephone Management"]
		union ["Encounter, Performed": "Telephone Evaluation"]
		union ["Encounter, Performed": "Annual Wellness Visit"]
		union ["Encounter, Performed": "Psych Visit - Diagnostic Evaluation"]
		union ["Encounter, Performed": "Psych Visit - Psychotherapy"] ) ValidEncounter
		where ValidEncounter.relevantPeriod during "Measurement Period"

define "Cumulative Medication Duration Greater Than or Equal to 84 Days":
	"Cumulative Medication Duration"("Antidepressant Taken within 114 Days After Initial Dispense")>= 84

define "Cumulative Medication Duration Greater Than or Equal to 180 Days":
	"Cumulative Medication Duration"("Antidepressant Taken within 231 Days After Initial Dispense")>= 180

define "Initial Population":
	exists ( ["Patient Characteristic Birthdate"] BirthDate
			where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 18
	)
		and "Has Initial Major Depression Diagnosis"
		and exists "Qualifying Encounters"

define "Antidepressant Dispensed 270 days before or 90 days after Start of Measurement Period":
	First(["Medication, Dispensed": "Antidepressant Medication"] FirstAntidepressant
			where FirstAntidepressant.authorDatetime 270 days or less before start of "Measurement Period"
				or FirstAntidepressant.authorDatetime 90 days or less on or after start of "Measurement Period"
			sort by authorDatetime
	)

define "Antidepressant Taken within 114 Days After Initial Dispense":
	["Medication, Active": "Antidepressant Medication"] OnAntidepressant
		with "Antidepressant Dispensed 270 days before or 90 days after Start of Measurement Period" InitialDispense
			such that OnAntidepressant.relevantPeriod starts 114 days or less on or after day of InitialDispense.authorDatetime

define "Antidepressant Taken within 231 Days After Initial Dispense":
	["Medication, Active": "Antidepressant Medication"] ActiveAntidepressant
		with "Antidepressant Dispensed 270 days before or 90 days after Start of Measurement Period" InitialDispense
			such that ActiveAntidepressant.relevantPeriod starts 231 days or less on or after day of InitialDispense.authorDatetime

define "Denominator Exclusions":
	Hospice."Has Hospice"
		or exists ["Medication, Active": "Antidepressant Medication"] OnAntiDepressant
			with "Antidepressant Dispensed 270 days before or 90 days after Start of Measurement Period" AntidepressantDispensed
				such that OnAntiDepressant.relevantPeriod starts 105 days or less before AntidepressantDispensed.authorDatetime

define function "Cumulative Medication Duration"(Medication List<"Medication, Active">):
	Sum((collapse(Medication.relevantPeriod))CumulativeAntidepressant
			return all duration in days of CumulativeAntidepressant
	)
