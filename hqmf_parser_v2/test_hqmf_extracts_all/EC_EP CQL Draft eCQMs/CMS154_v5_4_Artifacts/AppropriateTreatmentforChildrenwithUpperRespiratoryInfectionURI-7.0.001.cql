library AppropriateTreatmentforChildrenwithUpperRespiratoryInfectionURI version '7.0.001'

using QDM version '5.3'

include Hospice version '1.0.000' called Hospice
include MATGlobalCommonFunctions version '2.0.000' called Global

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Antibiotic Medications for Pharyngitis": 'urn:oid:2.16.840.1.113883.3.464.1003.196.12.1001'
valueset "Competing Conditions for Respiratory Conditions": 'urn:oid:2.16.840.1.113883.3.464.1003.102.12.1017'
valueset "Emergency Department Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1010'
valueset "Hospital Observation Care - Initial": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1002'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Preventive Care, Established Office Visit, 0 to 17": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1024'
valueset "Preventive Care Services, Initial Office Visit, 0 to 17": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1022'
valueset "Upper Respiratory Infection": 'urn:oid:2.16.840.1.113883.3.464.1003.102.12.1022'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Denominator":
	"Initial Population"

define "Qualifying Encounter":
	( ["Encounter, Performed": "Office Visit"]
		union ["Encounter, Performed": "Emergency Department Visit"]
		union ["Encounter, Performed": "Preventive Care Services, Initial Office Visit, 0 to 17"]
		union ["Encounter, Performed": "Preventive Care, Established Office Visit, 0 to 17"]
		union ["Encounter, Performed": "Hospital Observation Care - Initial"] ) ValidEncounter
		where ValidEncounter.relevantPeriod during "Measurement Period"

define "Initial Population":
	"Encounter with Upper Respiratory Infection" Encounter
		with ["Patient Characteristic Birthdate"] BirthDate
			such that Global."CalendarAgeInMonthsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 3
				and Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")< 18
		return Encounter.ValidEncounter

define "In Hospice":
	"Initial Population" IP
		where Hospice."Has Hospice"

define "Denominator Exclusions":
	"In Hospice"
		union "URI Encounter with Antibiotics Active 30 Days Prior"
		union "Has Competing Diagnosis for Upper Respiratory Infection"

define "Has Competing Diagnosis for Upper Respiratory Infection":
	( "Encounter with Upper Respiratory Infection" EncounterWithURI
			with ["Diagnosis": "Competing Conditions for Respiratory Conditions"] CompetingCondition
				such that CompetingCondition.prevalencePeriod starts 3 days or less after day of start of EncounterWithURI.ValidEncounter.relevantPeriod
			return EncounterWithURI.ValidEncounter
	)

define "URI Encounter with Antibiotics Active 30 Days Prior":
	"Encounter with Upper Respiratory Infection" EncounterWithURI
		with ["Medication, Active": "Antibiotic Medications for Pharyngitis"] ActiveAntibiotic
			such that ActiveAntibiotic.relevantPeriod overlaps Interval[Global."ToDate"((start of EncounterWithURI.URI.prevalencePeriod)- 30 days), Global."ToDate"(start of EncounterWithURI.URI.prevalencePeriod))
		return EncounterWithURI.ValidEncounter

define "Encounter with Upper Respiratory Infection":
	from
		"Qualifying Encounter" ValidEncounter,
		["Diagnosis": "Upper Respiratory Infection"] URI
		where URI.prevalencePeriod starts during ValidEncounter.relevantPeriod
			or URI.prevalencePeriod overlaps before ValidEncounter.relevantPeriod

define "Numerator":
	"Encounter with Upper Respiratory Infection" EncounterWithURI
		without ["Medication, Order": "Antibiotic Medications for Pharyngitis"] OrderedAntibiotic
			such that OrderedAntibiotic.authorDatetime 3 days or less on or after start of EncounterWithURI.ValidEncounter.relevantPeriod
		return EncounterWithURI.ValidEncounter
