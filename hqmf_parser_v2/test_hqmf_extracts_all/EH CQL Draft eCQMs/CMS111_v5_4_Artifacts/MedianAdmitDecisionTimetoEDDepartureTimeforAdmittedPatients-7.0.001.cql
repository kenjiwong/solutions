library MedianAdmitDecisionTimetoEDDepartureTimeforAdmittedPatients version '7.0.001'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Encounter Inpatient": 'urn:oid:2.16.840.1.113883.3.666.5.307'
valueset "Psychiatric/Mental Health Diagnosis": 'urn:oid:2.16.840.1.113883.3.117.1.7.1.299'
valueset "Hospital Settings": 'urn:oid:2.16.840.1.113762.1.4.1111.126'
valueset "Decision to Admit to Hospital Inpatient": 'urn:oid:2.16.840.1.113883.3.117.1.7.1.295'
valueset "Emergency Department Visit": 'urn:oid:2.16.840.1.113883.3.117.1.7.1.292'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "ED Visit with Admit Order":
	/*ED visit during the specified period with a Decision to admit order also during that specified period*/
	["Encounter, Performed": "Emergency Department Visit"] EDVisit
		with ["Encounter, Order": "Decision to Admit to Hospital Inpatient"] AdmitOrder
			such that AdmitOrder.authorDatetime during EDVisit.relevantPeriod

define "Inpatient Encounter":
	/*Inpatient encounter less than or equal to 120 days during the measurement period*/
	["Encounter, Performed": "Encounter Inpatient"] Encounter
		where Global."LengthInDays"(Encounter.relevantPeriod)<= 120
			and Encounter.relevantPeriod ends during "Measurement Period"

define "Initial Population":
	/*Emergency Department visit with a Decision to Admit inpatient followed by an Inpatient encounter within or at an hour of the ED Visit*/
	"Inpatient Encounter" Encounter
		with "ED Visit with Admit Order" EDAdmitOrder
			such that EDAdmitOrder.relevantPeriod ends 1 hour or less before or on start of Encounter.relevantPeriod

define "Measure Population Exclusions":
	/* Exclude ED encounters with an admission source in "Hospital Setting" (any different facility, even if part of the same hospital system) resulting in an inpatient stay */
	"Inpatient Encounter" Encounter
		with ["Encounter, Performed": "Emergency Department Visit"] EDVisit
			such that EDVisit.relevantPeriod ends 1 hour or less before or on start of Encounter.relevantPeriod
				and EDVisit.admissionSource in "Hospital Settings"

define "Stratification 1":
	/*Patients without a principal diagnosis in the "Psychiatric/Mental Health Diagnosis" value set*/
	"Inpatient Encounter" Encounter
		where not ( Encounter.principalDiagnosis in "Psychiatric/Mental Health Diagnosis" )

define "Stratification 2":
	/*Patients with a principal diagnosis that is in the "Psychiatric/Mental Health Diagnosis" value set*/
	"Inpatient Encounter" Encounter
		where Encounter.principalDiagnosis in "Psychiatric/Mental Health Diagnosis"

define "Measure Population":
	"Initial Population"

define function "Departure Time"(Encounter "Encounter, Performed"):
	/* The time the patient physically departed the Emergency Department*/
	Last(Encounter.facilityLocations Location
			return end of Location.locationPeriod
			sort ascending
	)

define function "Related ED Visit"(Encounter "Encounter, Performed"):
	/*ED visit with a Decision to admit order that preceded an inpatient encounter*/
	Last(["Encounter, Performed": "Emergency Department Visit"] EDVisit
			with ["Encounter, Order": "Decision to Admit to Hospital Inpatient"] AdmitOrder
				such that AdmitOrder.authorDatetime during EDVisit.relevantPeriod
			where EDVisit.relevantPeriod ends 1 hour or less before or on start of Encounter.relevantPeriod
			sort by start of relevantPeriod
	)

define function "Admit Decision"(Encounter "Encounter, Performed"):
	/*Decision to admit order that was during the ED visit that is being referenced*/
	Last(["Encounter, Order": "Decision to Admit to Hospital Inpatient"] AdmitOrder
			where AdmitOrder.authorDatetime during "Related ED Visit"(Encounter).relevantPeriod
			sort by authorDatetime
	)

define function "Measure Observation"(Encounter "Encounter, Performed"):
	/* the duration from the Decision to Admit to the departure from the Emergency Department*/
	duration in minutes of Interval["Admit Decision"(Encounter).authorDatetime, "Departure Time"("Related ED Visit"(Encounter))]
