'''
Author: Kenji Wong
Date: July 3, 2018
Purpose: Intended to be an example of HQMF parsing for the dev team: https://docs.google.com/spreadsheets/d/18vUdoFqFtyKlzPlG9kMbqSuB5xTSWZt58crTDntsTCA/edit?ts=5b34f128#gid=0
Notes:
    - Parser executed on 3 files: CMS71, CMS90, CMS146 and results are still being validated
    - Only pulled metadata where there were examples
    - Would like to clean up the formatting dict method eventually
    - In the future, may want to unifrom all outputs of the parser as a list which denotes the actual key value pair. Definitely, there is a cleaner way to do this.
'''

import argparse
import os
import time
import simplejson
from lxml import etree

# Section parsers
from hqmf_section_parsers import QualityDocumentSectionParser, SubjectOfParser, AuthorParser, CustodianParser, \
    DefinitionParser, ControlVariableParser, TypeIdParser, RelatedDocumentsParser, ComponentParser

dashes = '-' * 150


class HQMFParser(object):

    def __init__(self):
        # Initialize namespaces and dictionaries
        self.NSMAP = {'CQL': 'urn:hhs-cql:hqmf-n1-extensions:v1', 'URN': 'urn:hl7-org:v3',
                      'XSI': "http://www.w3.org/2001/XMLSchema-instance"}
        self.parsed_hqmf_dict = {
            'Identifiers': {'None': {}},
            'Classification': {'None': {}},
            'Administrative and Governance': {'None': {}},
            'Content and Descriptive Metadata': {'None': {}},
            'Usage': {'None': {}},
            'Lifecycle': {'None': {}},
            'Provenance': {'None': {}},
            'Relationships': {'None': {}},
            'Format and Representation': {'None': {}},
        }
        # Initialize parsers
        self.hqmf_parsers= [
            QualityDocumentSectionParser,
            SubjectOfParser,
            AuthorParser,
            CustodianParser,
            DefinitionParser,
            ControlVariableParser,
            TypeIdParser,
            RelatedDocumentsParser,
            ComponentParser,
        ]

    def parse_hqmf(self, extract_directory):
        # Find HQMF file from extracted zip
        xml_file_paths = self.find_hqmf_file(extract_directory)

        # Parse HQMF file to dict
        hqmf_dict = self.parse_hqmf_file(xml_file_paths)

        return hqmf_dict

    @staticmethod
    def find_hqmf_file(extract_directory):
        # Initialize variables
        find_hqmf_start_time = time.time()
        extract_directory = os.walk(extract_directory)
        xml_file_paths = []

        # Iterate through extracts and pull xml file paths
        for dirpath, subdirs, files in extract_directory:
            for file in files:
                if file.endswith('.xml'):
                    xml_file_paths.append(dirpath + '/' + file)

        find_hqmf_end_time = time.time()

        print('Find HQMF File Duration: {0} seconds'.format(find_hqmf_end_time - find_hqmf_start_time))

        return xml_file_paths

    def parse_hqmf_file(self, xml_file_paths):
        # Get HQMF Root
        for xml_file_path in xml_file_paths:

            # Open xml_file and get root
            try:
                xml_tree = etree.parse(xml_file_path)
                self.xml_root = xml_tree.getroot()
            except IOError:
                print('Error while reading XML file: {}'.format(xml_file_path))

            # Check for Quality Measure Document tag
            if self.xml_root.tag == '{urn:hl7-org:v3}QualityMeasureDocument':
                print('HQMF File Path: {}'.format(xml_file_path))
                # Iterate through parsers
                for parser in self.hqmf_parsers:
                    parse_start = time.time()

                    section_parser = parser()

                    self.parsed_hqmf_dict = section_parser.parse(self.NSMAP, self.xml_root, self.parsed_hqmf_dict)

                    parse_end = time.time()

                    print('Section - {0} Parsing Duration: {1} seconds'.format(section_parser.section, parse_end - parse_start))

        return self.parsed_hqmf_dict

    def format_dict(self):
        # Initialize Variables
        temp_dict = dict()
        # Purpose of this messy loop is to format all the datatypes used
        # Iterate through category and values - values are dicts
        for category, values in self.parsed_hqmf_dict.iteritems():
            temp_dict[category] = {}
            # Iterate through groups and values
            for group, value in values.iteritems():
                # Some values may or may not have multiple values and no group
                if group == 'None':
                    for key, items in value.iteritems():
                        # Enumerate is the items are lists with multiple values
                        if isinstance(items, list) and len(items)>1:
                            for idx, item in enumerate(items):
                                enumerated_key = key + ' {0}'.format(idx+1)
                                temp_dict[category][enumerated_key] = item
                        else:
                            # Get value if value is a list with one indice
                            if isinstance(items, list):
                                temp_dict[category][key] = items[0]
                            else:
                                temp_dict[category][key] = items
                # If the group is assigned, do the same except with group key
                else:
                    # Enumerate is the items are lists with multiple values
                    if isinstance(value, list):
                        for idx, item in enumerate(value):
                            enumerated_group = group + ' {0}'.format(idx + 1)
                            temp_dict[category][enumerated_group] = item
                    elif isinstance(value, dict):
                        temp_dict[category].setdefault(group, {})
                        for key, value in value.iteritems():
                            # Get value if value is a list with one indice
                            if isinstance(value, list) and len(value)>1:
                                for idx, item in enumerate(value):
                                    enumerated_key = key + ' {0}'.format(idx + 1)
                                    temp_dict[category][group][enumerated_key] = item
                            elif isinstance(value, str) or value==None:
                                temp_dict[category][group][key] = value
                            else:
                                temp_dict[category][group][key] = value[0]

        self.parsed_hqmf_dict = temp_dict

        return self.parsed_hqmf_dict

    def write_file(self, file_name):
        # Write metadata to a file as JSON for easier reading
        with open(file_name, 'w') as file:
            file.write(simplejson.dumps(self.parsed_hqmf_dict, indent=4, sort_keys=True))


if __name__ == '__main__':
    print(dashes)
    print('Time Statistics')

    # Arg parse
    arg_parse = argparse.ArgumentParser()
    arg_parse.add_argument('--path', help='Path to HQMF extract directory')
    args = arg_parse.parse_args()

    # Test Paths Reference
    # /Users/kenji.wong/PycharmProjects/solutions/hqmf_parser_v2/test_hqmf_extracts/CMS71_v5_4_Artifacts
    # /Users/kenji.wong/PycharmProjects/solutions/hqmf_parser_v2/test_hqmf_extracts/CMS90_v5_4_Artifacts
    # /Users/kenji.wong/PycharmProjects/solutions/hqmf_parser_v2/test_hqmf_extracts/CMS146_v5_4_Artifacts

    # Initialize parser
    hqmf_parser = HQMFParser()

    # Parse and format HQMF zip
    extract_path = args.path
    hqmf_parser.parse_hqmf(extract_path)
    hqmf_parser.format_dict()

    # Write to txt
    file_name = extract_path.split('/')[-1] + '_Parsed.txt'
    hqmf_parser.write_file(file_name)