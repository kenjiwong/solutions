# HQMF Section Parsers
class Utilities(object):

    # Find attribute after navigating to path
    def find_attrib(self, node, new_xpath, attribute):
        new_node = node.xpath(new_xpath, namespaces=self.namespaces)
        if new_node:
            return new_node[0].get(attribute, 'None')
        else:
            return None

    # Find new node
    def find_node(self, node, new_xpath):
        return node.xpath(new_xpath, namespaces=self.namespaces)

    # Get attribute from node
    def get_attrib(self, node, attribute):
        if node:
            return node[0].get(attribute, 'None')
        else:
            return None


class QualityDocumentSectionParser(Utilities):

    def __init__(self):
        self.section = 'Quality Document Section Parser'

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces

        # These values are hard coded at the moment
        xml_format = 'XML 1.0'
        namespaces_dict = {}
        for idx, ns in enumerate(self.namespaces):
            namespaces_dict['Namespace {0}'.format(idx+1)] = ns

        # Append to parsed HQMF dict - Identifiers
        parsed_hqmf_dict['Identifiers']['None']['Document Type'] = self.find_attrib(hqmf_root, 'URN:code/URN:displayName', 'value')
        parsed_hqmf_dict['Identifiers']['None']['Measure Name'] = self.find_attrib(hqmf_root, 'URN:title', 'value')
        parsed_hqmf_dict['Identifiers']['None']['Measure Description'] = self.find_attrib(hqmf_root, 'URN:text', 'value')
        parsed_hqmf_dict['Identifiers']['None']['Globally Unique Measure (Versioned) ID'] = self.find_attrib(hqmf_root, 'URN:id', 'root')
        parsed_hqmf_dict['Identifiers']['None']['Globally Unique Measure ID'] = self.find_attrib(hqmf_root, 'URN:setId', 'root')
        parsed_hqmf_dict['Identifiers']['None']['Measure Version Number'] = self.find_attrib(hqmf_root, 'URN:versionNumber', 'value')

        # Append to parsed HQMF dict - Administrative and Governance
        parsed_hqmf_dict['Administrative and Governance']['None']['Measure Status'] = self.find_attrib(hqmf_root,'URN:statusCode', 'code')

        # Append to parsed HQMF dict - Format and Representation - These values are hard coded at the moment
        parsed_hqmf_dict['Format and Representation']['None']['Format'] = xml_format
        parsed_hqmf_dict['Format and Representation']['Namespaces'] = namespaces_dict

        return parsed_hqmf_dict


class SubjectOfParser(Utilities):

    def __init__(self):
        self.section = 'SubjectOf Section Parser'
        # Dictionary of attribute codes that we are looking for
        self.measure_attributes_codes = {
            'Identifiers': {'None': ['NQF Number', 'eCQM Identifier (Measure Authoring Tool)']},
            'Classification': {'None': ['MSRTYPE', 'MSRSCORE']},
            'Administrative and Governance': {'None': ['COPY', 'DISC']},
            'Content and Descriptive Metadata': {'Populations': ['IPOP', 'DENOM', 'DENEX', 'NUMER', 'NUMEX', 'DENEXCEP'],
                                                 'Supplemental Data Elements': ['SDE'],
                                                 'None': ['STRAT', 'MSRADJ', 'MSRAGG', 'ITMCNT', 'RAT', 'CRS', 'IDUR', 'GUIDE', ]},
            'Usage': {},
            'Lifecycle': {},
            'Provenance': {'None': ['REF']},
            'Relationships': {},
            'Format and Representation': {'None': ['TRANF']},
        }

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces
        self.subjectOf_root = self.find_node(hqmf_root, 'URN:subjectOf')

        for subjectOf in self.subjectOf_root:
            # Iterate through all subjectOf nodes
            while True:
                code_path = self.find_node(subjectOf, 'URN:measureAttribute/URN:code')
                original_text_path = self.find_node(code_path[0], 'URN:originalText')
                original_text = self.get_attrib(original_text_path, 'value')

                # If the original text is within the Identifiers. Do not need to iterate through categories and their codes
                if original_text in self.measure_attributes_codes['Identifiers']['None']:
                    parsed_hqmf_dict['Identifiers']['None'][original_text] = self.find_attrib(code_path[0],'../URN:value', 'value')
                    break

                # Get attribute code
                measure_attribute_code = self.get_attrib(code_path, 'code')

                # Iterate through all categories
                for category, groups in self.measure_attributes_codes.iteritems():
                    for group, code_list in groups.iteritems():
                        if measure_attribute_code in code_list:
                            # Initialize paths
                            code_display_name = self.find_attrib(code_path[0], 'URN:displayName', 'value')
                            value_path = self.find_node(code_path[0], '../URN:value')
                            value_display_name_path = self.find_node(value_path[0], 'URN:displayName')

                            # The ITMCNT is an exception to the rule
                            if measure_attribute_code == 'ITMCNT':
                                value_display_name = self.get_attrib(value_path, 'extension')
                            # If there is a value in the list, then grab its value
                            if value_display_name_path:
                                value_display_name = self.get_attrib(value_display_name_path, 'value')
                            else:
                                value_display_name = self.get_attrib(value_path, 'value')

                            # Set as a list in case there are multiple values for one key.
                            # There are a couple values that take multiple values, but could be cleaned up to only make lists for one with multiple values
                            if not group == 'None':
                                parsed_hqmf_dict[category].setdefault(group, {})
                                parsed_hqmf_dict[category][group].setdefault(code_display_name, []).append(value_display_name)
                            else:
                                parsed_hqmf_dict[category]['None'].setdefault(code_display_name, []).append(value_display_name)

                            break
                break

        return parsed_hqmf_dict


class AuthorParser(Utilities):

    def __init__(self):
        self.section = 'Author Section Parser'

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces
        self.represented_resp_org_root = self.find_node(hqmf_root, 'URN:author/URN:responsibleParty/URN:representedResponsibleOrganization')

        # Parse out author id and author name
        author_id = self.find_attrib(self.represented_resp_org_root[0],'URN:id/URN:item', 'root')
        author_name = self.find_attrib(self.represented_resp_org_root[0], 'URN:name/URN:item/URN:part', 'value')

        # Assign values to output dict
        parsed_hqmf_dict['Administrative and Governance']['None']['Measure Author ID'] = author_id
        parsed_hqmf_dict['Administrative and Governance']['None']['Measure Author Name'] = author_name

        return parsed_hqmf_dict


class CustodianParser(Utilities):

    def __init__(self):
        self.section = 'Custodian Section Parser'

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces
        self.represented_resp_org_root = hqmf_root.xpath('URN:custodian/URN:responsibleParty/URN:representedResponsibleOrganization', namespaces=namespaces)

        # Parse out author id and author name
        custodian_id = self.find_attrib(self.represented_resp_org_root[0], 'URN:id/URN:item', 'root')
        custodian_name = self.find_attrib(self.represented_resp_org_root[0], 'URN:name/URN:item/URN:part', 'value')

        # Assign values to output dict
        parsed_hqmf_dict['Administrative and Governance']['None']['Measure Steward ID'] = custodian_id
        parsed_hqmf_dict['Administrative and Governance']['None']['Measure Steward Name'] = custodian_name

        return parsed_hqmf_dict


class DefinitionParser(Utilities):

    def __init__(self):
        self.section = 'Definition Section Parser'

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces
        self.value_set_root = hqmf_root.xpath('URN:definition/URN:valueSet', namespaces=namespaces)

        # Iterate through value set definitions
        for value_set in self.value_set_root:
            value_set_name = self.find_attrib(value_set, 'URN:title', 'value')
            value_set_oid = self.find_attrib(value_set, 'URN:id', 'root')

            # Create output pair
            name_and_oid = {
                'Value Set Name': value_set_name,
                'Value Set OID': value_set_oid
            }

            # Assign values to output dict
            parsed_hqmf_dict['Content and Descriptive Metadata'].setdefault('Value Set', []).append(name_and_oid)

        return parsed_hqmf_dict


class ControlVariableParser(Utilities):

    def __init__(self):
        self.section = 'Control Variable Section Parser'

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces
        self.control_variable = self.find_node(hqmf_root, 'URN:controlVariable/URN:measurePeriod/URN:value/URN:phase')

        # Pull measure period
        measure_period_start = self.find_attrib(self.control_variable[0], 'URN:low', 'value')
        measure_period_end = self.find_attrib(self.control_variable[0], 'URN:high', 'value')

        # Pull duration
        duration_node = self.find_node(self.control_variable[0], 'URN:width')[0]
        duration_value = duration_node.get('value')
        duration_unit = duration_node.get('unit')

        # Assign values to output dict
        parsed_hqmf_dict['Content and Descriptive Metadata'].setdefault('Measure Period', dict())
        parsed_hqmf_dict['Content and Descriptive Metadata']['Measure Period']['Measure Period Start'] = measure_period_start
        parsed_hqmf_dict['Content and Descriptive Metadata']['Measure Period']['Measure Period End'] = measure_period_end
        parsed_hqmf_dict['Content and Descriptive Metadata']['Measure Period']['Measure Duration'] = duration_value + duration_unit

        return parsed_hqmf_dict


class TypeIdParser(Utilities):

    def __init__(self):
        self.section = 'Type Id Section Parser'

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces
        self.typeId = self.find_node(hqmf_root, 'URN:typeId')

        # Pull Quality Measure Document Type Root
        quality_measure_document_type = self.get_attrib(self.typeId[0], 'extension')

        # Assign values to output dict
        parsed_hqmf_dict['Format and Representation']['None']['Quality Measure Document Type'] = quality_measure_document_type

        return parsed_hqmf_dict


class RelatedDocumentsParser(Utilities):

    def __init__(self):
        self.section = 'Related Documents Section Parser'

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces
        self.related_documents_root = self.find_node(hqmf_root, 'URN:relatedDocument')

        related_documents_cql = []

        # Pull related document values
        for related_document in self.related_documents_root:
            cql_file_name = self.find_attrib(related_document, 'URN:expressionDocument/URN:text/URN:reference','value')
            related_documents_cql.append(cql_file_name)

        # Assign values to output dict
        parsed_hqmf_dict['Relationships'].setdefault('Assets Used', dict())
        parsed_hqmf_dict['Relationships']['Assets Used']['Related CQL File'] = related_documents_cql

        return parsed_hqmf_dict


class ComponentParser(Utilities):

    def __init__(self):
        self.section = 'Component Section Parser'

    def parse(self, namespaces, hqmf_root, parsed_hqmf_dict):
        self.namespaces = namespaces
        self.population_criteria_root = self.find_node(hqmf_root, 'URN:component/URN:populationCriteriaSection/URN:component')
        self.data_criteria_root = self.find_node(hqmf_root, 'URN:component/URN:dataCriteriaSection/URN:entry/URN:localVariableName')
        self.measure_observation_root = self.find_node(hqmf_root, 'URN:component/URN:measureObservationSection/URN:definition/URN:measureObservationDefinition/URN:methodCode/URN:item')
        cql_variables = []

        # Pull population criteria
        for variable in self.population_criteria_root:
            data_criteria_component = self.find_attrib(variable.getchildren()[0], 'URN:precondition/URN:criteriaReference/URN:id', 'extension')
            cql_variables.append(data_criteria_component)

        # Pull data criteria
        for variable in self.data_criteria_root:
            cql_variables.append(variable.get('value', 'None'))

        # Pull measure type
        measure_type = self.get_attrib(self.measure_observation_root, 'code')

        # Assign values to output dict
        parsed_hqmf_dict['Content and Descriptive Metadata'].setdefault('Data Elements Used or Contained', dict())
        parsed_hqmf_dict['Content and Descriptive Metadata']['Data Elements Used or Contained']['Data Element'] = cql_variables
        parsed_hqmf_dict['Classification']['None']['Measure Method Code'] = measure_type

        return parsed_hqmf_dict
