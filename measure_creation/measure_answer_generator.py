import csv

scoring_type = 'Proportion'
answer_configuration = {
    'patientIdStart': 90000000,
    'DidNotMeet': 5,
    'InitialPopulation': 10,
    'Denominator': 10,
    'DenominatorExclusions': 5,
    'DenominatorExceptions': 15,
    'Numerator': 20
}

print("Program Start")

result_definitions = {
    'DidNotMeet': [0,0,0,0,0],
    'InitialPopulation': [1,0,0,0,0],
    'Denominator': [1,1,0,0,0],
    'DenominatorExclusions': [1,1,1,0,0],
    'DenominatorExceptions': [1,1,0,1,0],
    'Numerator': [1,1,0,0,1]
}

number_of_outputs = 0
for key, value in answer_configuration.iteritems():
    if not key == 'patientIdStart':
        number_of_outputs += value

print ('Total Outputs: {0}'.format(number_of_outputs))

patientIdstart = answer_configuration['patientIdStart']

with open('output.csv', 'w') as file:
    writer = csv.writer(file)
    writer.writerow(["patientId", "InitialPopulation", "Denominator", "DenominatorExclusions", "DenominatorExceptions", "Numerator"])
    for key, value in answer_configuration.iteritems():
        if not key == 'patientIdStart':
            for i in range(value):
                writer.writerow([patientIdstart]+result_definitions[key])
                patientIdstart +=1

print('Finished')