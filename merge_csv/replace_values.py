import random

# reading the CSV file
text = open("combined_csv.csv", "r")

provider_names = ["Dr. Taylor, Mayson",
                "Dr. Moses, Mathew",
                "Dr. Dorsey, Daniyal",
                "Dr. Kirk, Keira",
                "Dr. Burt, Leen",
                "Dr. Clements, Dotty",
                "Dr. Leech, Daniela",
                "Dr. Gilbert, Millie-Mae",]

# join() method combines all contents of
# csvfile.csv and formed as a string
text = ''.join([i for i in text])

# search and replace the contents
for i in range(1,41):
    text = text.replace("Hospital {0}".format(i), random.choice(provider_names))

text = text.replace("FHIR Query", "Manual Upload")


# text = text.replace("EmployeeName", "EmpName")
# text = text.replace("EmployeeNumber", "EmpNumber")
# text = text.replace("EmployeeDepartment", "EmpDepartment")
# text = text.replace("lined", "linked")

# output.csv is the output file opened in write mode
x = open("output.csv", "w")

# all the replaced text is written in the output.csv file
x.writelines(text)
x.close()