from clientpymaster.fhirclient.models.fhirelementfactory import FHIRElementFactory
import os
import json
import argparse

"""
Author: Kenji Wong
Date: 9/26/2018
Purpose: Build a translator that takes objects from the FHIR Client module and builds a dictionary. 
From there, we add meta data used to execute CQL using FHIR 
Notes: Currently uses the STU-3 FHIR model
Total Number of Files Read: 2484
Total Apervita Failed Parsed Files: 192 --> Failing due to model requirements
Total JSON Failed Parsed Files: 121 --> Json.load failing to load file
+---------+-------------+---------------------------+
| Version |    FHIR     |                           |
+---------+-------------+---------------------------+
| 3.0.0   |       3.0.0 | (STU-3)                   |
| x.x     |       1.8.0 | (STU-3 Ballot, Jan 2017)  |
| x.x     |       1.6.0 | (STU-3 Ballot, Sep 2016)  |
| 1.0.3   |       1.0.2 | (DSTU 2)                  |
| 1.0     |       1.0.1 | (DSTU 2)                  |
| 0.5     |  0.5.0.5149 | (DSTU 2 Ballot, May 2015) |
| 0.0.4   | 0.0.82.2943 | (DSTU 1)                  |
| 0.0.3   | 0.0.82.2943 | (DSTU 1)                  |
| 0.0.2   | 0.0.82.2943 | (DSTU 1)                  |
+---------+-------------+---------------------------+
"""


class FHIRApervitaTranslator(object):

    def as_apervita_dict(self, fhir_object):
        """ Serializes to JSON by inspecting `elementProperties()` and creating
        a JSON dictionary of all registered properties. Checks:

        - whether required properties are not None (and lists not empty)
        - whether not-None properties are of the correct type

        :raises: FHIRValidationError if properties have the wrong type or if
            required properties are empty
        :returns: A validated dict object that can be JSON serialized
        """
        js = {}
        errs = []

        # JSONify all registered properties
        found = set()
        nonoptionals = set()

        js['apervita_meta'] = {'id': fhir_object.__class__.__name__}

        if hasattr(fhir_object, 'elementProperties'):
            for name, jsname, typ, is_list, of_many, not_optional in fhir_object.elementProperties():
                if not_optional:
                    nonoptionals.add(of_many or jsname)

                err = None
                value = getattr(fhir_object, name)
                if value is None:
                    continue

                if is_list:
                    if not isinstance(value, list):
                        err = TypeError("Expecting property \"{}\" on {} to be list, but is {}"
                                        .format(name, type(fhir_object), type(value)))
                    elif len(value) > 0:
                        if not fhir_object._matches_type(value[0], typ):
                            err = TypeError("Expecting property \"{}\" on {} to be {}, but is {}"
                                            .format(name, type(fhir_object), typ, type(value[0])))
                        else:
                            lst = []
                            for v in value:
                                try:
                                    if hasattr(v, 'as_json'):
                                        apervita_fhir_component = self.as_apervita_dict(v)
                                        apervita_fhir_component['apervita_meta'] = {'id': v.__class__.__name__}
                                    else:
                                        apervita_fhir_component = v
                                    lst.append(apervita_fhir_component)
                                except FHIRValidationError as e:
                                    err = e.prefixed(str(len(lst))).prefixed(name)
                            found.add(of_many or jsname)
                            js[jsname] = lst
                else:
                    if not fhir_object._matches_type(value, typ):
                        err = TypeError("Expecting property \"{}\" on {} to be {}, but is {}"
                                        .format(name, type(fhir_object), typ, type(value)))
                    else:
                        try:
                            found.add(of_many or jsname)
                            if hasattr(value, 'as_json'):
                                apervita_fhir_component = self.as_apervita_dict(value)
                                if isinstance(apervita_fhir_component, dict):
                                    apervita_fhir_component['apervita_meta'] = {'id': value.__class__.__name__}
                                else:
                                    apervita_fhir_component = {'value': apervita_fhir_component, 'apervita_meta': {'id': value.__class__.__name__}}

                                js[jsname] = apervita_fhir_component
                            else:
                                js[jsname] = value
                        except FHIRValidationError as e:
                            err = e.prefixed(name)

                if err is not None:
                    errs.append(err if isinstance(err, FHIRValidationError) else FHIRValidationError([err], name))
            # any missing non-optionals?
            if len(nonoptionals - found) > 0:
                for nonop in nonoptionals - found:
                    errs.append(KeyError("Property \"{}\" on {} is not optional, you must provide a value for it"
                                         .format(nonop, fhir_object)))
            if len(errs) > 0:
                raise FHIRValidationError(errs)
            return js
        else:
            # Exception - FHIR Dates have their own as_json.
            # For the moment, we found that FHIR dates were the only exception to FHIRAbstractBase inheritance
            # It has its own as_json function and no element properties
            if fhir_object.origval is not None:
                return fhir_object.origval
            return fhir_object.isostring

    def translation_test(self, source, destination):
        # Counting errors on example files from https://www.hl7.org/fhir/downloads.html
        apervita_fail_count = 0
        json_fail_count = 0
        errors_apervita = []

        # Iterate through all files in directory.
        for subdir, dirs, files in os.walk(source):
            for file in files:
                # Only take files that end with json
                if file.endswith('.json'):
                    with open(os.path.join(subdir, file)) as f:
                        # Many of the files had issues with loading as JSON even though they were json files
                        try:
                            current_patient_json = json.load(f)
                        except:
                            json_fail_count += 1
                        # Some files had some validation issues with attributes missing from base classes
                        # e.g 'Non-optional property "base" on <object> is missing'
                        try:
                            current_resource = FHIRElementFactory.instantiate(current_patient_json['resourceType'], current_patient_json)
                            current_apervita_resource = self.as_apervita_dict(current_resource)
                            with open(destination+'/Apervita_'+file, 'w') as output:
                                json.dump(current_apervita_resource, output)
                        except:
                            apervita_fail_count += 1
                            errors_apervita.append(file)

        # Write errored files to a text file
        with open('error_list.txt', 'w') as f:
            for error in sorted(errors_apervita):
                f.write(str(error)+'\n')

        # Print some information about how many were read and how many failed
        print('Total Number of Files Read: {0}'.format(len(files)))
        print('Total Apervita Failed Parsed Files: {0}'.format(apervita_fail_count))
        print('Total JSON Failed Parsed Files: {0}'.format(json_fail_count))


class FHIRValidationError(Exception):
    """ Exception raised when one or more errors occurred during model
    validation.
    """

    def __init__(self, errors, path=None):
        """ Initializer.

        :param errors: List of Exception instances. Also accepts a string,
            which is converted to a TypeError.
        :param str path: The property path on the object where errors occurred
        """
        if not isinstance(errors, list):
            errors = [TypeError(errors)]
        msgs = "\n  ".join([str(e).replace("\n", "\n  ") for e in errors])
        message = "{}:\n  {}".format(path or "{root}", msgs)

        super(FHIRValidationError, self).__init__(message)

        self.errors = errors
        """ A list of validation errors encountered. Typically contains
        TypeError, KeyError, possibly AttributeError and others. """

        self.path = path
        """ The path on the object where the errors occurred. """

    def prefixed(self, path_prefix):
        """ Creates a new instance of the receiver, with the given path prefix
        applied. """
        path = '{}.{}'.format(path_prefix, self.path) if self.path is not None else path_prefix
        return self.__class__(self.errors, path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create parameters for Translator')
    parser.add_argument('-s', '--source')
    parser.add_argument('-d', '--destination')
    args = parser.parse_args()

    fhir_apervita_translator = FHIRApervitaTranslator()
    fhir_apervita_translator.translation_test(args.source, args.destination)
