import pandas as pd
import numpy


class TJCDataRanker(object):

    def __init__(self, file_path, oms_file_path, delimiter):
        self.data_frame = pd.read_csv(file_path, delimiter=delimiter)
        self.months_data_frame = pd.DataFrame.from_dict(dict(
                                Report_Month = [1, 2, 3, 4 , 5, 6 , 7, 8, 9, 10, 11, 12],
                                Calendar_Month=['January', 'February', 'March', 'April', 'May', 'June', 'July',
                                'August', 'September', 'October', 'November', 'December']))
        self.quarters = ['Q1', 'Q2', 'Q3', 'Q4']
        self.oms_data_frame = pd.read_csv(oms_file_path, delimiter=delimiter)

    def get_unique_year_measure(self):
        self.measure_names = self.data_frame.Measure_Label.unique()
        self.improvement_notation = {"Increase": ['eSTK6', 'eVTE1', 'eVTE2', 'eSTK2', 'eSTK5', 'eSTK3', 'ePC05',
                                         'eEHDI1a', 'eAMI8a', 'eCAC3'],
                                     "Decrease" : ['eED1a', 'eED1b', 'eED1c', 'eED2a', 'eED2b', 'eED2c', 'ePC01',]}
        self.years = sorted(self.data_frame.Report_Year.unique())

    def get_unique_hco_ids(self):
        self.hco_ids = self.data_frame.HCO_ID.unique()

    def filter_measure_and_rank(self):
        self.data_frame["Raw_Value_Rank_Proportion"] = self.data_frame.groupby(self.data_frame.Measure_Label).\
            Observed_Rate.rank(ascending=True, method='dense')
        self.data_frame["Raw_Value_Rank_CV"] = self.data_frame.groupby(self.data_frame.Measure_Label).\
            Median_Observed_Value.rank(ascending=True, method='dense')

    def merge_ranks_and_values(self):
        self.data_frame["Rank"] = self.data_frame["Raw_Value_Rank_Proportion"].map(str) + self.data_frame["Raw_Value_Rank_CV"].map(str)
        self.data_frame["Rank"] = self.data_frame["Rank"].str.replace("nan", "").replace(".0", "")
        self.data_frame["Improvement_Notation"] = self.data_frame.apply(lambda row: self.add_improvement_notation(row.Measure_Label), axis=1)
        self.data_frame["Measure_Value"] = self.data_frame["Observed_Rate"].map(str) + self.data_frame[
            "Median_Observed_Value"].map(str)
        self.data_frame["Measure_Value"] = self.data_frame["Measure_Value"].str.replace("nan", "")
        self.data_frame = pd.DataFrame.merge(self.data_frame, self.months_data_frame, on='Report_Month', how='left')

    def merge_oms_data(self):
        self.data_frame = pd.DataFrame.merge(self.data_frame, self.oms_data_frame, on='HCO_ID', how='left')

    def add_improvement_notation(self, measure_name):
        if measure_name in self.improvement_notation["Increase"]:
            return "Increase"
        elif measure_name in self.improvement_notation["Decrease"]:
            return "Decrease"

    def write_to_csv(self):
        self.data_frame.index.name = 'Index'
        self.data_frame.to_csv('Ranked_HCO_Feedback_Data_{}.csv'.format('_'.join([str(year) for year in self.years])))

    def main(self):
        self.get_unique_year_measure()
        self.get_unique_hco_ids()
        self.filter_measure_and_rank()
        self.merge_ranks_and_values()
        self.merge_oms_data()
        self.write_to_csv()


if __name__ == '__main__':
    app_ranker = TJCDataRanker('/Users/kenji.wong/PycharmProjects/solutions/tjc_hco_feedback/HCO_Feedback_Data_2017_2018.csv', '/Users/kenji.wong/PycharmProjects/solutions/tjc_hco_feedback/Hospital_Information.csv', ',')
    app_ranker.main()
