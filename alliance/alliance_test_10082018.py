'''
Author: Kenji Wong
Date: 8/30/2018
Purpose: Create testing script for Alliance
'''

import hmac
from Crypto.Hash import SHA256
import datetime
import requests
import time
import json
import numpy as np



public_key = 'ecd81339b064b9d4126af320c83201896430a729'
# public_key = 'eac5e62326501e12a624c6abdab9c2cc9e8dd5bf'
# private_key = getpass.getpass("Enter your Private Key: ")
private_key = '638912a252b85cf83e2dd777a4af0856c18d766f'
# private_key = 'b02cf4f4564d52fccc5077c01378f8f1c38add8f'
cds_analytic_id = 'gcb-aqkc5d.1.0'
# cds_analytic_id = 'gcb-aqkc8r.1.0'
cds_input_adapter_id = '46wr-aqkg23.1.0'
cds_output_adapter_id = '6d7Qk4S6Tmmtbw2GgwGwmkYQ'
test_patient_json = {
                "events": [
                {
                    "subjectId": "67890",
                    "birthDate": "7/2/1986",
                    "gender": "male",
                    "first": "Paul",
                    "last": "Welles",
                    "mrn": "98201232",
                },
                {
                    "observationId": "O-4",
                    "patientReference": "Patient/67890",
                    "subjectId": "67890",
                    "codeLOINC": "60256-5",
                    "effectiveDateTime": "4/20/2018 12:00",
                    "status": "final",
                    "interpretation": "POS",
                    "bodySite": "",
                    "text": "Neisseria gonorrhoeae rRNA [Presence] in Urine by Probe and target amplification method"
                },
        ]}

test_patient_2 = {
    "apple": "123728"
}

alliance_test_patient = {
    "events": [
      {
        "patientid": "152509",
        "birthDate": "1990-01-01T00:00:00",
        "gender": "male",
        "first": "Jose",
        "last": "Test",
        "mrn": ""
      },
      {
        "resourceType": "Condition 1",
        "cnd_id": "1852538745872900",
        "patientReference": "152509",
        "patientid": "071d8b10923a488b9475bc9dc520d1a5",
        "code": [
          {
            "system": "SNOMED",
            "code": "366979004",
            "display": "Sadness"
          },
          1
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "F32.9",
            "display": "Major depressive disorder, single episode, unspecified"
          },
          1
        ],
        "date": "2018-09-14T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 2",
        "cnd_id": "1852538917873580",
        "patientReference": "152509",
        "patientid": "071d8b10923a488b9475bc9dc520d1a5",
        "code": [
          {
            "system": "SNOMED",
            "code": "366979004",
            "display": "Sadness"
          },
          2
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "F32.9",
            "display": "Major depressive disorder, single episode, unspecified"
          },
          2
        ],
        "date": "2018-09-14T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "medicationId": "1839161377342990",
        "patientReference": "152509",
        "patientid": "071d8b10923a488b9475bc9dc520d1a5",
        "codeableConcept": [
          {
            "system": "NDC",
            "code": "61958070101",
            "display": "TRUVADA 200-300 MG ORAL TABLET"
          },
          1
        ],
        "code2": [
          {
            "system": "GPI",
            "code": "12109902300320",
            "display": "TRUVADA 200-300 MG ORAL TABLET"
          },
          1
        ],
        "date": "2018-04-12T14:09:37.037-06:00",
        "clinicalStatus": "active"
      }
    ]
  }

alliance_test_patient_2 = {
  "subject": {
    "subjectid": "d64fe3a2-2800-4c4f-aa0c-c7ccf473ba5e",
    "events": [
      {
        "subjectid": "152508",
        "birthDate": "1985-11-07T00:00:00",
        "gender": "female",
        "first": "Candy",
        "last": "Test",
        "mrn": ""
      },
      {
        "observationId": "2719",
        "patientReference": "patient/152508",
        "subjectId": "152508",
        "codeLOINC": "2719",
        "effectiveDateTime": "2018-09-27T09:23:40.040-06:00",
        "status": "final",
        "interpretation": "Reactive",
        "bodySite": "",
        "text": "GC DNA PROBE"
      },
      {
        "observationId": "3399",
        "patientReference": "patient/152508",
        "subjectId": "152508",
        "codeLOINC": "3399",
        "effectiveDateTime": "2018-09-27T09:23:54.054-06:00",
        "status": "final",
        "interpretation": "Negative",
        "bodySite": "",
        "text": "HIV AB"
      },
      {
        "allergyIntoleranceId": "1854005264906990",
        "patientReference": "patient/152508",
        "subjectId": "152508",
        "codeNDC": "00093716956",
        "codeGPI": "0340001000",
        "clinicalStatus": "active",
        "verificationStatus": "confirmed",
        "allergyIntoleranceCategory": "medication",
        "substance": "AZITHROMYCIN"
      },
      {
        "resourceType": "Condition 1",
        "cnd_id": "1837089561410670",
        "code": [
          {
            "system": "SNOMED",
            "code": "237285000",
            "display": "Gestational edema"
          },
          1
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "O12.01",
            "display": "Gestational edema, first trimester"
          },
          1
        ],
        "date": "2018-03-19T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 2",
        "cnd_id": "1837248516022390",
        "code": [
          {
            "system": "SNOMED",
            "code": "237285000",
            "display": "Gestational edema"
          },
          2
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "O12.01",
            "display": "Gestational edema, first trimester"
          },
          2
        ],
        "date": "2018-03-21T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 3",
        "cnd_id": "1842186337490610",
        "code": [
          {
            "system": "SNOMED",
            "code": "237285000",
            "display": "Gestational edema"
          },
          3
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "O12.01",
            "display": "Gestational edema, first trimester"
          },
          3
        ],
        "date": "2018-05-17T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 4",
        "cnd_id": "1843204783478430",
        "code": [
          {
            "system": "SNOMED",
            "code": "237285000",
            "display": "Gestational edema"
          },
          4
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "O12.01",
            "display": "Gestational edema, first trimester"
          },
          4
        ],
        "date": "2018-05-28T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 5",
        "cnd_id": "1844434240920600",
        "code": [
          {
            "system": "SNOMED",
            "code": "237285000",
            "display": "Gestational edema"
          },
          5
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "O12.01",
            "display": "Gestational edema, first trimester"
          },
          5
        ],
        "date": "2018-06-12T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 6",
        "cnd_id": "1850995769761610",
        "code": [
          {
            "system": "SNOMED",
            "code": "237285000",
            "display": "Gestational edema"
          },
          6
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "O12.01",
            "display": "Gestational edema, first trimester"
          },
          6
        ],
        "date": "2018-08-27T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 7",
        "cnd_id": "1851073758192430",
        "code": [
          {
            "system": "SNOMED",
            "code": "237285000",
            "display": "Gestational edema"
          },
          7
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "O12.01",
            "display": "Gestational edema, first trimester"
          },
          7
        ],
        "date": "2018-08-28T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 8",
        "cnd_id": "1854005321907090",
        "code": [
          {
            "system": "SNOMED",
            "code": "237285000",
            "display": "Gestational edema"
          },
          8
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "O12.01",
            "display": "Gestational edema, first trimester"
          },
          8
        ],
        "date": "2018-09-03T00:00:00.000-06:00",
        "clinicalStatus": "active"
      }
    ]
  }
}

alliance_test_patient_3 = {
  "subject": {
    "subjectid": "12005fc19dd14f3b887975f7fb1f6cc9",
    "events": [
      {
        "subjectid": "152509",
        "birthDate": "1990-01-01T00:00:00",
        "gender": "male",
        "first": "Jose",
        "last": "Test",
        "mrn": ""
      },
      {
        "observationId": "308",
        "patientReference": "152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeLOINC": "20507-0",
        "effectiveDateTime": "2016-10-01T14:50:52.052-06:00",
        "status": "final",
        "interpretation": "Reactive",
        "bodySite": "",
        "text": "RPR"
      },
      {
        "observationId": "2719",
        "patientReference": "152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeLOINC": "698-1",
        "effectiveDateTime": "2014-10-10T14:51:08.008-06:00",
        "status": "final",
        "interpretation": "Non-Reactive",
        "bodySite": "",
        "text": "GC DNA PROBE"
      },
      {
        "observationId": "2719",
        "patientReference": "152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeLOINC": "698-1",
        "effectiveDateTime": "2018-10-01T14:41:09.009-06:00",
        "status": "final",
        "interpretation": "Reactive",
        "bodySite": "",
        "text": "GC DNA PROBE"
      },
      {
        "observationId": "14722",
        "patientReference": "152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeLOINC": "21192-0",
        "effectiveDateTime": "2018-10-01T14:41:09.009-06:00",
        "status": "final",
        "interpretation": "Reactive",
        "bodySite": "",
        "text": "CHLAMYD DNA"
      },
      {
        "observationId": "3399",
        "patientReference": "152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeLOINC": "80203-3",
        "effectiveDateTime": "2018-10-01T14:41:09.009-06:00",
        "status": "final",
        "interpretation": "Not Reactive",
        "bodySite": "",
        "text": "HIV AB"
      },
      {
        "allergyIntoleranceId": "1856184387319510",
        "patientReference": "patient/152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeNDC": "",
        "codeGPI": "0110000000",
        "clinicalStatus": "active",
        "verificationStatus": "confirmed",
        "allergyIntoleranceCategory": "medication",
        "substance": "PENICILLIN"
      },
      {
        "allergyIntoleranceId": "1856184387319510",
        "patientReference": "patient/152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeNDC": "",
        "codeGPI": "6610002000",
        "clinicalStatus": "active",
        "verificationStatus": "confirmed",
        "allergyIntoleranceCategory": "medication",
        "substance": "PENICILLIN"
      },
      {
        "resourceType": "Condition 1",
        "cnd_id": "1854105956729450",
        "patientReference": "152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "code": [
          {
            "system": "SNOMED",
            "code": "44054006",
            "display": "NCDMM"
          },
          1
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "E11.9",
            "display": "Type 2 diabetes mellitus without complications"
          },
          1
        ],
        "date": "2018-04-12T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "medicationId": "1839161377342990",
        "patientReference": "152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeableConcept": [
          {
            "system": "NDC",
            "code": "61958070101",
            "display": "TRUVADA 200-300 MG ORAL TABLET"
          },
          1
        ],
        "code2": [
          {
            "system": "GPI",
            "code": "12109902300320",
            "display": "TRUVADA 200-300 MG ORAL TABLET"
          },
          1
        ],
        "date": "2018-04-12T14:09:37.037-06:00",
        "clinicalStatus": "active"
      },
      {
        "medicationId": "1856184632319620",
        "patientReference": "152509",
        "subjectId": "12005fc19dd14f3b887975f7fb1f6cc9",
        "codeableConcept": [
          {
            "system": "NDC",
            "code": "00143314205",
            "display": "DOXYCYCLINE HYCLATE 100 MG ORAL CAPSULE"
          },
          2
        ],
        "code2": [
          {
            "system": "GPI",
            "code": "04000020100110",
            "display": "DOXYCYCLINE HYCLATE 100 MG ORAL CAPSULE"
          },
          2
        ],
        "date": "2018-10-01T14:41:09.009-06:00",
        "clinicalStatus": "active"
      }
    ]
  }
}

paul = {
  "subject": {
    "subjectid": "ffbccd459ad44459979f53c4f559abb1",
    "events": [
      {
        "subjectid": "167198",
        "birthDate": "1999-10-04T00:00:00",
        "gender": "male",
        "first": "Bob",
        "last": "Welles",
        "mrn": ""
      },
      {
        "observationId": "2719",
        "patientReference": "167198",
        "subjectId": "ffbccd459ad44459979f53c4f559abb1",
        "codeLOINC": "698-1",
        "effectiveDateTime": "2018-11-19T15:21:48.048-06:00",
        "status": "final",
        "interpretation": "Positive",
        "bodySite": "",
        "text": "GC DNA PROBE"
      },
      {
        "observationId": "14722",
        "patientReference": "167198",
        "subjectId": "ffbccd459ad44459979f53c4f559abb1",
        "codeLOINC": "21192-0",
        "effectiveDateTime": "2018-11-19T15:21:49.049-06:00",
        "status": "final",
        "interpretation": "Positive",
        "bodySite": "",
        "text": "CHLAMYD DNA"
      },
      {
        "resourceType": "Condition 1",
        "cnd_id": "1859816704467800",
        "patientReference": "167198",
        "subjectId": "ffbccd459ad44459979f53c4f559abb1",
        "code": [
          {
            "system": "SNOMED",
            "code": "54150009",
            "display": "Upper RTI"
          },
          1
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "J06.9",
            "display": "Acute upper respiratory infection, unspecified"
          },
          1
        ],
        "date": "2018-06-06T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 2",
        "cnd_id": "1859816704467860",
        "patientReference": "167198",
        "subjectId": "ffbccd459ad44459979f53c4f559abb1",
        "code": [
          {
            "system": "SNOMED",
            "code": "54150009",
            "display": "Upper RTI"
          },
          2
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "J06.9",
            "display": "Acute upper respiratory infection, unspecified"
          },
          2
        ],
        "date": "2018-06-06T00:00:00.000-06:00",
        "clinicalStatus": "active"
      }
    ]
  }
}



class ApiClient(object):

    def __init__(self):
        # Initialize attributes on object
        self.api_key = public_key
        self.private_key = private_key
        self.http_method = 'https://'
        self.url = 'www.apervita.net'

    def pathway_single_subject_calculator(self, json, insightid, inputadapterid, outputadapterid):
        # Designate POST parameters for signature
        rest_type = 'post'
        path = '/api/v2/pathway/json'

        response = self.assemble_call(rest_type=rest_type, path=path, json=json, inputadapterid=inputadapterid, outputadapterid=outputadapterid, insightid=insightid)

        return response

    # def pathway_single_subject_calculator(self, json, insightid, inputadapterid):
    #     # Designate POST parameters for signature
    #     rest_type = 'post'
    #     path = '/api/v2/pathway/json'
    #
    #     response = self.assemble_call(rest_type=rest_type, path=path, json=json, inputadapterid=inputadapterid, insightid=insightid)
    #
    #     return response

    def single_subject_calculator(self, json, insightid):
        # Designate POST parameters for signature
        rest_type = 'post'
        path = '/api/v2/subject/calculate/json'

        response = self.assemble_call(rest_type=rest_type, path=path, json=json, insightid=insightid)

        return response

    def assemble_call(self, rest_type, path, file_source_path=None, json=None, **kwargs):
        # Get current time stamp
        current_timestamp = datetime.datetime.utcnow().replace(microsecond=0).isoformat()

        # Create base parameters in every call
        parameters = {'api_key': self.api_key, 'timestamp': current_timestamp}

        for key, value in kwargs.iteritems():
            parameters[key] = value

        response = self.make_call(rest_type, path, parameters, file_source_path, json)

        return response

    def make_call(self, rest_type, path, parameters, file_source_path=None, json=None):
        dashes = '-' * 150
        print(dashes)

        # Create signature
        url_parameters = self.add_signature(rest_type, path, parameters)

        # Create URL with parameters and signature
        url = self.create_url(path, url_parameters)

        # Make either GET or POST
        if rest_type == 'post':
            print('Method: POST')
            print('Final URL: {0}'.format(url))

            # Open up file and make post request
            if file_source_path:
                upload_file = open(file_source_path, 'rb')
                response = requests.post(url, files={'uploadfile': upload_file})
            elif json:
                response = requests.post(url, json=json)
            else:
                response = requests.post(url)

            # print response
            print('Request Headers: ' + str(response.request.headers))
            print('Response: ' + response.text)
        elif rest_type == 'get':
            print('Method: GET')
            print('Final URL: {0}'.format(url))

            # Make get request
            response = requests.get(url)

            # Print response
            print('Request Headers: ' + str(response.request.headers))
            print('Response: ' + response.text)
        else:
            response = None
            print('Unknown method')

        print(dashes)

        return response

    def add_signature(self, rest_type, path, parameters):
        # Create parameters list in alphabetical order
        parameters_list = ['{0}={1}'.format(key, parameters[key]) for key in sorted(parameters.keys())]

        # Join parameters and rest of the URL
        signature_list = [rest_type, self.url, path, '&'.join(parameters_list)]

        # Join by pipes and lowercase inputs
        prep_sign = '|'.join(signature_list).lower()
        print('Prepare Signature: {0}'.format(prep_sign))

        # Create signature SHA-256
        signature = hmac.new(self.private_key, prep_sign, SHA256).hexdigest()

        # Add signature to dict
        parameters['signature'] = signature

        return parameters

    def create_url(self, path, parameters):
        # Sort parameters
        parameters_list = ['{0}={1}'.format(key, parameters[key]) for key in sorted(parameters.keys())]

        # Join with the rest of parameters
        url_list = [self.http_method, self.url, path+'?', '&'.join(parameters_list)]

        # Create URL
        url = ''.join(url_list)

        return url


paul = {
  "subject": {
    "subjectid": "ffbccd459ad44459979f53c4f559abb1",
    "events": [
      {
        "subjectid": "167198",
        "birthDate": "1999-10-04T00:00:00",
        "gender": "male",
        "first": "Paul",
        "last": "Welles",
        "mrn": ""
      },
      {
        "observationId": "2719",
        "patientReference": "167198",
        "subjectId": "ffbccd459ad44459979f53c4f559abb1",
        "codeLOINC": "698-1",
        "effectiveDateTime": "2018-11-19T15:21:48.048-06:00",
        "status": "final",
        "interpretation": "Positive",
        "bodySite": "",
        "text": "GC DNA PROBE"
      },
      {
        "observationId": "14722",
        "patientReference": "167198",
        "subjectId": "ffbccd459ad44459979f53c4f559abb1",
        "codeLOINC": "21192-0",
        "effectiveDateTime": "2018-11-19T15:21:49.049-06:00",
        "status": "final",
        "interpretation": "Positive",
        "bodySite": "",
        "text": "CHLAMYD DNA"
      },
      {
        "resourceType": "Condition 1",
        "cnd_id": "1859816704467800",
        "patientReference": "167198",
        "subjectId": "ffbccd459ad44459979f53c4f559abb1",
        "code": [
          {
            "system": "SNOMED",
            "code": "54150009",
            "display": "Upper RTI"
          },
          1
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "J06.9",
            "display": "Acute upper respiratory infection, unspecified"
          },
          1
        ],
        "date": "2018-06-06T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 2",
        "cnd_id": "1859816704467860",
        "patientReference": "167198",
        "subjectId": "ffbccd459ad44459979f53c4f559abb1",
        "code": [
          {
            "system": "SNOMED",
            "code": "54150009",
            "display": "Upper RTI"
          },
          2
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "J06.9",
            "display": "Acute upper respiratory infection, unspecified"
          },
          2
        ],
        "date": "2018-06-06T00:00:00.000-06:00",
        "clinicalStatus": "active"
      }
    ]
  }
}


mary = {
  "subject": {
    "subjectid": "edf8c65009e54024b831c70ae3012b86",
    "events": [
      {
        "subjectid": "167199",
        "birthDate": "1994-08-07T00:00:00",
        "gender": "female",
        "first": "Mary",
        "last": "Contrary",
        "mrn": ""
      },
      {
        "observationId": "308",
        "patientReference": "167199",
        "subjectId": "edf8c65009e54024b831c70ae3012b86",
        "codeLOINC": "20507-0",
        "effectiveDateTime": "2018-11-20T15:57:12.012-06:00",
        "status": "final",
        "interpretation": "Negative",
        "bodySite": "",
        "text": "RPR"
      },
      {
        "observationId": "3399",
        "patientReference": "167199",
        "subjectId": "edf8c65009e54024b831c70ae3012b86",
        "codeLOINC": "80203-3",
        "effectiveDateTime": "2018-09-20T15:50:23.023-06:00",
        "status": "final",
        "interpretation": "Negative",
        "bodySite": "",
        "text": "HIV AB"
      },
      {
        "observationId": "2719",
        "patientReference": "167199",
        "subjectId": "edf8c65009e54024b831c70ae3012b86",
        "codeLOINC": "698-1",
        "effectiveDateTime": "2018-10-20T15:36:07.007-06:00",
        "status": "final",
        "interpretation": "Positive",
        "bodySite": "15058008",
        "text": "GC DNA PROBE"
      },
      {
        "observationId": "2719",
        "patientReference": "167199",
        "subjectId": "edf8c65009e54024b831c70ae3012b86",
        "codeLOINC": "698-1",
        "effectiveDateTime": "2018-09-20T15:41:08.008-06:00",
        "status": "final",
        "interpretation": "POSITIVE",
        "bodySite": "15058008",
        "text": "GC DNA PROBE"
      },
      {
        "allergyIntoleranceId": "1859644194451710",
        "patientReference": "patient/167199",
        "subjectId": "edf8c65009e54024b831c70ae3012b86",
        "codeNDC": "00781577631",
        "codeGPI": "0340001000",
        "clinicalStatus": "active",
        "verificationStatus": "confirmed",
        "allergyIntoleranceCategory": "medication",
        "substance": "AZITHROMYCIN"
      },
      {
        "resourceType": "Condition 1",
        "cnd_id": "1859816221466270",
        "patientReference": "167199",
        "subjectId": "edf8c65009e54024b831c70ae3012b86",
        "code": [
          {
            "system": "SNOMED",
            "code": "192080009",
            "display": "Chronic depression"
          },
          1
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "F32.9",
            "display": "Major depressive disorder, single episode, unspecified"
          },
          1
        ],
        "date": "2018-09-20T00:00:00.000-06:00",
        "clinicalStatus": "active"
      },
      {
        "resourceType": "Condition 2",
        "cnd_id": "1859816292466820",
        "patientReference": "167199",
        "subjectId": "edf8c65009e54024b831c70ae3012b86",
        "code": [
          {
            "system": "SNOMED",
            "code": "192080009",
            "display": "Chronic depression"
          },
          2
        ],
        "code2": [
          {
            "system": "ICD10",
            "code": "F32.9",
            "display": "Major depressive disorder, single episode, unspecified"
          },
          2
        ],
        "date": "2016-09-20T00:00:00.000-06:00",
        "clinicalStatus": "active"
      }
    ]
  }
}


import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import chart_studio.plotly as py
import plotly.tools as tls

if __name__ == '__main__':
    # Initialize client
    test_client = ApiClient()

    time_list = [['Total Time', 'Input Time', 'CDS Time', 'Output Time'] ]
    # Make single subject call
    for i in range(0,1000):
      print(i)
      start_time = time.time()
      response = json.loads(test_client.pathway_single_subject_calculator(mary, cds_analytic_id, cds_input_adapter_id, cds_output_adapter_id).text)
      time_list.append([time.time() - start_time, response.get('data').get('Input_Shaper_Performance').replace("u'", ""), response.get('data').get('CDS_Performance').replace("u'", ""), response.get('data').get('Output_Shaper_Performance').replace("u'", "")])

    # plt.hist(time_list)
    # plt.title("Gaussian Histogram")
    # plt.xlabel("Value")
    # plt.ylabel("Frequency")
    #
    # plt.show()
    print(time_list)

    # test_client.pathway_single_subject_calculator(alliance_test_patient_3, cds_analytic_id, cds_input_adapter_id)
    # print('Call Duration: {}'.format(time.time()-start_time))

