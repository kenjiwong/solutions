from pprint import pprint

test_dict = [{"subjectId": "20401787062", "address": "", "first": "Mary", "last": "Contrary", "gender": "female", "birthDate": "19940805", "ethnicity": ""}, {"allergyIntoleranceId": "324e99e7-863d-4984-aee3-bbd1ac20e101", "patientReference": "Patient/20401787062", "subjectId": "20401787062", "verificationStatus": "completed", "clinicalStatus": "active", "onsetDateTime": "2019-01-17 00:00:00", "codeRXNORM": "18631", "codeNDFRT": 'None', "codeSNOMED": 'None', "text": "Propensity to adverse reactions to drug"}, {"observationId": "d8659136-63d9-4168-80ae-f8f3e059fce3", "patientReference": "Patient/20401787062", "subjectId": "20401787062", "codeLOINC": "", "effectiveDateTime": "20190102023200+0000", "status": "completed", "interpretation": 'None', "bodySite": "", "text": ""}, {"observationId": "96035ae2-24cb-4356-a267-1df8da07eb34", "patientReference": "Patient/20401787062", "subjectId": "20401787062", "codeLOINC": "21613-5", "effectiveDateTime": "20190102023500+0000", "status": "completed", "interpretation": 'None', "bodySite": "75", "text": ""}, {"observationId": "b92fad1a-b5e4-4f9a-bdf4-3b128cbc2029", "patientReference": "Patient/20401787062", "subjectId": "20401787062", "codeLOINC": "47387-6", "effectiveDateTime": "20190102023500+0000", "status": "completed", "interpretation": 'None', "bodySite": "75", "text": ""}, {"observationId": "580d00f4-e47a-460a-b045-4d24f1749375", "patientReference": "Patient/20401787062", "subjectId": "20401787062", "codeLOINC": "31201-7", "effectiveDateTime": "20190102023300+0000", "status": "completed", "interpretation": 'None', "bodySite": "", "text": ""}, {"medicationOrderId": "ec306c42-2bd1-442c-970b-d5fe9c70f09a", "patientReference": "Patient/20401787062", "subjectId": "20401787062", "codeRXNORM": "749785", "dateWritten": "2018-04-17 00:00:00", "status": "", "text": "{7 (Ethinyl Estradiol 0.035 MG "}, {"medicationOrderId": "a5185ffa-c316-4884-96a8-1e663708e5de", "patientReference": "Patient/20401787062", "subjectId": "20401787062", "codeRXNORM": "993564", "dateWritten": "2017-01-17 00:00:00", "status": "", "text": "24 HR Bupropion Hydrochloride 300 MG Extended Release Oral Tablet [Wellbutrin]"}]






headers = []
for item in test_dict:
    headers = headers + list(item.keys())

output_dict_list = []

unique_headers = sorted(list(set(headers)))

for item in test_dict:
    temp_dict = dict((el, '') for el in unique_headers)
    for key, value in item.iteritems():
        temp_dict[key] = str(value)
    output_dict_list.append(temp_dict)

with open('example_data.txt', 'w') as w:
    w.write('|'.join(output_dict_list[0].keys())+'|enumeration|testing_id' + '\n')
    for idx, write_dict in enumerate(sorted(output_dict_list)):
        sorted_dict = dict(sorted(write_dict.items()))
        w.write('|'.join(sorted_dict.values()) + '|{0}|ABC123'.format(idx) +'\n')



