import csv
import uuid

input_file = open('Encounters_Optum.txt', 'r')
output_file = open('Encounters_Optum_Reshaped.txt', 'w')

next(input_file)
input_reader = csv.reader(input_file, delimiter='\t')


headers = ['id', 'source', 'encounterId', 'attribute', 'attribute_value']
output_file.write('\t'.join(headers)+'\n')

for value in input_reader:
    encounterId = str(uuid.uuid4())
    source = 'Population B'
    start = [value[0], source, encounterId, 'Start Date', value[1]]
    end = [value[0], source, encounterId, 'End Date', value[2]]
    encounter_system = [value[0], source, encounterId, 'Code System', value[4]]
    code = [value[0], source, encounterId, 'Code', value[5]]

    output_file.write('\t'.join(start)+'\n')
    output_file.write('\t'.join(end)+'\n')
    output_file.write('\t'.join(encounter_system)+'\n')
    output_file.write('\t'.join(code)+'\n')



