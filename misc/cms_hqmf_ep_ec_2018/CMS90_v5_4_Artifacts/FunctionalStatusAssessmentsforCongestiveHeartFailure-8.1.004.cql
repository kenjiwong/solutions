library FunctionalStatusAssessmentsforCongestiveHeartFailure version '8.1.004'

using QDM version '5.3'

include Hospice version '1.0.000' called Hospice
include MATGlobalCommonFunctions version '2.0.000' called Global

codesystem "LOINC:2.61": 'urn:oid:2.16.840.1.113883.6.1' version 'urn:hl7:version:2.61'

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Heart Failure": 'urn:oid:2.16.840.1.113883.3.526.3.376'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Severe Dementia": 'urn:oid:2.16.840.1.113883.3.526.3.1025'
valueset "PROMIS 10 Global Mental Health Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1138'
valueset "PROMIS 10 Global Physical Health Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1139'
valueset "PROMIS 29 Sleep Disturbance Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1211'
valueset "PROMIS 29 Social Roles Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1217'
valueset "PROMIS 29 Physical Function Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1212'
valueset "PROMIS 29 Fatigue Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1214'
valueset "PROMIS 29 Depression Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1215'
valueset "PROMIS 29 Anxiety Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1216'
valueset "VR12 Mental Component T Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1177'
valueset "VR12 Physical Component T Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1176'
valueset "VR 36 Mental Component Summary (MCS) Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1225'
valueset "VR 36 Physical Component Summary (PCS) Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1226'
valueset "PROMIS 29 Pain Interference Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1213'

code "Clinical summary score [KCCQ]": '72188-6' from "LOINC:2.61" display 'Clinical summary score [KCCQ]'
code "Physical limitation score [KCCQ]": '72195-1' from "LOINC:2.61" display 'Physical limitation score [KCCQ]'
code "Quality of life score [KCCQ]": '72189-4' from "LOINC:2.61" display 'Quality of life score [KCCQ]'
code "Self-efficacy score [KCCQ]": '72190-2' from "LOINC:2.61" display 'Self-efficacy score [KCCQ]'
code "Social limitation score [KCCQ]": '72196-9' from "LOINC:2.61" display 'Social limitation score [KCCQ]'
code "Total symptom score [KCCQ]": '72191-0' from "LOINC:2.61" display 'Total symptom score [KCCQ]'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Denominator":
	"Initial Population"

define "PROMIS10 Total Assessment":
	["Assessment, Performed": "PROMIS 10 Global Mental Health Score"] PROMIS10MentalScore
		with ["Assessment, Performed": "PROMIS 10 Global Physical Health Score"] PROMIS10PhysicalScore
			such that PROMIS10MentalScore.authorDatetime same day as PROMIS10PhysicalScore.authorDatetime
				and PROMIS10PhysicalScore.result is not null
		where PROMIS10MentalScore.result is not null

define "PROMIS29 Total Assessment":
	["Assessment, Performed": "PROMIS 29 Sleep Disturbance Score"] Promis29Sleep
		with ["Assessment, Performed": "PROMIS 29 Social Roles Score"] Promis29SocialRoles
			such that Promis29Sleep.authorDatetime same day as Promis29SocialRoles.authorDatetime
				and Promis29SocialRoles.result is not null
		with ["Assessment, Performed": "PROMIS 29 Physical Function Score"] Promis29Physical
			such that Promis29Sleep.authorDatetime same day as Promis29Physical.authorDatetime
				and Promis29Physical.result is not null
		with ["Assessment, Performed": "PROMIS 29 Pain Interference Score"] Promis29Pain
			such that Promis29Sleep.authorDatetime same day as Promis29Pain.authorDatetime
				and Promis29Pain is not null
		with ["Assessment, Performed": "PROMIS 29 Fatigue Score"] Promis29Fatigue
			such that Promis29Sleep.authorDatetime same day as Promis29Fatigue.authorDatetime
				and Promis29Fatigue.result is not null
		with ["Assessment, Performed": "PROMIS 29 Depression Score"] Promis29Depression
			such that Promis29Sleep.authorDatetime same day as Promis29Depression.authorDatetime
				and Promis29Depression.result is not null
		with ["Assessment, Performed": "PROMIS 29 Anxiety Score"] Promis29Anxiety
			such that Promis29Sleep.authorDatetime same day as Promis29Anxiety.authorDatetime
				and Promis29Anxiety.result is not null
		where Promis29Sleep.result is not null

define "VR12 Total Assessment":
	["Assessment, Performed": "VR12 Mental Component T Score"] VR12MentalScore
		with ["Assessment, Performed": "VR12 Physical Component T Score"] VR12PhysicalScore
			such that VR12MentalScore.authorDatetime same day as VR12PhysicalScore.authorDatetime
				and VR12PhysicalScore.result is not null
		where VR12MentalScore.result is not null

define "VR36 Total Assessment":
	["Assessment, Performed": "VR 36 Mental Component Summary (MCS) Score"] VR12MentalScore
		with ["Assessment, Performed": "VR 36 Physical Component Summary (PCS) Score"] VR12PhysicalScore
			such that VR12MentalScore.authorDatetime same day as VR12PhysicalScore.authorDatetime
				and VR12PhysicalScore.result is not null
		where VR12MentalScore.result is not null

define "Complete Assessment":
	"PROMIS29 Total Assessment"
		union "KCCQ Total Assessment"
		union "VR12 Total Assessment"
		union "VR36 Total Assessment"
		union "PROMIS10 Total Assessment"

define "Numerator":
	exists ( "Initial Congestive Heart Failure Functional Assessment" )
		and exists ( "Follow-up Congestive Heart Failure Functional Assessment" )

define "Congestive Heart Failure Followup Encounter":
	"Qualifying Encounters" OfficeVisit
		with "Congestive Heart Failure Initial Encounters" CHFInitial
			such that OfficeVisit.relevantPeriod starts 30 days or more after day of end of CHFInitial.relevantPeriod
				and OfficeVisit.relevantPeriod starts 180 days or less after day of end of CHFInitial.relevantPeriod

define "Congestive Heart Failure Initial Encounters":
	First("Qualifying Encounters" QualifyingEncounter
			where QualifyingEncounter.relevantPeriod ends 185 days or less after start of "Measurement Period"
			sort by start of relevantPeriod
	)

define "Follow-up Congestive Heart Failure Functional Assessment":
	"Complete Assessment" FollowupHeartFailureFSA
		with "Initial Congestive Heart Failure Functional Assessment" InitialHeartFailureFSA
			such that FollowupHeartFailureFSA.authorDatetime 30 days or more after day of InitialHeartFailureFSA.authorDatetime
				and FollowupHeartFailureFSA.authorDatetime 180 days or less after day of InitialHeartFailureFSA.authorDatetime
				and InitialHeartFailureFSA.result is not null

define "Initial Congestive Heart Failure Functional Assessment":
	"Complete Assessment" HeartFailureFSA
		with "Congestive Heart Failure Initial Encounters" CHFInitial
			such that ( HeartFailureFSA.authorDatetime 14 days or less on or before day of end of CHFInitial.relevantPeriod )
				and HeartFailureFSA.result is not null

define "Qualifying Encounters":
	["Encounter, Performed": "Office Visit"] ValidEncounter
		where ValidEncounter.relevantPeriod during "Measurement Period"

define "KCCQ Total Assessment":
	["Assessment, Performed": "Quality of life score [KCCQ]"] KCCQLifeQuality
		with ["Assessment, Performed": "Clinical summary score [KCCQ]"] KCCQClinical
			such that KCCQLifeQuality.authorDatetime same day as KCCQClinical.authorDatetime
				and KCCQClinical.result is not null
		with ["Assessment, Performed": "Self-efficacy score [KCCQ]"] KCCQSelfEfficacy
			such that KCCQLifeQuality.authorDatetime same day as KCCQSelfEfficacy.authorDatetime
				and KCCQSelfEfficacy.result is not null
		with ["Assessment, Performed": "Total symptom score [KCCQ]"] KCCQSymptoms
			such that KCCQLifeQuality.authorDatetime same day as KCCQSymptoms.authorDatetime
				and KCCQSymptoms.result is not null
		with ["Assessment, Performed": "Physical limitation score [KCCQ]"] KCCQPhysicalLimits
			such that KCCQLifeQuality.authorDatetime same day as KCCQPhysicalLimits.authorDatetime
				and KCCQPhysicalLimits.result is not null
		with ["Assessment, Performed": "Social limitation score [KCCQ]"] KCCQSocialLimits
			such that KCCQLifeQuality.authorDatetime same day as KCCQSocialLimits.authorDatetime
				and KCCQSocialLimits.result is not null
		where KCCQLifeQuality.result is not null

define "Denominator Exclusions":
	Hospice."Has Hospice"
		or exists ( ["Diagnosis": "Severe Dementia"] Dementia
				where Dementia.prevalencePeriod overlaps "Measurement Period"
		)

define "Initial Population":
	exists ( ["Patient Characteristic Birthdate"] BirthDate
			where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 18
	)
		and exists ( ["Diagnosis": "Heart Failure"] HeartFailure
				where HeartFailure.prevalencePeriod overlaps "Measurement Period"
		)
		and exists ( "Congestive Heart Failure Followup Encounter" )
