library DiabeticRetinopathyDocumentationofPresenceorAbsenceofMacularEdemaandLevelofSeverityofRetinopathy version '6.0.005'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Care Services in Long-Term Residential Facility": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1014'
valueset "Diabetic Retinopathy": 'urn:oid:2.16.840.1.113883.3.526.3.327'
valueset "Level of Severity of Retinopathy Findings": 'urn:oid:2.16.840.1.113883.3.526.3.1283'
valueset "Macular Edema Findings Absent": 'urn:oid:2.16.840.1.113883.3.526.3.1284'
valueset "Macular Edema Findings Present": 'urn:oid:2.16.840.1.113883.3.526.3.1320'
valueset "Macular Exam": 'urn:oid:2.16.840.1.113883.3.526.3.1251'
valueset "Medical Reason": 'urn:oid:2.16.840.1.113883.3.526.3.1007'
valueset "Nursing Facility Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1012'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Ophthalmological Services": 'urn:oid:2.16.840.1.113883.3.526.3.1285'
valueset "Outpatient Consultation": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1008'
valueset "Patient Reason": 'urn:oid:2.16.840.1.113883.3.526.3.1008'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Numerator":
	exists "Findings of Macular Exam"

define "Face to Face Encounter During Measurement Period":
	( ["Encounter, Performed": "Office Visit"]
		union ["Encounter, Performed": "Ophthalmological Services"]
		union ["Encounter, Performed": "Outpatient Consultation"]
		union ["Encounter, Performed": "Nursing Facility Visit"]
		union ["Encounter, Performed": "Care Services in Long-Term Residential Facility"] ) FaceToFaceEncounter
		where FaceToFaceEncounter.relevantPeriod during "Measurement Period"

define "Diabetic Retinopathy Encounter":
	"Face to Face Encounter During Measurement Period" FaceToFaceEncounter
		with ["Diagnosis": "Diabetic Retinopathy"] DiabeticRetinopathy
			such that DiabeticRetinopathy.prevalencePeriod overlaps FaceToFaceEncounter.relevantPeriod

define "Initial Population":
	exists ["Patient Characteristic Birthdate"] BirthDate
		where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 18
			and exists "Diabetic Retinopathy Encounter"

define "Denominator":
	"Initial Population"

define "Macular Exam Not Performed for Medical or Patient Reason":
	["Diagnostic Study, Not Performed": "Macular Exam"] NoMacularExam
		with "Diabetic Retinopathy Encounter" EncounterDiabeticRetinopathy
			such that NoMacularExam.authorDatetime during EncounterDiabeticRetinopathy.relevantPeriod
		where ( NoMacularExam.negationRationale in "Medical Reason"
				or NoMacularExam.negationRationale in "Patient Reason"
		)

define "Findings of Macular Exam":
	["Diagnostic Study, Performed": "Macular Exam"] MacularExam
		with "Diabetic Retinopathy Encounter" EncounterDiabeticRetinopathy
			such that MacularExam.relevantPeriod during EncounterDiabeticRetinopathy.relevantPeriod
		where exists ( MacularExam.components Component
				where Component.result in "Level of Severity of Retinopathy Findings"
		)
			and exists MacularExam.components Component
				where ( Component.result in "Macular Edema Findings Absent"
						or Component.result in "Macular Edema Findings Present"
				)

define "Denominator Exceptions":
	exists "Macular Exam Not Performed for Medical or Patient Reason"
