library FunctionalStatusAssessmentforTotalHipReplacement version '7.1.001'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global
include Hospice version '1.0.000' called Hospice

codesystem "CPT:2018": 'urn:oid:2.16.840.1.113883.6.12' version 'urn:hl7:version:2018'
codesystem "SNOMEDCT:2017-09": 'urn:oid:2.16.840.1.113883.6.96' version 'urn:hl7:version:2017-09'

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Fracture - Lower Body": 'urn:oid:2.16.840.1.113883.3.464.1003.113.12.1037'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Outpatient Consultation": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1008'
valueset "Primary THA Procedure": 'urn:oid:2.16.840.1.113883.3.464.1003.198.12.1006'
valueset "PROMIS 10 Global Mental Health Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1138'
valueset "PROMIS 10 Global Physical Health Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1139'
valueset "VR12 Mental Component T Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1177'
valueset "VR12 Physical Component T Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1176'
valueset "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Quality of Life Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1166'
valueset "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Sport Recreation Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1149'
valueset "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Activities of Daily Living Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1150'
valueset "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Symptoms Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1168'
valueset "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Pain Score": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1170'
valueset "Hip Dysfunction and Osteoarthritis Outcome Score for Joint Replacement [HOOSJR]": 'urn:oid:2.16.840.1.113883.3.464.1003.118.12.1210'

code "Postoperative follow-up visit, normally included in the surgical package, to indicate that an evaluation and management service was performed during a postoperative period for a reason(s) related to the original procedure": '99024' from "CPT:2018" display 'Postoperative follow-up visit, normally included in the surgical package, to indicate that an evaluation and management service was performed during a postoperative period for a reason(s) related to the original procedure'
code "Severe dementia (disorder)": '428351000124105' from "SNOMEDCT:2017-09" display 'Severe dementia (disorder)'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Denominator":
	"Initial Population"

define "Numerator":
	exists "Total Hip Arthroplasty with Initial Assessment"
		and exists "Hip Assessment 270 to 365 Days after Total Hip Arthroplasty Procedure"

define "2 or More Lower Body Fractures":
	"Total Hip Arthroplasty Procedure" THAProcedure
		where Count(["Diagnosis": "Fracture - Lower Body"] LowerBodyFracture
				where LowerBodyFracture.prevalencePeriod starts less than 1 day on or before THAProcedure.relevantPeriod
		)>= 2

define "Dementia Diagnosis":
	["Diagnosis": "Severe dementia (disorder)"] Dementia
		where Dementia.prevalencePeriod overlaps "Measurement Period"

define "Qualifying Encounter":
	( ["Encounter, Performed": "Outpatient Consultation"]
		union ["Encounter, Performed": "Office Visit"]
		union ["Encounter, Performed": "Postoperative follow-up visit, normally included in the surgical package, to indicate that an evaluation and management service was performed during a postoperative period for a reason(s) related to the original procedure"] ) QualifyingEncounter
		where QualifyingEncounter.relevantPeriod during "Measurement Period"

define "PROMIS10 Total Assessment":
	["Assessment, Performed": "PROMIS 10 Global Mental Health Score"] Promis10MentalScore
		with ["Assessment, Performed": "PROMIS 10 Global Physical Health Score"] Promis10PhysicalScore
			such that Promis10MentalScore.authorDatetime same day as Promis10PhysicalScore.authorDatetime
				and Promis10PhysicalScore.result is not null
		where Promis10MentalScore.result is not null

define "VR12 Total Assessment":
	["Assessment, Performed": "VR12 Mental Component T Score"] VR12MentalScore
		with ["Assessment, Performed": "VR12 Physical Component T Score"] VR12PhysicalScore
			such that VR12MentalScore.authorDatetime same day as VR12PhysicalScore.authorDatetime
				and VR12PhysicalScore.result is not null
		where VR12MentalScore.result is not null

define "HOOS Total Assessment":
	["Assessment, Performed": "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Quality of Life Score"] HOOSLifeQuality
		with ["Assessment, Performed": "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Sport Recreation Score"] HOOSSport
			such that HOOSLifeQuality.authorDatetime same day as HOOSSport.authorDatetime
				and HOOSSport.result is not null
		with ["Assessment, Performed": "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Activities of Daily Living Score"] HOOSActivityScore
			such that HOOSLifeQuality.authorDatetime same day as HOOSActivityScore.authorDatetime
				and HOOSActivityScore.result is not null
		with ["Assessment, Performed": "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Symptoms Score"] HOOSSymptoms
			such that HOOSLifeQuality.authorDatetime same day as HOOSSymptoms.authorDatetime
				and HOOSSymptoms.result is not null
		with ["Assessment, Performed": "Hip Dysfunction and Osteoarthritis Outcome Score [HOOS] Pain Score"] HOOSPain
			such that HOOSLifeQuality.authorDatetime same day as HOOSPain.authorDatetime
				and HOOSPain.result is not null
		where HOOSLifeQuality.result is not null

define "Complete Assessment":
	( ["Assessment, Performed": "Hip Dysfunction and Osteoarthritis Outcome Score for Joint Replacement [HOOSJR]"] HOOSJr
			where HOOSJr.result is not null
	)
		union "HOOS Total Assessment"
		union "VR12 Total Assessment"
		union "PROMIS10 Total Assessment"

define "Initial Population":
	exists ( "Total Hip Arthroplasty Procedure" )
		and exists ( "Qualifying Encounter" )
		and exists ( ["Patient Characteristic Birthdate"] BirthDate
				where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 19
		)

define "Denominator Exclusions":
	Hospice."Has Hospice"
		or exists ( "2 or More Lower Body Fractures" )
		or exists ( "Dementia Diagnosis" )

define "Hip Assessment 270 to 365 Days after Total Hip Arthroplasty Procedure":
	"Complete Assessment" HipAssessment
		with "Total Hip Arthroplasty with Initial Assessment" TotalHip
			such that HipAssessment.authorDatetime 270 days or more after day of end of TotalHip.relevantPeriod
				and HipAssessment.authorDatetime 365 days or less after day of end of TotalHip.relevantPeriod

define "Total Hip Arthroplasty Procedure":
	( ["Procedure, Performed": "Primary THA Procedure"] THAProcedure
			where THAProcedure.relevantPeriod starts 12 months or less before start of "Measurement Period"
	)

define "Total Hip Arthroplasty with Initial Assessment":
	"Total Hip Arthroplasty Procedure" TotalHip
		with "Complete Assessment" HipAssessment
			such that TotalHip.relevantPeriod starts 90 days or less on or after day of HipAssessment.authorDatetime
