library PrimaryOpenAngleGlaucomaPOAGOpticNerveEvaluation version '6.0.007'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Care Services in Long-Term Residential Facility": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1014'
valueset "Cup to Disc Ratio": 'urn:oid:2.16.840.1.113883.3.526.3.1333'
valueset "Medical Reason": 'urn:oid:2.16.840.1.113883.3.526.3.1007'
valueset "Nursing Facility Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1012'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Ophthalmological Services": 'urn:oid:2.16.840.1.113883.3.526.3.1285'
valueset "Optic Disc Exam for Structural Abnormalities": 'urn:oid:2.16.840.1.113883.3.526.3.1334'
valueset "Outpatient Consultation": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1008'
valueset "Primary Open-Angle Glaucoma": 'urn:oid:2.16.840.1.113883.3.526.3.326'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Face to Face Encounter During Measurement Period":
	( ["Encounter, Performed": "Office Visit"]
		union ["Encounter, Performed": "Ophthalmological Services"]
		union ["Encounter, Performed": "Outpatient Consultation"]
		union ["Encounter, Performed": "Nursing Facility Visit"]
		union ["Encounter, Performed": "Care Services in Long-Term Residential Facility"] ) FaceToFaceEncounter
		where FaceToFaceEncounter.relevantPeriod during "Measurement Period"

define "Primary Open Angle Glaucoma Encounter":
	"Face to Face Encounter During Measurement Period" FaceToFaceEncounter
		with ["Diagnosis": "Primary Open-Angle Glaucoma"] PrimaryOpenAngleGlaucoma
			such that PrimaryOpenAngleGlaucoma.prevalencePeriod overlaps FaceToFaceEncounter.relevantPeriod

define "Initial Population":
	exists ["Patient Characteristic Birthdate"] BirthDate
		where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 18
			and exists "Primary Open Angle Glaucoma Encounter"

define "Denominator":
	"Initial Population"

define "Cup to Disc Ratio Not Performed for Medical Reason":
	["Diagnostic Study, Not Performed": "Cup to Disc Ratio"] NoCupToDiscExam
		with "Primary Open Angle Glaucoma Encounter" EncounterPrimaryOpenAngleGlaucoma
			such that NoCupToDiscExam.authorDatetime during EncounterPrimaryOpenAngleGlaucoma.relevantPeriod
		where NoCupToDiscExam.negationRationale in "Medical Reason"

define "Optic Disc Exam Not Performed for Medical Reason":
	["Diagnostic Study, Not Performed": "Optic Disc Exam for Structural Abnormalities"] NoOpticDiscExam
		with "Primary Open Angle Glaucoma Encounter" EncounterPrimaryOpenAngleGlaucoma
			such that NoOpticDiscExam.authorDatetime during EncounterPrimaryOpenAngleGlaucoma.relevantPeriod
		where NoOpticDiscExam.negationRationale in "Medical Reason"

define "Optic Disc Exam Performed with Result":
	["Diagnostic Study, Performed": "Optic Disc Exam for Structural Abnormalities"] OpticDiscExam
		with "Primary Open Angle Glaucoma Encounter" EncounterPrimaryOpenAngleGlaucoma
			such that OpticDiscExam.relevantPeriod during EncounterPrimaryOpenAngleGlaucoma.relevantPeriod
				and OpticDiscExam.result is not null

define "Denominator Exceptions":
	exists "Cup to Disc Ratio Not Performed for Medical Reason"
		or exists "Optic Disc Exam Not Performed for Medical Reason"

define "Cup to Disc Ratio Performed with Result":
	["Diagnostic Study, Performed": "Cup to Disc Ratio"] CupToDiscExam
		with "Primary Open Angle Glaucoma Encounter" EncounterPrimaryOpenAngleGlaucoma
			such that CupToDiscExam.relevantPeriod during EncounterPrimaryOpenAngleGlaucoma.relevantPeriod
				and CupToDiscExam.result is not null

define "Numerator":
	exists "Cup to Disc Ratio Performed with Result"
		and exists "Optic Disc Exam Performed with Result"
