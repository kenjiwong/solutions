'''
Author: Kenji Wong
Date: 8/30/2018
Purpose: Create testing script for Alliance
'''

import hmac
from Crypto.Hash import SHA256
import datetime
import requests
import time
import statistics
import matplotlib as mpl
mpl.use('TkAgg')
from matplotlib import pyplot as plt
import numpy

public_key = '0e395733f5dbb0dba67fc3b6eda3de6ad22af1e1'
private_key = '627ccd1a532df2d14ad40c22de3e130bb530d1be'
cds_analytic_id = '57t-aqdd5d.1.0'
population_id = 'bdx-yj94c9.1.0'
subject_id = 'abc123'
iterations = int(raw_input("How many times would you like to run this call? "))
firewall_creds = ('apervita.tester', 'bl00dm00n')

test_patient_json = {
            "subject": {
                "subjectid" : "3d86541d-95a9-40b2-8592-7c6bc805b018",
                "events": [
                {
                    "subjectId": "67890",
                    "birthDate": "7/2/1986",
                    "gender": "male",
                    "first": "Paul",
                    "last": "Welles",
                    "mrn": "98201232",
                },
                {
                    "observationId": "O-4",
                    "patientReference": "Patient/67890",
                    "subjectId": "67890",
                    "codeLOINC": "60256-5",
                    "effectiveDateTime": "4/20/2018 12:00",
                    "status": "final",
                    "interpretation": "POS",
                    "bodySite": "",
                    "text": "Neisseria gonorrhoeae rRNA [Presence] in Urine by Probe and target amplification method"
                },
        ]}
    }

test_patient_2 = {
    "apple": "123728"
}

test_patient_3 = {
            "subject": {
                "subjectid": "3d86541d-95a9-40b2-8592-7c6bc805b018",
                "events": [
                {
                    "observationId": "O-4",
                    "patientReference": "Patient/67890",
                    "subjectId": "67890",
                    "codeLOINC": "60256-5",
                    "effectiveDateTime": "4/20/2018 12:00",
                    "status": "final",
                    "interpretation": "POS",
                    "bodySite": "",
                },
        ]}
    }

class ApiClient(object):

    def __init__(self):
        # Initialize attributes on object
        self.api_key = public_key
        self.private_key = private_key
        self.http_method = 'https://'
        self.url = 'www.computehealth.com'

    def pathway_single_subject_calculator(self, json, insightid, populationid, subjectid):
        # Designate POST parameters for signature
        #
        rest_type = 'post'
        path = '/api/v2/pathway/json'
        response = self.assemble_call(rest_type=rest_type, populationid=populationid, subjectid=subjectid, path=path, json=json, insightid=insightid)

        return response

    def single_subject_calculator(self, json, insightid):
        # Designate POST parameters for signature
        rest_type = 'post'
        path = '/api/v2/subject/calculate/json'

        response = self.assemble_call(rest_type=rest_type, path=path, json=json, insightid=insightid)

        return response

    def assemble_call(self, rest_type, path, file_source_path=None, json=None, **kwargs):
        # Get current time stamp
        current_timestamp = datetime.datetime.utcnow().replace(microsecond=0).isoformat()

        # Create base parameters in every call
        parameters = {'api_key': self.api_key, 'timestamp': current_timestamp}

        for key, value in kwargs.iteritems():
            parameters[key] = value

        response = self.make_call(rest_type, path, parameters, file_source_path, json)

        return response

    def make_call(self, rest_type, path, parameters, file_source_path=None, json=None):
        dashes = '-' * 150
        print(dashes)

        # Create signature
        url_parameters = self.add_signature(rest_type, path, parameters)

        # Create URL with parameters and signature
        url = self.create_url(path, url_parameters)

        # Make either GET or POST
        if rest_type == 'post':
            print('Method: POST')
            print('Final URL: {0}'.format(url))

            # Open up file and make post request
            if file_source_path:
                upload_file = open(file_source_path, 'rb')
                response = requests.post(url, files={'uploadfile': upload_file})
            elif json:
                response = requests.post(url, json=json, auth=firewall_creds)
            else:
                response = requests.post(url)

            # print response
            print('Request Headers: ' + str(response.request.headers))
            print('Response: ' + response.text)
        elif rest_type == 'get':
            print('Method: GET')
            print('Final URL: {0}'.format(url))

            # Make get request
            response = requests.get(url)

            # Print response
            print('Request Headers: ' + str(response.request.headers))
            print('Response: ' + response.text)
        else:
            response = None
            print('Unknown method')

        print(dashes)

        return response

    def add_signature(self, rest_type, path, parameters):
        # Create parameters list in alphabetical order
        parameters_list = ['{0}={1}'.format(key, parameters[key]) for key in sorted(parameters.keys())]

        # Join parameters and rest of the URL
        signature_list = [rest_type, self.url, path, '&'.join(parameters_list)]

        # Join by pipes and lowercase inputs
        prep_sign = '|'.join(signature_list).lower()
        print('Prepare Signature: {0}'.format(prep_sign))

        # Create signature SHA-256
        signature = hmac.new(self.private_key, prep_sign, SHA256).hexdigest()

        # Add signature to dict
        parameters['signature'] = signature

        return parameters

    def create_url(self, path, parameters):
        # Sort parameters
        parameters_list = ['{0}={1}'.format(key, parameters[key]) for key in sorted(parameters.keys())]

        # Join with the rest of parameters
        url_list = [self.http_method, self.url, path+'?', '&'.join(parameters_list)]

        # Create URL
        url = ''.join(url_list).lower()

        return url


if __name__ == '__main__':
    # Initialize client
    test_client = ApiClient()
    start_time = time.time()
    # Make single subject call
    time_measurements = []
    for i in range(iterations):
        start_time = time.time()
        test_client.pathway_single_subject_calculator(test_patient_3, cds_analytic_id, population_id, subject_id)
        time_measurements.append((time.time()-start_time))
        print('Run Number: {0}'.format(i))
    print('Number of Calls {0}\n Average Call Time: {1}'.format(len(time_measurements), (sum(time_measurements)/len(time_measurements))))
    print('Standard Deviation: {0}'.format(statistics.stdev(time_measurements)))
    print('Max Call Time: {}'.format(max(time_measurements)))
    print('Min Call Time: {}'.format(min(time_measurements)))

    plt.hist(time_measurements, bins=30)
    plt.show()