import os
import argparse


class NcqaSeedGenerator(object):

    def __init__(self):
        self.name = "NCQA Seed Generator"
        self.headers = set()

    def extract_seed(self, path):
        extract_directory = os.walk(path)

        for dirpath, subdirs, files in extract_directory:
            for file in files:
                if file.endswith('.txt'):
                    with open(dirpath+'/'+file, 'r') as f:
                        first_line = f.readline()
                        headers = first_line.split('|')
                        for header in headers:
                            self.headers.add(header.replace('\r\n',''))

    def write_file(self):
        output_file = open('NCQA_Seed.txt', 'w')
        output_file.write('|'.join(self.headers)+'\n')
        output_file.write('|'.join(['NULL' for idx in range(len(self.headers))]))

if __name__ == '__main__':

    # Arg parse
    arg_parse = argparse.ArgumentParser()
    arg_parse.add_argument('--path', help='Path to NCQA extract directory')
    args = arg_parse.parse_args()

    # Initialize parser
    ncqa_seed_generator = NcqaSeedGenerator()

    # Parse and format HQMF zip
    extract_path = args.path
    ncqa_seed_generator.extract_seed(extract_path)
    ncqa_seed_generator.write_file()
