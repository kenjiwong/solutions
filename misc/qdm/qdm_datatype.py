from lxml import etree

class XsdParser(object):

    def __init__(self, file_path):
        self.name = 'XSD Parser'
        self.file = open(file_path, 'r')
        self.output_dict = dict()
        self.namespaces = {
        'XSD':'{http://www.w3.org/2001/XMLSchema}'
        }

    def parse(self):
        xml_tree = etree.parse(self.file)
        root = xml_tree.getroot()

        sequences = []

        for complex_type in root:
            self.output_dict.setdefault(complex_type.get('name', 'None'), [])
            key = complex_type.get('name')
            if complex_type.tag == '{http://www.w3.org/2001/XMLSchema}complexType':
                for attributes in complex_type:
                    if attributes.tag == '{http://www.w3.org/2001/XMLSchema}sequence':
                        sequences.append(attributes)
                        for i in sequences:
                            for j in i:
                                self.output_dict[key].append([j.get('name','Error'), j.get('type')])
                    if attributes.tag == '{http://www.w3.org/2001/XMLSchema}complexContent':
                        sequence = attributes.getchildren()[0].getchildren()
                        if sequence:
                            sequences.append(sequence[0])
                            for i in sequence[0]:
                                self.output_dict[key].append([i.get('name'), i.get('type')])

        f = open('QDMDataTypeToAttribute.txt','w')
        f.write('QDMDataType|element|type\n')
        for key, values in self.output_dict.iteritems():
            for value in values:
                f.write('{0}|{1}|{2}\n'.format(key,value[0],value[1]))

        f2 = open('AttributeToQDMDataType', 'w')
        unique_output = dict()
        for key, values in self.output_dict.iteritems():
            for value in values:
                unique_output.setdefault(value[0], []).append(key)
        f2.write('element|QDMDataTypes\n')
        for key, values in unique_output.iteritems():
            f2.write('{0}|{1}\n'.format(key, '|'.join(values)))

if __name__ == "__main__":
    xsd_parser = XsdParser('/Users/kenji.wong/PycharmProjects/solutions/misc/qdm/qdm.xsd')
    xsd_parser.parse()