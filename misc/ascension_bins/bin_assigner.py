import numpy
import math
bins = [-2.05,
-1.95,
-1.85,
-1.75,
-1.65,
-1.55,
-1.45,
-1.35,
-1.25,
-1.15,
-1.05,
-0.95,
-0.85,
-0.75,
-0.65,
-0.55,
-0.45,
-0.35,
-0.25,
-0.15,
-0.05,
0.05,
0.15,
0.25,
0.35,
0.45,
0.55,
0.65,
0.75,
0.85,
0.95,
1.05,
1.15,
1.25,
1.35,
1.45,
1.55,
1.65,
1.75,
1.85,
1.95,
2.05,
2.15,
2.25]


with open('output.txt', 'w') as f2:
    with open('ROUNDED_SUM_Score.txt', 'r') as f:
        for i in f:
            try:
                i = float(i)
            except:
                pass
            if isinstance(i, float):
                for idx, bin in enumerate(bins):
                    if i > bin:
                        pass
                    else:
                        ceiling = idx
                        break
                if ceiling-1 == -1:
                    index = 0
                else:
                    index = ceiling
                output = '{}\n'.format(bins[index])

            else:
                output = i

            f2.write(output)
