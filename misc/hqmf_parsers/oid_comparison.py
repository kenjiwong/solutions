f1 = open('/Users/kenji.wong/PycharmProjects/solutions/misc/hqmf_parsers/2017_ep_ec_oids.txt', 'r')
f2 = open('/Users/kenji.wong/PycharmProjects/solutions/misc/hqmf_parsers/2018_ep_ec_oids.txt', 'r')

list1 = []
list2 = []

difference = set()

outputs = []

for row in f1:
    list1.append(row.split('|')[0])
for row in f2:
    list2.append(row.split('|')[0])

for row in list1:
    if not row in list2:
        difference.add(row.split('|')[0])

for row in list2:
    if not row in list1:
        difference.add(row.split('|')[0])

f1.close()
f2.close()

output_file = open('output_file.txt', 'w')

for diff in difference:
    output_file.write(diff + '\n')