import os
from lxml import etree
import argparse


class OidParser(object):

    def __init__(self):
        self.NAMESPACES = {'CQL': 'urn:hhs-cql:hqmf-n1-extensions:v1', 'URN': 'urn:hl7-org:v3',
                      'XSI': "http://www.w3.org/2001/XMLSchema-instance"}
        self.unique_oids = dict()

    def get_all_xml(self, extract_directory):
        extract_directory = os.walk(extract_directory)
        xml_file_paths = []

        # Iterate through extracts and pull xml file paths
        for dirpath, subdirs, files in extract_directory:
            for file in files:
                if file.endswith('.xml'):
                    xml_file_paths.append(dirpath + '/' + file)

        return xml_file_paths

    def extract_oids(self, file_paths=None, file_name=''):
        hqmf_counter = 0

        if file_paths:
            # Iterate through all xml paths and parse them if they are HQMF
            for xml_file_path in file_paths:
                xml_tree = etree.parse(xml_file_path)
                xml_root = xml_tree.getroot()

                if xml_root.tag == '{urn:hl7-org:v3}QualityMeasureDocument':
                    # print('File Name: {0}'.format(xml_file_path.split('/')[-1]))
                    hqmf_counter+=1
                    self.parse(xml_root, xml_file_path)

            print('Number of HQMF Files Parsed: {0}'.format(hqmf_counter))
            print('Unique OIDs Count: {0}'.format(len(self.unique_oids)))

            self.write_file(self.unique_oids, file_name)

    def parse(self, hqmf_root, xml_path):
        self.value_set_root = hqmf_root.xpath('URN:definition/URN:valueSet', namespaces=self.NAMESPACES)

        # Iterate through value set definitions
        for value_set in self.value_set_root:
            value_set_oid = value_set.xpath('URN:id', namespaces=self.NAMESPACES)[0].get('root')
            self.unique_oids.setdefault(value_set_oid, set()).add(xml_path.split('/')[-1].split('_')[0])

    def write_file(self, oids, file_name):
        f = open(file_name, 'w')
        for oid, measures in oids.iteritems():
            f.write('{0}| {1}'.format(oid, ','.join(measures)) + '\n')

if __name__ == '__main__':
    arg_parse = argparse.ArgumentParser()
    arg_parse.add_argument('--path', help='Path to HQMF extract directory')
    arg_parse.add_argument('--name', help='Enter write file name')
    args = arg_parse.parse_args()

    # Initialize parser
    oid_parser = OidParser()

    # Get xml paths
    extract_path = args.path
    xml_file_paths = oid_parser.get_all_xml(extract_path)

    # Extract oids
    file_name = args.name
    oid_parser.extract_oids(xml_file_paths, file_name)
