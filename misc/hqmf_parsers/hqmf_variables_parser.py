import os
from lxml import etree
import argparse


class VariableParser():

    def __init__(self):
        self.NAMESPACES = {'CQL': 'urn:hhs-cql:hqmf-n1-extensions:v1', 'URN': 'urn:hl7-org:v3',
                      'XSI': "http://www.w3.org/2001/XMLSchema-instance"}
        self.unique_variables = dict()

    def get_all_xml(self, extract_directory):
        extract_directory = os.walk(extract_directory)
        xml_file_paths = []

        # Iterate through extracts and pull xml file paths
        for dirpath, subdirs, files in extract_directory:
            for file in files:
                if file.endswith('.xml'):
                    xml_file_paths.append(dirpath + '/' + file)

        return xml_file_paths

    def extract_variables(self, file_paths=None):
        hqmf_counter = 0

        if file_paths:
            # Iterate through all xml paths and parse them if they are HQMF
            for xml_file_path in file_paths:
                xml_tree = etree.parse(xml_file_path)
                xml_root = xml_tree.getroot()

                if xml_root.tag == '{urn:hl7-org:v3}QualityMeasureDocument':
                    # print('File Name: {0}'.format(xml_file_path.split('/')[-1]))
                    hqmf_counter+=1
                    self.parse(xml_root, xml_file_path)

            for key, value in self.unique_variables.iteritems():
                self.unique_variables[key] = sorted(value)

            # print('-' * 150)
            # print(self.unique_variables.keys())
            # print(self.unique_variables)

            print('Number of HQMF Files Parsed: {0}'.format(hqmf_counter))
            print('Unique Variables Count: {0}'.format(len(self.unique_variables)))

            self.write_file(self.unique_variables)

    def parse(self, hqmf_root, xml_file_path):

        self.data_criteria_root = hqmf_root.xpath('URN:component/URN:dataCriteriaSection/URN:entry', namespaces=self.NAMESPACES)

        # Pull data criteria
        for variable in self.data_criteria_root:
            children = variable.getchildren()
            if len(children) == 1:
                title = children[0]
            else:
                title = children[1]
            value = title.xpath('URN:title', namespaces=self.NAMESPACES)[0]
            self.unique_variables.setdefault(value.get('value', 'None'), set()).add(xml_file_path.split('/')[-1].split('_')[0])

    def write_file(self, variables):
        f = open('unique_variables_output.txt', 'w')
        for variable, measures in variables.iteritems():
            f.write('{0}: {1}'.format(variable, ', '.join(measures)) + '\n')

if __name__ == '__main__':
    arg_parse = argparse.ArgumentParser()
    arg_parse.add_argument('--path', help='Path to HQMF extract directory')
    args = arg_parse.parse_args()

    # Initialize parser
    variable_parser = VariableParser()

    # Get xml paths
    extract_path = args.path
    xml_file_paths = variable_parser.get_all_xml(extract_path)

    # Extract Variables
    variable_parser.extract_variables(xml_file_paths)
