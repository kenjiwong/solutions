library HypertensionImprovementinBloodPressure version '8.0.001'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global
include Hospice version '1.0.000' called Hospice

codesystem "LOINC:2.63": 'urn:oid:2.16.840.1.113883.6.1' version 'urn:hl7:version:2.63'

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "End Stage Renal Disease": 'urn:oid:2.16.840.1.113883.3.526.3.353'
valueset "Essential Hypertension": 'urn:oid:2.16.840.1.113883.3.464.1003.104.12.1011'
valueset "Hemodialysis": 'urn:oid:2.16.840.1.113883.3.526.3.1083'
valueset "Kidney Transplant": 'urn:oid:2.16.840.1.113883.3.464.1003.109.12.1012'
valueset "Peritoneal Dialysis": 'urn:oid:2.16.840.1.113883.3.526.3.1084'
valueset "Pregnancy": 'urn:oid:2.16.840.1.113883.3.526.3.378'
valueset "Blood Pressure Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1056'
valueset "Kidney Transplant Recipient": 'urn:oid:2.16.840.1.113883.3.464.1003.109.12.1029'

code "Systolic blood pressure": '8480-6' from "LOINC:2.63" display 'Systolic blood pressure'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Denominator":
	"Initial Population"

define "Last Systolic Blood Pressure during Followup Encounter":
	Last(["Physical Exam, Performed": "Systolic blood pressure"] LastSystolicBloodPressure
			with "Followup Blood Pressure Visit" FollowupEncounter
				such that LastSystolicBloodPressure.relevantPeriod during FollowupEncounter.relevantPeriod
			where LastSystolicBloodPressure.result is not null
			sort by start of relevantPeriod
	)

define "Has Last Systolic Blood Pressure Result":
	( "Last Systolic Blood Pressure during Initial Encounter" SystolicBloodPressure
			where SystolicBloodPressure.result >= 140 'mm[Hg]'
	) is not null

define "End Stage Renal Disease Procedures":
	( ["Procedure, Performed": "Kidney Transplant"]
		union ["Procedure, Performed": "Hemodialysis"]
		union ["Procedure, Performed": "Peritoneal Dialysis"] ) ESRDProcedures
		where ESRDProcedures.relevantPeriod starts before end of "Measurement Period"

define "Numerator":
	( from
			"Last Systolic Blood Pressure during Initial Encounter" InitialSystolicBloodPressure
			with "Last Systolic Blood Pressure during Followup Encounter" FollowupSystolicBloodPressure
				such that ( ( InitialSystolicBloodPressure.result as Quantity ) - ( FollowupSystolicBloodPressure.result as Quantity ) ) >= 10 'mm[Hg]'
					or FollowupSystolicBloodPressure.result < 140 'mm[Hg]'
	) is not null

define "Initial Population":
	exists ["Patient Characteristic Birthdate"] BirthDate
		where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 18
			and Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")< 85
			and "Has Last Systolic Blood Pressure Result"

define "Last Systolic Blood Pressure during Initial Encounter":
	Last(["Physical Exam, Performed": "Systolic blood pressure"] LastSystolicBloodPressure
			with "Initial Blood Pressure Visit" InitialEncounter
				such that LastSystolicBloodPressure.relevantPeriod during InitialEncounter.relevantPeriod
			where LastSystolicBloodPressure.result is not null
			sort by start of relevantPeriod
	)

define "Initial Blood Pressure Visit":
	First(["Encounter, Performed": "Blood Pressure Visit"] InitialBloodPressureVisit
			with ["Diagnosis": "Essential Hypertension"] Hypertension
				such that InitialBloodPressureVisit.relevantPeriod overlaps Hypertension.prevalencePeriod
			with ["Physical Exam, Performed": "Systolic blood pressure"] SystolicBloodPressure
				such that InitialBloodPressureVisit.relevantPeriod overlaps SystolicBloodPressure.relevantPeriod
			where InitialBloodPressureVisit.relevantPeriod starts 6 months or less on or after start of "Measurement Period"
			sort by start of relevantPeriod
	)

define "Denominator Exclusion":
	Hospice."Has Hospice"
		or exists "End Stage Renal Disease Procedures"
		or exists "Pregnancy Or End Stage Renal Disease Diagnoses Exclusion"

define "Pregnancy Or End Stage Renal Disease Diagnoses Exclusion":
	( ["Diagnosis": "Pregnancy"]
		union ["Diagnosis": "Kidney Transplant Recipient"]
		union ["Diagnosis": "End Stage Renal Disease"] ) ExclusionDiagnosis
		where ExclusionDiagnosis.prevalencePeriod overlaps "Measurement Period"

define "Followup Blood Pressure Visit":
	First(["Encounter, Performed": "Blood Pressure Visit"] FollowupBloodPressureVisit
			with "Initial Blood Pressure Visit" InitialBloodPressureVisit
				such that FollowupBloodPressureVisit.relevantPeriod starts 6 months or more after day of end of InitialBloodPressureVisit.relevantPeriod
					and FollowupBloodPressureVisit.relevantPeriod ends during "Measurement Period"
			sort by start of relevantPeriod
	)
