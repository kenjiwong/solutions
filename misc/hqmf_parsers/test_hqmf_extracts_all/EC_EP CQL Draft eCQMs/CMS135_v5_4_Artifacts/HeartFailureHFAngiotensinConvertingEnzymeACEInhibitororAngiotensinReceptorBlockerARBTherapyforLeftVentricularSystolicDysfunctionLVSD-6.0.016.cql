library HeartFailureHFAngiotensinConvertingEnzymeACEInhibitororAngiotensinReceptorBlockerARBTherapyforLeftVentricularSystolicDysfunctionLVSD version '6.0.016'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "ACE Inhibitor or ARB": 'urn:oid:2.16.840.1.113883.3.526.3.1139'
valueset "ACE Inhibitor or ARB Ingredient": 'urn:oid:2.16.840.1.113883.3.526.3.1489'
valueset "Allergy to ACE Inhibitor or ARB": 'urn:oid:2.16.840.1.113883.3.526.3.1211'
valueset "Care Services in Long-Term Residential Facility": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1014'
valueset "Discharge Services - Hospital Inpatient": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1007'
valueset "Ejection Fraction": 'urn:oid:2.16.840.1.113883.3.526.3.1134'
valueset "Heart Failure": 'urn:oid:2.16.840.1.113883.3.526.3.376'
valueset "Home Healthcare Services": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1016'
valueset "Intolerance to ACE Inhibitor or ARB": 'urn:oid:2.16.840.1.113883.3.526.3.1212'
valueset "Left Ventricular Systolic Dysfunction": 'urn:oid:2.16.840.1.113883.3.526.3.1091'
valueset "Medical Reason": 'urn:oid:2.16.840.1.113883.3.526.3.1007'
valueset "Moderate or Severe": 'urn:oid:2.16.840.1.113883.3.526.3.1092'
valueset "Moderate or Severe LVSD": 'urn:oid:2.16.840.1.113883.3.526.3.1090'
valueset "Nursing Facility Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1012'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Outpatient Consultation": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1008'
valueset "Patient Provider Interaction": 'urn:oid:2.16.840.1.113883.3.526.3.1012'
valueset "Patient Reason": 'urn:oid:2.16.840.1.113883.3.526.3.1008'
valueset "Patient Reason for ACE Inhibitor or ARB Decline": 'urn:oid:2.16.840.1.113883.3.526.3.1140'
valueset "Pregnancy": 'urn:oid:2.16.840.1.113883.3.526.3.378'
valueset "Renal Failure Due to ACE Inhibitor": 'urn:oid:2.16.840.1.113883.3.526.3.1151'
valueset "System Reason": 'urn:oid:2.16.840.1.113883.3.526.3.1009'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Face to Face Encounter During Measurement Period":
	( ["Encounter, Performed": "Care Services in Long-Term Residential Facility"]
		union ["Encounter, Performed": "Home Healthcare Services"]
		union ["Encounter, Performed": "Nursing Facility Visit"]
		union ["Encounter, Performed": "Office Visit"]
		union ["Encounter, Performed": "Outpatient Consultation"] ) FaceToFaceEncounter
		where FaceToFaceEncounter.relevantPeriod during "Measurement Period"

define "Inpatient Discharge During Measurement Period":
	["Encounter, Performed": "Discharge Services - Hospital Inpatient"] InpatientEncounter
		where InpatientEncounter.relevantPeriod during "Measurement Period"

define "Heart Failure Outpatient Encounter":
	"Face to Face Encounter During Measurement Period" FaceToFaceEncounter
		with ["Diagnosis": "Heart Failure"] HeartFailure
			such that HeartFailure.prevalencePeriod overlaps FaceToFaceEncounter.relevantPeriod

define "Heart Failure Inpatient Encounter":
	"Inpatient Discharge During Measurement Period" InpatientEncounter
		with ["Diagnosis": "Heart Failure"] HeartFailure
			such that HeartFailure.prevalencePeriod overlaps InpatientEncounter.relevantPeriod

define "Heart Failure Outpatient Encounter with Moderate or Severe LVSD":
	"Heart Failure Outpatient Encounter" HFOutpatientEncounter
		with "Moderate or Severe LVSD Findings" OutpatientLVSDFindings
			such that OutpatientLVSDFindings.prevalencePeriod starts before end of HFOutpatientEncounter.relevantPeriod
				or OutpatientLVSDFindings.relevantPeriod starts before end of HFOutpatientEncounter.relevantPeriod

define "Allergy or Intolerance to ACE Inhibitor or ARB Ingredient Outpatient":
	["Allergy/Intolerance": "ACE Inhibitor or ARB Ingredient"] ACEInhibitorOrARBAllergyIntolerance
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that ACEInhibitorOrARBAllergyIntolerance.prevalencePeriod overlaps after ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod

define "Allergy or Intolerance to ACE Inhibitor or ARB Ingredient Inpatient":
	["Allergy/Intolerance": "ACE Inhibitor or ARB Ingredient"] ACEInhibitorOrARBAllergyIntolerance
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that ACEInhibitorOrARBAllergyIntolerance.prevalencePeriod overlaps after ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod

define "Initial Population 2":
	exists ["Patient Characteristic Birthdate"] BirthDate
		where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 18
			and exists "Heart Failure Inpatient Encounter"

define "Diagnosis of Allergy to ACE Inhibitor or ARB Outpatient":
	["Diagnosis": "Allergy to ACE Inhibitor or ARB"] ACEInhibitorOrARBAllergyDiagnosis
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that ACEInhibitorOrARBAllergyDiagnosis.prevalencePeriod overlaps after ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod

define "Diagnosis of Allergy to ACE Inhibitor or ARB Inpatient":
	["Diagnosis": "Allergy to ACE Inhibitor or ARB"] ACEInhibitorOrARBAllergyDiagnosis
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that ACEInhibitorOrARBAllergyDiagnosis.prevalencePeriod overlaps after ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod

define "Diagnosis of Intolerance to ACE Inhibitor or ARB Outpatient":
	["Diagnosis": "Intolerance to ACE Inhibitor or ARB"] ACEInhibitorOrARBIntoleranceDiagnosis
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that ACEInhibitorOrARBIntoleranceDiagnosis.prevalencePeriod overlaps after ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod

define "Diagnosis of Intolerance to ACE Inhibitor or ARB Inpatient":
	["Diagnosis": "Intolerance to ACE Inhibitor or ARB"] ACEInhibitorOrARBIntoleranceDiagnosis
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that ACEInhibitorOrARBIntoleranceDiagnosis.prevalencePeriod overlaps after ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod

define "Diagnosis of Pregnancy Outpatient":
	["Diagnosis": "Pregnancy"] Pregnancy
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that Pregnancy.prevalencePeriod overlaps ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod

define "Diagnosis of Pregnancy Inpatient":
	["Diagnosis": "Pregnancy"] Pregnancy
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that Pregnancy.prevalencePeriod overlaps ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod

define "Diagnosis of Renal Failure Due to ACE Inhibitor Outpatient":
	["Diagnosis": "Renal Failure Due to ACE Inhibitor"] RenalFailureDueToACEInhibitor
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that RenalFailureDueToACEInhibitor.prevalencePeriod overlaps ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod

define "Diagnosis of Renal Failure Due to ACE Inhibitor Inpatient":
	["Diagnosis": "Renal Failure Due to ACE Inhibitor"] RenalFailureDueToACEInhibitor
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that RenalFailureDueToACEInhibitor.prevalencePeriod overlaps ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod

define "Medical Patient or System Reason for Not Ordering ACE Inhibitor or ARB Outpatient":
	["Medication, Not Ordered": "ACE Inhibitor or ARB"] NoACEInhibitorOrdered
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that NoACEInhibitorOrdered.authorDatetime during ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod
				and ( NoACEInhibitorOrdered.negationRationale in "Medical Reason"
						or NoACEInhibitorOrdered.negationRationale in "Patient Reason"
						or NoACEInhibitorOrdered.negationRationale in "System Reason"
				)

define "Medical Patient or System Reason for Not Ordering ACE Inhibitor or ARB Inpatient":
	["Medication, Not Ordered": "ACE Inhibitor or ARB"] NoACEInhibitorOrdered
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that NoACEInhibitorOrdered.authorDatetime during ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod
				and ( NoACEInhibitorOrdered.negationRationale in "Medical Reason"
						or NoACEInhibitorOrdered.negationRationale in "Patient Reason"
						or NoACEInhibitorOrdered.negationRationale in "System Reason"
				)

define "Denominator Exceptions 1":
	exists "Medical Patient or System Reason for Not Ordering ACE Inhibitor or ARB Outpatient"
		or exists "Patient Declined ACE Inhibitor or ARB Outpatient"
		or exists "Allergy or Intolerance to ACE Inhibitor or ARB Ingredient Outpatient"
		or exists "Diagnosis of Allergy to ACE Inhibitor or ARB Outpatient"
		or exists "Diagnosis of Intolerance to ACE Inhibitor or ARB Outpatient"
		or exists "Diagnosis of Pregnancy Outpatient"
		or exists "Diagnosis of Renal Failure Due to ACE Inhibitor Outpatient"

define "Denominator Exceptions 2":
	exists "Medical Patient or System Reason for Not Ordering ACE Inhibitor or ARB Inpatient"
		or exists "Patient Declined ACE Inhibitor or ARB Inpatient"
		or exists "Allergy or Intolerance to ACE Inhibitor or ARB Ingredient Inpatient"
		or exists "Diagnosis of Allergy to ACE Inhibitor or ARB Inpatient"
		or exists "Diagnosis of Intolerance to ACE Inhibitor or ARB Inpatient"
		or exists "Diagnosis of Pregnancy Inpatient"
		or exists "Diagnosis of Renal Failure Due to ACE Inhibitor Inpatient"

define "Heart Failure Inpatient Encounter with Moderate or Severe LVSD":
	"Heart Failure Inpatient Encounter" HFInpatientEncounter
		with "Moderate or Severe LVSD Findings" InpatientLVSDFindings
			such that InpatientLVSDFindings.prevalencePeriod starts before end of HFInpatientEncounter.relevantPeriod
				or InpatientLVSDFindings.relevantPeriod starts before end of HFInpatientEncounter.relevantPeriod

define "Denominator 1":
	"Initial Population 1"
		and exists "Heart Failure Outpatient Encounter with Moderate or Severe LVSD"

define "Denominator 2":
	"Initial Population 2"
		and exists "Heart Failure Inpatient Encounter with Moderate or Severe LVSD"

define "Numerator 1":
	exists "ACE Inhibitor or ARB Ordered Outpatient"
		or exists "Currently Taking ACE Inhibitor or ARB Outpatient"

define "Patient Declined ACE Inhibitor or ARB Inpatient":
	["Communication: From Patient To Provider": "Patient Reason for ACE Inhibitor or ARB Decline"] CommunicatePatientDeclinedACEInhibitorOrARB
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that CommunicatePatientDeclinedACEInhibitorOrARB.authorDatetime during ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod

define "Patient Declined ACE Inhibitor or ARB Outpatient":
	["Communication: From Patient To Provider": "Patient Reason for ACE Inhibitor or ARB Decline"] CommunicatePatientDeclinedACEInhibitorOrARB
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that CommunicatePatientDeclinedACEInhibitorOrARB.authorDatetime during ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod

define "ACE Inhibitor or ARB Ordered Inpatient":
	["Medication, Order": "ACE Inhibitor or ARB"] ACEInhibitorOrARBOrdered
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that ACEInhibitorOrARBOrdered.authorDatetime during ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod

define "Currently Taking ACE Inhibitor or ARB Inpatient":
	["Medication, Active": "ACE Inhibitor or ARB"] ActiveACEInhibitorOrARB
		with "Heart Failure Inpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFInpatientEncounter
			such that ActiveACEInhibitorOrARB.relevantPeriod overlaps after ModerateOrSevereLVSDHFInpatientEncounter.relevantPeriod

define "ACE Inhibitor or ARB Ordered Outpatient":
	["Medication, Order": "ACE Inhibitor or ARB"] ACEInhibitorOrARBOrdered
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that ACEInhibitorOrARBOrdered.authorDatetime during ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod

define "Currently Taking ACE Inhibitor or ARB Outpatient":
	["Medication, Active": "ACE Inhibitor or ARB"] ActiveACEInhibitorOrARB
		with "Heart Failure Outpatient Encounter with Moderate or Severe LVSD" ModerateOrSevereLVSDHFOutpatientEncounter
			such that ActiveACEInhibitorOrARB.relevantPeriod overlaps after ModerateOrSevereLVSDHFOutpatientEncounter.relevantPeriod

define "Numerator 2":
	exists "ACE Inhibitor or ARB Ordered Inpatient"
		or exists "Currently Taking ACE Inhibitor or ARB Inpatient"

define "Qualifying Encounter":
	( ["Encounter, Performed": "Office Visit"]
		union ["Encounter, Performed": "Outpatient Consultation"]
		union ["Encounter, Performed": "Nursing Facility Visit"]
		union ["Encounter, Performed": "Care Services in Long-Term Residential Facility"]
		union ["Encounter, Performed": "Home Healthcare Services"]
		union ["Encounter, Performed": "Patient Provider Interaction"] ) ValidEncounter
		where ValidEncounter.relevantPeriod during "Measurement Period"

define "Initial Population 1":
	exists ["Patient Characteristic Birthdate"] BirthDate
		where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 18
			and Count("Qualifying Encounter")>= 2
			and exists "Heart Failure Outpatient Encounter"

define "Moderate or Severe LVSD Findings":
	( ["Diagnostic Study, Performed": "Ejection Fraction"] EjectionFraction
			where EjectionFraction.result < 40 '%'
	)
		union ["Diagnosis": "Moderate or Severe LVSD"]
		union ( ["Diagnosis": "Left Ventricular Systolic Dysfunction"] LVSD
				where LVSD.severity in "Moderate or Severe"
		)
