library ControllingHighBloodPressure version '7.0.004'

using QDM version '5.3'

include Adult_Outpatient_Encounters version '1.1.000' called AdultOutpatientEncounters
include MATGlobalCommonFunctions version '2.0.000' called Global
include Hospice version '1.0.000' called Hospice

codesystem "LOINC:2.63": 'urn:oid:2.16.840.1.113883.6.1' version 'urn:hl7:version:2.63'

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Chronic Kidney Disease, Stage 5": 'urn:oid:2.16.840.1.113883.3.526.3.1002'
valueset "Dialysis Services": 'urn:oid:2.16.840.1.113883.3.464.1003.109.12.1013'
valueset "End Stage Renal Disease": 'urn:oid:2.16.840.1.113883.3.526.3.353'
valueset "ESRD Monthly Outpatient Services": 'urn:oid:2.16.840.1.113883.3.464.1003.109.12.1014'
valueset "Kidney Transplant": 'urn:oid:2.16.840.1.113883.3.464.1003.109.12.1012'
valueset "Pregnancy": 'urn:oid:2.16.840.1.113883.3.526.3.378'
valueset "Vascular Access for Dialysis": 'urn:oid:2.16.840.1.113883.3.464.1003.109.12.1011'
valueset "Adult Outpatient Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1065'
valueset "Essential Hypertension": 'urn:oid:2.16.840.1.113883.3.464.1003.104.12.1011'
valueset "Kidney Transplant Recipient": 'urn:oid:2.16.840.1.113883.3.464.1003.109.12.1029'

code "Diastolic blood pressure": '8462-4' from "LOINC:2.63" display 'Diastolic blood pressure'
code "Systolic blood pressure": '8480-6' from "LOINC:2.63" display 'Systolic blood pressure'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Denominator":
	"Initial Population"

define "Systolic Blood Pressure Exam":
	["Physical Exam, Performed": "Systolic blood pressure"] SystolicBP
		where SystolicBP.result is not null

define "Diastolic Blood Pressure Exam":
	["Physical Exam, Performed": "Diastolic blood pressure"] DiastolicBP
		where DiastolicBP.result is not null

define "End Stage Renal Disease Procedures":
	( ["Procedure, Performed": "Vascular Access for Dialysis"]
		union ["Procedure, Performed": "Kidney Transplant"]
		union ["Procedure, Performed": "Dialysis Services"] ) ESRDProcedure
		where ESRDProcedure.relevantPeriod starts before end of "Measurement Period"

define "End Stage Renal Disease Encounter":
	["Encounter, Performed": "ESRD Monthly Outpatient Services"] ESRDEncounter
		where ESRDEncounter.relevantPeriod starts before end of "Measurement Period"

define "Essential Hypertension Diagnosis":
	["Diagnosis": "Essential Hypertension"] Hypertension
		where Hypertension.prevalencePeriod starts 6 months or less on or after start of "Measurement Period"
			or Hypertension.prevalencePeriod overlaps before "Measurement Period"

define "Numerator":
	"Has Diastolic Blood Pressure Less Than 90"
		and "Has Systolic Blood Pressure Less Than 140"

define "Most Recent Visit With Blood Pressure":
	Last((["Encounter, Performed": "Adult Outpatient Visit"] OutpatientVisit
			with "Essential Hypertension Diagnosis" HypertensionDiagnosis
				such that HypertensionDiagnosis.prevalencePeriod overlaps OutpatientVisit.relevantPeriod
			with "Diastolic Blood Pressure Exam" DiastolicBP
				such that OutpatientVisit.relevantPeriod overlaps DiastolicBP.relevantPeriod
			with "Systolic Blood Pressure Exam" SystolicBP
				such that OutpatientVisit.relevantPeriod overlaps SystolicBP.relevantPeriod)VisitWithBP
			where VisitWithBP.relevantPeriod during "Measurement Period"
			sort by start of relevantPeriod
	)

define "Denominator Exclusions":
	Hospice."Has Hospice"
		or exists "Pregnancy Or Renal Diagnosis Exclusions"
		or ( exists "End Stage Renal Disease Procedures"
				or exists "End Stage Renal Disease Encounter"
		)

define "Initial Population":
	exists ( ["Patient Characteristic Birthdate"] BirthDate
			where Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 18
				and Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")< 85
	)
		and exists "Essential Hypertension Diagnosis"
		and exists AdultOutpatientEncounters."Qualifying Encounters"

define "Pregnancy Or Renal Diagnosis Exclusions":
	( ["Diagnosis": "Pregnancy"]
		union ["Diagnosis": "End Stage Renal Disease"]
		union ["Diagnosis": "Kidney Transplant Recipient"]
		union ["Diagnosis": "Chronic Kidney Disease, Stage 5"] ) PregnancyESRDDiagnosis
		where PregnancyESRDDiagnosis.prevalencePeriod overlaps "Measurement Period"

define "Has Systolic Blood Pressure Less Than 140":
	Last("Systolic Blood Pressure Exam" LastSystolicBP
			with "Most Recent Visit With Blood Pressure" HypertensionNumeratorEncounter
				such that LastSystolicBP.relevantPeriod during HypertensionNumeratorEncounter.relevantPeriod
			sort by start of relevantPeriod
	).result < 140 'mm[Hg]'

define "Has Diastolic Blood Pressure Less Than 90":
	Last("Diastolic Blood Pressure Exam" LastDiastolicBP
			with "Most Recent Visit With Blood Pressure" HypertensionNumeratorEncounter
				such that LastDiastolicBP.relevantPeriod during HypertensionNumeratorEncounter.relevantPeriod
			sort by start of relevantPeriod
	).result < 90 'mm[Hg]'
