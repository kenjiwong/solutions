from abc import ABCMeta, abstractmethod
import uuid
from datetime import datetime
from functools import wraps
import re
from lxml.etree import _Comment

ALLERGY_TEMPLATE_IDS = [
    "1.3.6.1.4.1.19376.1.5.3.1.3.13",
    "2.16.840.1.113883.10.20.1.2",
    "2.16.840.1.113883.3.88.11.83.102",
    "2.16.840.1.113883.10.20.22.2.6.1"
]
IMMUNIZATION_TEMPLATE_IDS = [
    "1.3.6.1.4.1.19376.1.5.3.1.3.23",
    "2.16.840.1.113883.3.88.11.83.117",
    "2.16.840.1.113883.10.20.1.6",
    "2.16.840.1.113883.10.20.22.2.2.1"
]
CONDITION_TEMPLATE_IDS = [
    "2.16.840.1.113883.10.20.1.11",
    "2.16.840.1.113883.10.20.22.2.5.1"
]
MEDICATION_TEMPLATE_IDS = [
    "2.16.840.1.113883.10.20.1.8",
    "2.16.840.1.113883.3.88.11.83.112",
    "1.3.6.1.4.1.19376.1.5.3.1.3.19",
    "2.16.840.1.113883.10.20.22.2.1.1"
]
OBSERVATION_TEMPLATE_IDS = [
    "2.16.840.1.113883.10.20.1.14",
    "1.3.6.1.4.1.19376.1.5.3.1.3.27",
    "1.3.6.1.4.1.19376.1.5.3.1.3.28",
    "2.16.840.1.113883.3.88.11.83.122",
    "2.16.840.1.113883.10.20.22.2.3.1",
    "2.16.840.1.113883.10.20.22.2.4.1"
]
COGNITIVE_TEMPLATE_IDS = [
    "2.16.840.1.113883.10.20.22.2.14"
]
PROCEDURE_TEMPLATE_IDS = [
    "2.16.840.1.113883.10.20.22.2.7.1"
]
ENCOUNTER_TEMPLATE_IDS = [
    "2.16.840.1.113883.10.20.22.2.22.1"
]
PAYER_TEMPLATE_IDS = [
    "2.16.840.1.113883.10.20.22.2.18"
]

PREFERRED_CODE_SYSTEMS = [
    "2.16.840.1.113883.6.96",
    "2.16.840.1.113883.6.1",
    "2.16.840.1.113883.12.292",
    "2.16.840.1.113883.6.88"
]

CANONICAL_FORMAT = "%Y-%m-%d %H:%M:%S"
YYYY = r"^((19|20)[0-9]{2})"        # year 1900-2099
MM = r"(0[1-9]|1[0-2])"              # month 01-09 or 10, 11, 12
DD = r"(0[1-9]|(1|2)[0-9]|3[0-1])"   # day-of-month 01-09, or 10-29, or 30, 31
HH = r"((0|1)[0-9]|2[0-3])"          # hour-of-day 00-09, 10-19, 20-23
mm = r"([0-5][0-9])"                 # minute-of-hour 00-59
ss = mm                              # second of hour
YYYYMMD = YYYY + MM + DD
YYYYMMDDHH = YYYYMMD + HH
YYYYMMDDHHmm = YYYYMMDDHH + mm
YYYYMMDDHHmmss = YYYYMMDDHHmm + ss
YYYYMMDD = YYYY + MM + DD


class _DatetimeParser(object):

    @classmethod
    def to_datetime(cls, datestring):
        if isinstance(datestring, str) and cls.PATTERN.match(datestring):
            timekwargs = cls.time_kwargs(datestring)
            return datetime(cls.year(datestring),
                            cls.month(datestring),
                            cls.day(datestring),
                            **timekwargs)

    @classmethod
    def time_kwargs(cls, datestring):
        return {}


class _YYYYMMDD_Parser(_DatetimeParser):

    PATTERN = re.compile(YYYYMMDD + '$')

    @classmethod
    def year(cls, datestring):
        return int(datestring[:4])

    @classmethod
    def month(cls, datestring):
        return int(datestring[4:6])

    @classmethod
    def day(cls, datestring):
        return int(datestring[6:8])


class _YYYYMMDDHH_Parser(_YYYYMMDD_Parser):

    PATTERN = re.compile(YYYYMMDDHH + '$')

    @classmethod
    def hour(cls, datestring):
        return int(datestring[8:10])

    @classmethod
    def time_kwargs(cls, datestring):
        kwargs = super(_YYYYMMDDHH_Parser, cls).time_kwargs(datestring)
        kwargs['hour'] = cls.hour(datestring)
        return kwargs


class _YYYYMMDDHHmm_Parser(_YYYYMMDDHH_Parser):

    PATTERN = re.compile(YYYYMMDDHHmm + '$')

    @classmethod
    def minute(cls, datestring):
        return int(datestring[10:12])

    @classmethod
    def time_kwargs(cls, datestring):
        kwargs = super(_YYYYMMDDHHmm_Parser, cls).time_kwargs(datestring)
        kwargs['minute'] = cls.minute(datestring)
        return kwargs


class _YYYYMMDDHHmmss_Parser(_YYYYMMDDHHmm_Parser):

    PATTERN = re.compile(YYYYMMDDHHmmss + '$')

    @classmethod
    def second(cls, datestring):
        return int(datestring[12:14])

    @classmethod
    def time_kwargs(cls, datestring):
        kwargs = super(_YYYYMMDDHHmmss_Parser, cls).time_kwargs(datestring)
        kwargs['second'] = cls.second(datestring)
        return kwargs


class DateParser(object):

    PARSERS = {
        8: _YYYYMMDD_Parser,
        10: _YYYYMMDDHH_Parser,
        12: _YYYYMMDDHHmm_Parser,
        14: _YYYYMMDDHHmmss_Parser,
    }

    @classmethod
    def to_datetime(cls, value):
        datestring = str(value)
        parser = cls.PARSERS.get(len(datestring))
        if parser:
            return parser.to_datetime(datestring)

    @classmethod
    def to_datetime_extended(cls, value):
        dt = cls.to_datetime(value)
        return dt or _YYYYMMDD_Parser.to_datetime(str(value))

    @classmethod
    def to_string(cls, datetyme):
        if datetyme:
            return datetyme.strftime(CANONICAL_FORMAT)


def etree_element(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        el = args[1] if isinstance(args[0], _CCDUtils) else args[0]
        if el is None or isinstance(el, _Comment):
            return None
        return func(*args, **kwargs)
    return wrapper


class _CCDUtils(object):

    def __init__(self, ccd_tree, ns):
        self.ns = ns
        self.ccd_tree = ccd_tree
        self.root_el = self.get_root_el(ccd_tree)
        self.html = self.get_html(self.root_el)

    @classmethod
    def generate_id(cls):
        return uuid.uuid4().hex

    @abstractmethod
    def parse(self):
        pass

    @staticmethod
    @etree_element
    def el_val(el):
        return el.text if el.text else None

    @staticmethod
    def parse_datetime_str(date_str, return_str=True):
        datetime = DateParser.to_datetime(date_str)
        if return_str and datetime:
            return DateParser.to_string(datetime)
        return datetime

    @etree_element
    def attr_val(self, el, attr_name, tag=None, ns=None):
        if tag:
            el = self.rec_find(el, tag)
        attr_value = None
        if el is not None:
            if ns:
                attr_value = el.attrib.get(self.ns_attr(attr_name, ns))
            else:
                attr_value = el.attrib.get(attr_name)
        return attr_value if attr_value else ''

    def dereference_text(self, text_el):
        '''
        public static string DereferenceText(XElement textEl, XElement sectionText)
        {
            string text = "";

            if (textEl != null && textEl.Attribute("nullFlavor") == null && sectionText != null)
            {
                if (textEl.HasElements)
                {
                    var reference = textEl.Element(V3() + "reference");
                    if (reference != null && reference.Attribute("value") != null)
                    {
                        var referencedElement = sectionText.Descendants().FirstOrDefault(el => el.Attribute("ID") != null && el.Attribute("ID").Value == reference.Attribute("value").Value.Substring(1));
                        if (referencedElement != null)
                        {
                            text = referencedElement.Value;
                        }
                    }
                }
                else
                {
                    text = textEl.Value;
                }
            }

            return text;

        }
        '''
        text = None
        if text_el is not None and not text_el.attrib.get('nullFlavor') and self.html is not None:
            if list(text_el):
                ref_el = self.find(text_el, 'CDA:reference')
                ref_el_attr_val = self.attr_val(ref_el, 'value')
                if ref_el is not None and ref_el_attr_val:
                    id_val = ref_el_attr_val.split('#')[1]
                    html_ref_el = self.find_html_by_id(id_val)
                    if html_ref_el is not None:
                        text = html_ref_el.text
            else:
                text = text_el.text
        return text

    def find_html_by_id(self, id_val):
        for el in self.findall(self.html, './/'):
            if el.attrib.get('ID') == id_val:
                return el

    @etree_element
    def get_html(self, el):
        return self.rec_find(el, 'table')


    @etree_element
    def get_range_high(self, el, use_el_val=False):
        if el is None: return None
        el_high = self.rec_find(el, 'high')
        if el_high is None or el_high.attrib.get('nullFlavor'):
            return el.attrib.get('value') if use_el_val else None
        else:
            el_high_value_text = el_high.attrib.get('value')
            return el_high_value_text if el_high_value_text else el_high.text

    @etree_element
    def get_range_low(self, el, use_el_val=True):
        if el is None: return None
        el_low = self.get_CDA_child(el, 'low')
        if el_low is None or el_low.attrib.get('nullFlavor'):
            return el.attrib.get('value') if use_el_val else None
        else:
            el_low_attr_value = el_low.attrib.get('value')
            return el_low_attr_value if el_low_attr_value else el_low.text

    def get_date_range(self, el, use_el_val=True):
        dt_low = self.parse_datetime_str(self.get_range_low(el, use_el_val))
        dt_high = self.parse_datetime_str(self.get_range_high(el, use_el_val))
        return dt_low or '', dt_high or ''

    @etree_element
    def get_root_el(self, el):
        sections = self.xpath(el, 'CDA:component/CDA:structuredBody/CDA:component/CDA:section')
        for section in sections:
            for template_id in self.xpath(section, 'CDA:templateId'):
                if template_id.attrib['root'] in self.template_ids:
                    return section

    def iter_entries(self):
    	if self.root_el is not None:
    		for childEl in self.root_el.iterchildren():
    			if self.has_CDA_tag(childEl, 'entry'):
    				yield childEl

    def ns_attr(self, attr, ns):
        return '{' + self.ns.get(ns) + '}' + attr

    @etree_element
    def readable_tag(self, el):
        return el.tag.replace('{' + self.ns['CDA'] + '}', '')

    @etree_element
    def rec_find(self, el, tag):
        el_return = None
        if len(el):
            for child_el in el:
                child_tag = self.readable_tag(child_el)
                if tag == child_tag:
                    el_return = child_el
                    break
                elif len(child_el):
                    el_return = self.rec_find(child_el, tag)
                    if el_return is not None:
                        break
        return el_return

    @etree_element
    def rec_find_all(self, el, tag, els_return=None):
        if els_return is None:
            els_return = list()
        if len(el):
            for child_el in el:
                child_tag = self.readable_tag(child_el)
                if tag == child_tag:
                    els_return.append(child_el)
                if len(child_el):
                    els_return + self.rec_find_all(child_el, tag, els_return)
        return els_return

    @etree_element
    def xpath(self, el, query):
        return el.xpath(query, namespaces=self.ns)

    def extract_id(self, el):
        if self.is_null(el): return ''
        ext = el.attrib.get('extension', '')
        return ext if len(ext) > 0 else el.attrib.get('root', '')

    def quote(self, s):
        # TESTING!
        return s
        # On platform, use quotes to prevent code values from losing leading zeroes
        # return '"{}"'.format(s)

    #=========================================================================================
    # Tools for navigating CDA via lxml/etree
    #=========================================================================================

    def ns_tag(self, tag, ns):
    	'''Build a fully-qualified tag, including namespace.'''
    	return '{' + ns + '}' + tag

    def has_tag(self, el, tag, ns):
    	'''Check if an element has tag name and namespace.'''
    	if isinstance(el.tag, basestring) and el.tag == self.ns_tag(tag, ns):
    		return True
    	else:
    		return False

    def is_CDA_el(self, el):
    	'''Verify that element is not a comment or whitespace, and that it's a CDA element.'''
    	return ( isinstance(el.tag, basestring) and el.tag.startswith('{' + self.ns['CDA'] + '}') )

    def CDA_tag(self, tag):
    	'''Given just a tag, build a fully-qualified tagname in the CDA namespace.'''
    	return self.ns_tag(tag, self.ns['CDA'])

    def has_CDA_tag(self, el, tag):
    	"""Check if an element has tag name in the CDA namespace."""
    	return self.has_tag(el, tag, self.ns['CDA'])

    def el_negated(self, el):
    	"""Check whether an element is negated via negationInd attribute."""
    	return (el.get('negationInd', 'false').lower() == 'true')

    def get_related_entry(self, el, relationshipType, entryTag):
    	"""For an element, find entryRelationship child with given typeCode and entry tag."""
    	if el is not None:
    		for relationshipEl in el.iterchildren():
    			if self.has_CDA_tag(relationshipEl, 'entryRelationship') and relationshipEl.get('typeCode') == relationshipType:
    				for childEl in relationshipEl.iterchildren():
    					if self.has_CDA_tag(childEl, entryTag):
    						return childEl
    	return None

    def get_CDA_child(self, el, tag):
    	"""Return first child with given tag in CDA namespace."""
    	if el is not None:
    		for childEl in el.iterchildren():
    			if self.has_CDA_tag(childEl, tag):
    				return childEl
    	return None

    def get_CDA_children(self, el, tag):
    	"""Return generator for children with given tag in CDA namespace."""
    	if el is not None:
    		for childEl in el.iterchildren():
    			if self.has_CDA_tag(childEl, tag):
    				yield childEl

    def get_child(self, el, tag, NS):
    	"""Return first child with given tag in given namespace."""
    	if el is not None:
    		for childEl in el.iterchildren():
    			if self.has_tag(childEl, tag, NS):
    				return childEl
    	return None

    def get_non_null_CDA_child(self, el, tag):
    	"""Return first child with tag in CDA namespace, excluding any with nullFlavor."""
    	childEl = self.get_CDA_child(el, tag)
    	if childEl is None:
    		return None
    	elif childEl.attrib.get('nullFlavor', None) is None:
    		return None
    	else:
    		return childEl

    def is_null(self, el):
    	"""Check if element is None or has nullFlavor."""
    	if el is None:
    		return True
    	else:
    		return not ( el.attrib.get('nullFlavor', None) is None )

    def get_CDA_child_with_att(self, el, tag, attName, attValue):
    	"""Return first child with tag name and given attribute value."""
    	if el is not None:
    		for childEl in el.iterchildren():
    			if self.has_CDA_tag(childEl, tag):
    				if childEl.attrib.get(attName) == attValue:
    					return childEl
    	return None

    def get_addr(self, el):
        streetAddressLine = self.get_CDA_child(el, 'streetAddressLine')
        city = self.get_CDA_child(el, 'city')
        state = self.get_CDA_child(el, 'state')
        postalCode = self.get_CDA_child(el, 'postalCode')
        country = self.get_CDA_child(el, 'country')
        return (
            '' if self.is_null(streetAddressLine) else streetAddressLine.text,
            '' if self.is_null(city) else city.text,
            '' if self.is_null(state) else state.text,
            '' if self.is_null(postalCode) else postalCode.text,
            '' if self.is_null(country) else country.text
        )


    def parse_observation(self, obs_el):
        id = self.extract_id(self.get_CDA_child(obs_el, 'id'))

        eff_time_el = self.get_CDA_child(obs_el, 'effectiveTime')
        start_date, end_date = self.get_date_range(eff_time_el)

        code = code_system = ''
        code_el = self.get_CDA_child(obs_el, 'code')
        if not self.is_null(code_el):
            code = code_el.attrib.get('code', '')
            code_system = code_el.attrib.get('codeSystem', '')

        value_el = self.get_CDA_child(obs_el, 'value')
        value_type = value_el.attrib.get(self.ns_tag('type', self.ns['XSI']), '')
        if value_type == "ED":
            value = value_el.text
            value_unit = ''
            value_code = ''
            value_code_system = ''
        elif value_type in ["CO", "CS", "CV", "CD"]:
            value = ''
            value_unit = ''
            value_code = value_el.attrib.get('code', '')
            value_code_system = value_el.attrib.get('codeSystem', '')

            # Check if code system is not preferredm and a translation has been provided
            translation_code_el = self.get_CDA_child(value_el, 'translation')
            if translation_code_el is not None and value_code_system not in PREFERRED_CODE_SYSTEMS:
                translation_code = translation_code_el.attrib.get('code', '')
                translation_code_system = translation_code_el.attrib.get('codeSystem', '')

                # If the translation IS preferred, use it instead
                if translation_code_system in PREFERRED_CODE_SYSTEMS:
                    value_code = translation_code
                    value_code_system = translation_code_system

        else:
            value = value_el.attrib.get('value', '')
            value_unit = value_el.attrib.get('unit', '')
            value_code = ''
            value_code_system = ''

        return (id, start_date, end_date, code, code_system, value_type, value, value_unit, value_code, value_code_system)

    def parse_procedure(self, procedure_el):

        mood_code = procedure_el.attrib.get('moodCode', '')

        id = self.extract_id(self.get_CDA_child(procedure_el, 'id'))

        eff_time = self.get_CDA_child(procedure_el, 'effectiveTime')
        start_date, end_date = self.get_date_range(eff_time)

        code_el = self.get_CDA_child(procedure_el, 'code')
        if not self.is_null(code_el):
            code = code_el.attrib.get('code', '')
            code_system = code_el.attrib.get('codeSystem', '')

        return (id, mood_code, start_date, end_date, code, code_system)
