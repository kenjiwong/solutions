from utils import _CCDUtils, OBSERVATION_TEMPLATE_IDS


class ObservationParser(_CCDUtils):
    section = 'Observation'
    columns = [
        'event_class',
        'observation_id',
        'start_date',
        'end_date',
        'code',
        'code_system',
        'result_type',
        'result',
        'result_unit',
        'result_code',
        'result_code_system'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = OBSERVATION_TEMPLATE_IDS
        super(ObservationParser, self).__init__(ccd_tree, ns)


    def recursive_parse(self, entry, observation_data):

        for organizer_or_observation in entry:
            if self.has_CDA_tag(organizer_or_observation, 'organizer'):
                for component in self.get_CDA_children(organizer_or_observation, 'component'):
                    self.recursive_parse(component, observation_data)

            elif self.has_CDA_tag(organizer_or_observation, 'observation'):
                obs_el = organizer_or_observation

                id, start_date, end_date, code, code_system, value_type, value, value_unit, value_code, value_code_system = self.parse_observation(obs_el)

                observation_data.append({
                        'event_class': self.section,
                        'observation_id': id,
                        'start_date': start_date,
                        'end_date': end_date,
                        'code': self.quote(code),
                        'code_system': code_system,
                        'result_type': value_type,
                        'result': value,
                        'result_unit': value_unit,
                        'result_code': self.quote(value_code),
                        'result_code_system': value_code_system
                })


    def parse(self):

        observation_data = []

        for entry in self.iter_entries():
            self.recursive_parse(entry, observation_data)

        return observation_data
