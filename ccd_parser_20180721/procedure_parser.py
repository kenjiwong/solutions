from utils import _CCDUtils, PROCEDURE_TEMPLATE_IDS


class ProcedureParser(_CCDUtils):
    section = 'Procedure'
    columns = [
        'event_class',
        'procedure_id',
        'mood_code',
        'start_date',
        'end_date',
        'code',
        'code_system',
        'result_type',
        'result',
        'result_unit',
        'result_code',
        'result_code_system'
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = PROCEDURE_TEMPLATE_IDS
        super(ProcedureParser, self).__init__(ccd_tree, ns)

    def parse(self):
        procedure_data = []

        for entry in self.iter_entries():

            procedure_el = self.get_CDA_child(entry, 'procedure')
            if procedure_el is not None:

                id, mood_code, start_date, end_date, code, code_system = self.parse_procedure(procedure_el)

                procedure_data.append({
                    'event_class': self.section,
                    'procedure_id': id,
                    'mood_code': mood_code,
                    'start_date': start_date,
                    'end_date': end_date,
                    'code': self.quote(code),
                    'code_system': code_system,
                    'result_type': '',
                    'result': '',
                    'result_unit': '',
                    'result_code': '',
                    'result_code_system': ''
                })

            else:
                obs_el = self.get_CDA_child(entry, 'observation')
                if obs_el is not None:

                    id, start_date, end_date, code, code_system, value_type, value, value_unit, value_code, value_code_system = self.parse_observation(obs_el)

                    procedure_data.append({
                            'event_class': self.section,
                            'procedure_id': id,
                            'mood_code': 'EVN',
                            'start_date': start_date,
                            'end_date': end_date,
                            'code': self.quote(code),
                            'code_system': code_system,
                            'result_type': value_type,
                            'result': value,
                            'result_unit': value_unit,
                            'result_code': self.quote(value_code),
                            'result_code_system': value_code_system
                    })


        return procedure_data
