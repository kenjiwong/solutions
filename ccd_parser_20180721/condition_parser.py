from utils import _CCDUtils, CONDITION_TEMPLATE_IDS


class ConditionParser(_CCDUtils):
    section = 'Condition'
    columns = [
        'event_class',
        'conditionId',
        'start_date',
        'end_date',
        'onset_date',
        'abatement_date',
        'code_system',
        'code',
        'display',
        'negated',
    ]

    def __init__(self, ccd_tree, ns):
        self.template_ids = CONDITION_TEMPLATE_IDS
        super(ConditionParser, self).__init__(ccd_tree, ns)

    def parse(self):
        condition_data = []

        for entry in self.iter_entries():

            act_el = self.get_CDA_child(entry, 'act')

            id = self.extract_id(self.get_CDA_child(act_el, 'id'))

            eff_clinician = self.get_CDA_child(act_el, 'effectiveTime')
            start_date, end_date = self.get_date_range(eff_clinician)

            subject_el = None
            for rel in self.get_CDA_children(act_el, 'entryRelationship'):
                if rel.attrib.get('inversionInd', 'false').lower() == 'false':
                    subject_el = rel
                    break
            obs_el = self.get_CDA_child(subject_el, 'observation')

            eff_patient = self.get_CDA_child(obs_el, 'effectiveTime')
            onset_date, abatement_date = self.get_date_range(eff_patient)

            code = ''
            code_system = ''
            display = ''

            value_el = self.get_CDA_child(obs_el, 'value')
            if value_el is not None:
                if 'nullFlavor' not in value_el.attrib:
                    code = value_el.attrib.get('code', '')
                    code_system = value_el.attrib.get('codeSystem', '')
                    display = value_el.attrib.get('displayName', '')
                else:
                    translation_el = self.get_CDA_child(value_el, 'translation')
                    if translation_el is not None:
                        code = translation_el.attrib.get('code', '')
                        code_system = translation_el.attrib.get('codeSystem', '')
                        display = translation_el.attrib.get('displayName', '')

            negated = 'true' if (obs_el.attrib.get('inversionInd', 'false').lower() == 'true') else 'false'

            entry_data = dict(
                event_class=self.section,
                conditionId=id,
                start_date=start_date,
                end_date=end_date,
                onset_date=onset_date,
                abatement_date=abatement_date,
                code_system=code_system,
                code=self.quote(code),
                display=display,
                negated=negated
            )

            condition_data.append(entry_data)

        return condition_data
