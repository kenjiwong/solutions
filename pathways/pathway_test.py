'''
Author: Kenji Wong
Date: 6/20/2018
Purpose: Create testing script for pathways
'''

import os
import hmac
from Crypto.Hash import SHA256
import datetime
import requests
import json
import uuid

public_key = ''
private_key = ''
firewall_credentials = ('apervita.tester', 'bl00dm00n')

class ApiClient(object):

    def __init__(self):
        self.name = 'Kenji API Client'
        self.api_key = public_key
        self.private_key = private_key
        self.http_method = 'https://'
        self.url = 'www.computehealth.com'
        self.firewall_credentials = firewall_credentials

    def file_upload(self, file_source_path, idcolumn, delimiter):
        rest_type = 'post'
        path = '/api/v2/population/loadfile/json'
        filename = file_source_path.split('/')[-1]

        response = self.assemble_call(rest_type=rest_type, path=path, file_source_path=file_source_path, filename=filename, idcolumn=idcolumn, delimiter=delimiter)

        return response

    def load_file(self, populationid, bulkloadid):
        rest_type = 'get'
        path = '/api/v2/population/loadfacts/json'

        response = self.assemble_call(rest_type=rest_type, path=path, populationid=populationid, bulkloadid=bulkloadid)

        return response

    def purge_population(self, populationid, onbehalfid):
        rest_type = 'post'
        path = '/api/v2/population/purgefacts/json'

        response = self.assemble_call(rest_type=rest_type, path=path, populationid=populationid, onbehalfid=onbehalfid)

        return response

    def pathways_single_subject_calculator(self, json, raw_output=False):
        rest_type = 'post'
        path = '/api/v2/pathway/json'

        response = self.assemble_call(rest_type=rest_type, path=path, json=json, raw_output=raw_output)

        return response

    def single_subject_calculator(self, json, insightid):
        rest_type = 'post'
        path = '/api/v2/subject/calculate/json'

        response = self.assemble_call(rest_type=rest_type, path=path, json=json, insightid=insightid)

        return response

    def search_population(self, json, populationid):
        rest_type = 'post'
        path = '/api/v3/population/search/{0}/json'.format(populationid)

        response = self.assemble_call(rest_type=rest_type, path=path, json=json, insightid=populationid)

        return response

    def assemble_call(self, rest_type, path, file_source_path=None, json=None, **kwargs):
        current_timestamp = datetime.datetime.utcnow().replace(microsecond=0).isoformat()
        parameters = {'api_key': self.api_key, 'timestamp': current_timestamp}

        for key, value in kwargs.iteritems():
            parameters[key] = value

        response = self.make_call(rest_type, path, parameters, file_source_path, json)

        return response

    def make_call(self, rest_type, path, parameters, file_source_path=None, json=None):
        url_parameters = self.add_signature(rest_type, path, parameters)
        url = self.create_url(path, url_parameters)

        if rest_type == 'post':
            print('Method: POST')
            print('Final URL: {0}'.format(url))

            # Open up file and make post request
            if file_source_path:
                upload_file = open(file_source_path, 'rb')
                response = requests.post(url, files={'uploadfile': upload_file}, auth=self.firewall_credentials)
            elif json:
                response = requests.post(url, json=json, auth=self.firewall_credentials)
            else:
                response = requests.post(url, auth=self.firewall_credentials)
            print(response.text)
        elif rest_type == 'get':
            print('Method: GET')
            print('Final URL: {0}'.format(url))

            # Make get request
            response = requests.get(url, auth=self.firewall_credentials)
            print(response.text)
        else:
            raise Exception('Unable to use rest type: {}'.format(rest_type))

        dashes = '-' * 150
        print(dashes)

        return response

    def add_signature(self, rest_type, path, parameters):
        parameters_list = ['{0}={1}'.format(key, parameters[key]) for key in sorted(parameters.keys())]
        signature_list = [rest_type, self.url, path, '&'.join(parameters_list)]
        prep_sign = '|'.join(signature_list).lower()
        print('Prepare Signature: {0}'.format(prep_sign))

        # Create signature SHA-256
        signature = hmac.new(self.private_key, prep_sign, SHA256).hexdigest()

        # Signature to dict
        parameters['signature'] = signature

        return parameters

    def create_url(self, path, parameters):
        parameters_list = ['{0}={1}'.format(key, parameters[key]) for key in sorted(parameters.keys())]
        url_list = [self.http_method, self.url, path+'?', '&'.join(parameters_list)]

        # Create URL
        url = ''.join(url_list).lower()

        return url

class PathwayTester(object):

    def __init__(self):
        self.name = 'Kenji Pathway Tester'
        self.ApiClient = ApiClient()

    def pathway_workflow_a(self):
        pass

    def initial_data_upload(self, source_path, idcolumn, delimiter, populationid):
        for subdir, dirs, files in os.walk(source_path):
            for file in files:
                file_path = os.path.join(subdir, file)
                self.upload_and_load_file(file_path, idcolumn, delimiter, populationid)

    def upload_and_load_file(self, file_source_path, idcolumn, delimiter, populationid):
        bulkloadid = json.loads(self.ApiClient.file_upload(file_source_path, idcolumn, delimiter).text)['data']['bulkloadid']
        self.ApiClient.load_file(populationid, bulkloadid)

    def single_subject_execute_and_save(self, insightid):
        subject = {
            "subject": {
                "subjectid": "3d86541d-95a9-40b2-8592-7c6bc805b018",
                "events": [{
                    "patientid": "95001",
                    "bp_goal_diastolic": None,
                    "bp_goal_systolic": None,
                    "ckd": "Yes",
                    "current_state": None,
                    "diabetes_or_ckd_present": "Yes",
                    "hypertension": "Yes",
                    "most_recent_blood_pressure_measurement_diastolic": 200,
                    "most_recent_blood_pressure_measurement_systolic": 90,
                    "pathway_enrollment": "Yes",
                    "patient_age": 63,
                    "race": "Black",
                },
            ]
            }
        }

        search_criteria = {
            'search_criteria': [{
                'search_rule': 'EXACT',
                'concept_id': 'patientid',
                'value': '95001'
            }],
            'populationid': None
        }

        search_criteria['populationid'] = populationid

        self.ApiClient.search_population(search_criteria, populationid).text

        calculation_results = json.loads(self.ApiClient.single_subject_calculator(subject, insightid).text)['data']

        output_file = open('temp/output.txt', 'w')

        output_file.write('|'.join([str(key) for key in calculation_results.keys()]))
        output_file.write('\n')
        output_file.write('|'.join([str(value) for value in calculation_results.values()]))

        self.upload_and_load_file('/Users/kenji.wong/PycharmProjects/solutions/pathways/temp/output.txt', 'patientid', '|', populationid)

        self.ApiClient.search_population(search_criteria, populationid).text

    def test_pathway_api(self, populationid=None, insightid=None, inputadapterid=None, outputadapterid=None, already_ssjson=False):

        output_json = {
            "subject_data": {
                "subjectid": str(uuid.uuid4()),
                "events": [{
                    "Test": 1
                },]
            },
        "insightid": insightid,
        "already_ssjson": already_ssjson
        }

        self.ApiClient.pathways_single_subject_calculator(output_json)


    def execution_clean_up(self, population_id, onbehalfid):
        self.ApiClient.purge_population(population_id, onbehalfid)

if __name__ == '__main__':
    # Create pathway tester
    pathway_tester = PathwayTester()

    # Input parameters
    test_file_source_path = '/Users/kenji.wong/PycharmProjects/solutions/pathways/initial_files'
    populationid = '4b4m-376mtd.1.0'
    onbehalfid = 'kenji.wong@apervita.com'

    # Execute workflow
    pathway_tester.test_pathway_api("46ub-37dvc3.1.0", "trt-aqmw8r.1.0", already_ssjson=True)
    # pathway_tester.initial_data_upload(test_file_source_path, 'patientid', '|', populationid)
    # pathway_tester.single_subject_execute_and_save('47rj-aqm65f.1.0')
    # pathway_tester.execution_clean_up(populationid, onbehalfid)