import json

def cds_hooks(file_path):
        with open(file_path) as f:
            carePlan = json.load(f)

        # Start building the cards and orders collections
        # (idea is to stay consistent with CDS-Hooks implementation as much as possible)
        IC = {'cards': [], 'orders': []}

        # Look through the activity array
        for activity in carePlan['activity']:

            # Find the contained resource associated with the activity
            ref = activity['reference'][1:]
            for requestGroup in carePlan['contained']:
                if requestGroup['id'] == ref:

                    # ===============================

                    # Set up basic card
                    card = {
                        'indicator': 'info',
                        'links': [{'label': 'Apervita', 'url': 'https://www.apervita.com', 'type': 'absolute'}],
                        'suggestions': []
                    }

                    # Add source from RequestGroup definition if available
                    if 'definition' in requestGroup:
                        source = {
                            'label': requestGroup['definition']['display'],
                            'url': requestGroup['definition']['reference']
                        }
                        card['source'] = source

                    # Render CommunicationRequest to card, plus any other requests as suggestions
                    # NOTE: expectation is that any RequestGroup will have exactly one CommunicationRequest
                    for actionRequest in requestGroup['contained']:
                        if (actionRequest['resourceType'] == "CommunicationRequest"):
                            payload = actionRequest['payload']
                            card['summary'] = payload[0]['contentString']
                            card['detail'] = payload[1]['contentString']
                        else:
                            suggestion = {
                                'label': 'Submit ' + actionRequest['resourceType'].replace('Request', ' Request'),
                                'create': actionRequest['text']['div']
                            }
                            card['suggestions'].append(suggestion)

                    # Add to the cards collection
                    IC['cards'].append(card)

                    # Move along to the next activity entry
                    break

        # Render cards using template
        return carePlan, IC


care_plan_a, ic_a = cds_hooks('patient_a_care_plan.json')
care_plan_b, ic_b = cds_hooks('patient_b_care_plan.json')

print('Care Plan A: ' + json.dumps(ic_a) + '\n')
print('Care Plan B: ' + json.dumps(ic_b) + '\n')