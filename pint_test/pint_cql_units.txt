# Units need for CQL as of 6/20/2018
# Additional Time Definitions
wk = week
a = year
mo = month
d = day

# Percent Definition
abstract_value = [abstract_value]
percent = abstract_value/100

# Pressure Definition
mm[Hg] = mmHg

# Area Density
km2 = kilometer**2
m2 = meter**2

# Per Volume
per_volume = [per_volume] = [abstract_value]/[volume]
per_l = abstract_value/l
per_mm3 = abstract_value/(l/1000000)

# Beats per minute
beats = [beats] = Beats
bpm = [bpm] = [beats]/[time]
bpm = {Beats}/min