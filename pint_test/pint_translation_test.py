from pint import UnitRegistry

def load_file():
    # Initialize unit registry
    ureg = UnitRegistry()
    Q_ = ureg.Quantity

    # Load file into unit registry
    ureg.load_definitions('pint_test.txt')

    return Q_

def run_test(Q_):
    # Define abstract value test
    abstract_quantity_1 = Q_(1.54973129, 'abstract_value')
    abstract_quantity_2 = Q_(154.973129, 'abstract_percent')

    # Define pascal test values
    pascal_quantity_1 = Q_(10000000.8291, 'Pa')
    pascal_quantity_2 = Q_(10000.0008291, 'kPa')

    # Define liter to microliter test
    liter_quantity_1 = Q_(2839.28, 'L')
    liter_quantity_2 = Q_(2839280000.0, 'uL')

    # Define kilograms per liter to grams per milliliter test
    kilograms_per_liter_quantity_1 = Q_(1.5823, 'g_p_ml')
    kilograms_per_liter_quantity_2 = Q_(0.0015823, 'kg_p_ml')

    # Output
    print('-' * 150)
    print('Abstract Value to Abstract Percent Test: {0} --> {1}'.format(abstract_quantity_1, abstract_quantity_1.to('abstract_percent')))
    print('Abstract Percent to Abstract Value: {0} --> {1}'.format(abstract_quantity_2, abstract_quantity_2.to('abstract_value')))
    print('Pascal to Kilopascal Test: {0} --> {1}'.format(pascal_quantity_1, pascal_quantity_1.to('kPa')))
    print('Kilopascal to Pascal Test: {0} --> {1}'.format(pascal_quantity_2, pascal_quantity_2.to('Pa')))
    print('Liters to Microliters Test: {0} --> {1}'.format(liter_quantity_1, liter_quantity_1.to('uL')))
    print('Microliters to Liters Test: {0} --> {1}'.format(liter_quantity_2, liter_quantity_2.to('L')))
    print('Kilograms per Liter to Grams per Milliliter Test: {0} --> {1}'.format(kilograms_per_liter_quantity_1, kilograms_per_liter_quantity_1.to('kg_p_ml')))
    print('Grams per Liter to Kilograms per Milliliter Test: {0} --> {1}'.format(kilograms_per_liter_quantity_2, kilograms_per_liter_quantity_2.to('g_p_ml')))
    print('-' * 150)

if __name__ == '__main__':
    registry_load = load_file()
    run_test(registry_load)