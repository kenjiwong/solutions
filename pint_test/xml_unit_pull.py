'''
Author: Kenji Wong
Date: 6/20/2018
Objective: Unzip all files, unzip, and pull xml. From there, iterate through xml and pull units and write them to a CSV
'''


import os
import shutil
import re

source_path = '/Users/kenji.wong/PycharmProjects/solutions/pint_test/cql_download'
target_path = '/Users/kenji.wong/PycharmProjects/solutions/pint_test/cql_extract'
dashes = '-' * 150

def pull_xml_files(source_path, target_path):
    print('-' * 75 + 'File Walk Start' + '-' * 75 + '\n')

    for subdir, dirs, files in os.walk(source_path):
        for file in files:
            file_path = os.path.join(subdir, file)
            # print(file_path)
            if file_path.endswith('.xml'):
                shutil.copy(file_path, target_path)

    print('-' * 75 + 'File Walk Finish' + '-' * 75 + '\n')

def pull_value(source_path, value):
    print('-' * 75 + 'Pull Value Start' + '-' * 75 + '\n')

    units_file = open('/Users/kenji.wong/PycharmProjects/solutions/pint_test/output.txt', 'w')

    file_directory = os.listdir(source_path)

    for file in file_directory:
        new_file_path = os.path.join(source_path, file)
        f = open(new_file_path, 'r')
        try:
            for line in f:
                pattern = re.compile(value)
                results = pattern.findall(line)
                if results:
                    for result in results:
                        units_file.write(str(result) + '\n')
        except:
            raise Exception('This file failed {}'.format(new_file_path))

    print('Number of XML Files: {}'.format(len(file_directory)))

    units_file.close()

    print('-' * 75 + 'Pull Value Finish' + '-' * 75 + '\n')

def remove_duplicates(output_file):
    print('-' * 75 + 'Remove Duplicates Start' + '-' * 75 + '\n')

    f = open(output_file, 'r')
    output_f = open('unit_output.txt', 'w')
    unit_set = set()

    for unit in f:
        unit_set.add(unit)

    output_f.write('Units\n')

    for unit in unit_set:
        print('Units Found: {}'.format(unit.strip()))
        output_f.write(unit)

    os.remove('output.txt')

    output_f.close()

    print('-' * 75 + 'Remove Duplicates Finish' + '-' * 75 + '\n')

if __name__ == '__main__':
    print(dashes)
    pull_xml_files(source_path, target_path)
    pull_value(target_path, 'unit=".*?"')
    remove_duplicates('output.txt')
