library HearingScreeningPriorToHospitalDischarge version '7.0.002'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global

codesystem "SNOMEDCT:2017-03": 'urn:oid:2.16.840.1.113883.6.96' version 'urn:hl7:version:2017-03'

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Encounter Inpatient": 'urn:oid:2.16.840.1.113883.3.666.5.307'
valueset "Live Birth Newborn Born in Hospital": 'urn:oid:2.16.840.1.113762.1.4.1046.6'
valueset "Medical Reasons": 'urn:oid:2.16.840.1.114222.4.1.214079.1.1.7'
valueset "Newborn Hearing Screen Left": 'urn:oid:2.16.840.1.114222.4.1.214079.1.1.3'
valueset "Newborn Hearing Screen Right": 'urn:oid:2.16.840.1.114222.4.1.214079.1.1.4'
valueset "Pass Or Refer": 'urn:oid:2.16.840.1.114222.4.1.214079.1.1.6'

code "Patient deceased during stay (discharge status = dead) (finding)": '371828006' from "SNOMEDCT:2017-03" display 'Patient deceased during stay (discharge status = dead) (finding)'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Inpatient Encounters":
	["Encounter, Performed": "Encounter Inpatient"] Encounter
		where Global."LengthInDays"(Encounter.relevantPeriod)<= 120
			and Encounter.relevantPeriod ends during "Measurement Period"

define "Left Hearing Screen Performed":
	["Diagnostic Study, Performed": "Newborn Hearing Screen Left"] LeftScreen
		where LeftScreen.result as Code in "Pass Or Refer"
			and LeftScreen.result is not null

define "Right Hearing Screen Performed":
	["Diagnostic Study, Performed": "Newborn Hearing Screen Right"] RightScreen
		where RightScreen.result as Code in "Pass Or Refer"
			and RightScreen.result is not null

define "Diagnosis Live Birth Newborn Born in Hospital":
	"Inpatient Encounters" InpatientEncounter
		with ["Diagnosis": "Live Birth Newborn Born in Hospital"] QualifyingDiagnosis
			such that QualifyingDiagnosis.prevalencePeriod starts during InpatientEncounter.relevantPeriod

define "Encounter with Live Birth Diagnosis":
	"Inpatient Encounters" InpatientEncounter
		where exists InpatientEncounter.diagnoses EncounterDiagnosis
			where EncounterDiagnosis in "Live Birth Newborn Born in Hospital"

define "Numerator":
	"Has Complete Hearing Screening"
		union "Hearing Screen Not Done Due to Medical Reasons"

define "Initial Population":
	"Encounter with Live Birth Diagnosis"
		union "Diagnosis Live Birth Newborn Born in Hospital"

define "Denominator":
	"Initial Population"

define "Denominator Exclusions":
	"Initial Population" LiveBirthEncounter
		where LiveBirthEncounter.dischargeDisposition ~ "Patient deceased during stay (discharge status = dead) (finding)"
			and not exists "Has Complete Hearing Screening"

define "Has Complete Hearing Screening":
	"Inpatient Encounters" InpatientEncounter
		with "Left Hearing Screen Performed" LeftHearingScreen
			such that LeftHearingScreen.authorDatetime during InpatientEncounter.relevantPeriod
				and exists "Right Hearing Screen Performed" RightHearingScreen
					where RightHearingScreen.authorDatetime during InpatientEncounter.relevantPeriod

define "Hearing Screen Not Done Due to Medical Reasons":
	"Initial Population" LiveBirthEncounter
		with ( ["Diagnostic Study, Not Performed": "Newborn Hearing Screen Left"]
			union ["Diagnostic Study, Not Performed": "Newborn Hearing Screen Right"] ) ScreenNotDone
			such that ScreenNotDone.negationRationale in "Medical Reasons"
				and ScreenNotDone.authorDatetime during LiveBirthEncounter.relevantPeriod
