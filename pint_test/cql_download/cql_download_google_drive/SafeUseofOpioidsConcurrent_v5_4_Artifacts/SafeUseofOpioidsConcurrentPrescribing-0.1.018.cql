library SafeUseofOpioidsConcurrentPrescribing version '0.1.018'

using QDM version '5.3'

include MATGlobalCommonFunctions version '1.0.000' called Global

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Benzodiazepines": 'urn:oid:2.16.840.1.113762.1.4.1125.1'
valueset "Cancer": 'urn:oid:2.16.840.1.113883.3.526.3.1010'
valueset "Encounter ED and Observation Stay": 'urn:oid:2.16.840.1.113883.3.3157.1002.81'
valueset "Encounter Inpatient": 'urn:oid:2.16.840.1.113883.3.666.5.307'
valueset "Palliative care": 'urn:oid:2.16.840.1.113762.1.4.1125.3'
valueset "Schedule II and Schedule III Opioids": 'urn:oid:2.16.840.1.113762.1.4.1125.2'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Has an Active Cancer Diagnosis":
	"Hospital Based Encounter" Encounter
		with ["Diagnosis": "Cancer"] Cancer
			such that Cancer.prevalencePeriod overlaps Encounter.relevantPeriod

define "Has Palliative Care Order":
	"Hospital Based Encounter" Encounter
		with ["Intervention, Order": "Palliative care"] OrderedPalliative
			such that OrderedPalliative.authorDatetime during Encounter.relevantPeriod

define "Has Palliative Care Performed":
	"Hospital Based Encounter" Encounter
		with ["Intervention, Performed": "Palliative care"] PerformedPalliative
			such that PerformedPalliative.relevantPeriod overlaps Encounter.relevantPeriod
				or PerformedPalliative.relevantPeriod starts during Encounter.relevantPeriod

define "Hospital Based Encounter with Age Greater than or Equal to 18":
	"Hospital Based Encounter" Encounter
		with ["Patient Characteristic Birthdate"] BirthDate
			such that Global.CalendarAgeInYearsAt(BirthDate.birthDatetime, start of Encounter.relevantPeriod)>= 18

define "Emergency Department and Observation Encounters":
	["Encounter, Performed": "Encounter ED and Observation Stay"] EDorObservationEncounter
		where EDorObservationEncounter.relevantPeriod during "Measurement Period"

define "Hospital Based Encounter":
	"Inpatient Encounters"
		union "Emergency Department and Observation Encounters"

define "Encounters with Opiates and Benzodiazepines":
	"Hospital Based Encounter with Age Greater than or Equal to 18" Encounter
		return Tuple {
			encounter: Encounter,
			opioidsAtDischarge: ( ["Medication, Discharge": "Schedule II and Schedule III Opioids"] OpioidsDischarge
					where OpioidsDischarge.authorDatetime during Encounter.relevantPeriod
			),
			benzodiazepineAtDischarge: ( ["Medication, Discharge": "Benzodiazepines"] BenzodiazepineDischarge
					where BenzodiazepineDischarge.authorDatetime during Encounter.relevantPeriod
			),
			opioidsActive: ( ["Medication, Active": "Schedule II and Schedule III Opioids"] OpioidsActive
					where OpioidsActive.relevantPeriod starts before start Encounter.relevantPeriod
						and OpioidsActive.relevantPeriod overlaps after Encounter.relevantPeriod
			),
			benzodiazepineActive: ( ["Medication, Active": "Benzodiazepines"] BenzodiazepineActive
					where BenzodiazepineActive.relevantPeriod starts before start Encounter.relevantPeriod
						and BenzodiazepineActive.relevantPeriod overlaps after Encounter.relevantPeriod
			)
		}

define "Inpatient Encounters":
	["Encounter, Performed": "Encounter Inpatient"] InpatientEncounter
		where InpatientEncounter.lengthOfStay <= 120 days
			and InpatientEncounter.relevantPeriod ends during "Measurement Period"

define "Numerator":
	"Encounters with Opiates and Benzodiazepines" EncounterWithMeds
		where ( Count(EncounterWithMeds.opioidsAtDischarge
					union EncounterWithMeds.opioidsActive
			)>= 2
		)
			or ( exists ( EncounterWithMeds.opioidsAtDischarge
						union EncounterWithMeds.opioidsActive
				)
					and exists ( EncounterWithMeds.benzodiazepineAtDischarge
							union EncounterWithMeds.benzodiazepineActive
					)
			)
		return EncounterWithMeds.encounter

define "Denominator":
	"Initial Population"

define "Denominator Exclusion":
	"Has Palliative Care Order"
		union "Has Palliative Care Performed"
		union "Has an Active Cancer Diagnosis"

define "Encounter Discharges with Opioids":
	["Medication, Discharge": "Schedule II and Schedule III Opioids"] OpioidsDischarge
		with "Hospital Based Encounter with Age Greater than or Equal to 18" Encounter
			such that OpioidsDischarge.authorDatetime during Encounter.relevantPeriod

define "Encounter Discharges with Benzodiazepine":
	["Medication, Discharge": "Benzodiazepines"] BenzodiazepineDischarge
		with "Hospital Based Encounter with Age Greater than or Equal to 18" Encounter
			such that BenzodiazepineDischarge.authorDatetime during Encounter.relevantPeriod

define "Encounters with Active Opioids":
	["Medication, Active": "Schedule II and Schedule III Opioids"] OpioidsActive
		with "Hospital Based Encounter with Age Greater than or Equal to 18" Encounter
			such that OpioidsActive.relevantPeriod starts before start Encounter.relevantPeriod
				and OpioidsActive.relevantPeriod overlaps after Encounter.relevantPeriod

define "Encounters with Active Benzodiazepine":
	["Medication, Active": "Benzodiazepines"] BenzodiazepineActive
		with "Hospital Based Encounter with Age Greater than or Equal to 18" Encounter
			such that BenzodiazepineActive.relevantPeriod starts before start Encounter.relevantPeriod
				and BenzodiazepineActive.relevantPeriod overlaps after Encounter.relevantPeriod

define "Initial Population":
	"Encounters with Opiates and Benzodiazepines" EncounterWithMeds
		where exists ( EncounterWithMeds.opioidsAtDischarge
				intersect "Encounter Discharges with Opioids"
		)
			or exists ( EncounterWithMeds.benzodiazepineAtDischarge
					intersect "Encounter Discharges with Benzodiazepine"
			)
			or exists ( EncounterWithMeds.opioidsActive
					intersect "Encounters with Active Opioids"
			)
			or exists ( EncounterWithMeds.benzodiazepineActive
					intersect "Encounters with Active Benzodiazepine"
			)
		return EncounterWithMeds.encounter
