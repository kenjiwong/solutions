library DepressionRemissionatTwelveMonths version '6.2.013'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global

codesystem "LOINC:2.63": 'urn:oid:2.16.840.1.113883.6.1' version 'urn:hl7:version:2.63'

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Bipolar Disorder": 'urn:oid:2.16.840.1.113883.3.67.1.101.1.128'
valueset "Care Services in Long-Term Residential Facility": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1014'
valueset "Dysthymia": 'urn:oid:2.16.840.1.113883.3.67.1.101.1.254'
valueset "Major Depression Including Remission": 'urn:oid:2.16.840.113883.3.67.1.101.3.2444'
valueset "Palliative Care": 'urn:oid:2.16.840.1.113883.3.600.1.1579'
valueset "Personality Disorder": 'urn:oid:2.16.840.1.113883.3.67.1.101.1.246'
valueset "Palliative care encounter": 'urn:oid:2.16.840.1.113883.3.600.1.1575'
valueset "Contact or Office Visit": 'urn:oid:2.16.840.1.113762.1.4.1080.5'
valueset "Schizophrenia or Psychotic Disorder": 'urn:oid:2.16.840.1.113883.3.464.1003.105.12.1104'
valueset "Pervasive Developmental Disorder": 'urn:oid:2.16.840.1.113883.3.464.1003.105.12.1152'

code "Patient Health Questionnaire 9 item (PHQ-9) total score [Reported]": '44261-6' from "LOINC:2.63" display 'Patient Health Questionnaire 9 item (PHQ-9) total score [Reported]'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Denominator":
	"Initial Population"

define "Denominator Exclusions":
	exists ( "Palliative Care Order" )
		or exists ( "Encounter Palliative Care" )
		or exists ( "Long Term Care" )
		or exists ( "Expired" )
		or exists ( "Disorder Diagnoses" )

define "Denominator Identification Period":
	Interval[start of "Measurement Period" - 14 months, start of "Measurement Period" - 2 months )

define "Depression Diagnoses":
	["Diagnosis": "Major Depression Including Remission"]
		union ["Diagnosis": "Dysthymia"]

define "Depression Encounter":
	["Encounter, Performed": "Contact or Office Visit"] DepressionEncounter
		with "Depression Diagnoses" Depression
			such that DepressionEncounter.relevantPeriod overlaps Depression.prevalencePeriod
				and DepressionEncounter.relevantPeriod ends during "Denominator Identification Period"

define "Initial Population":
	exists ( ["Patient Characteristic Birthdate"] Birthdate
			with "Index Depression Assessment" IndexAssessment
				such that Global."CalendarAgeInYearsAt"(Birthdate.birthDatetime, IndexAssessment.authorDatetime)>= 12
	)

define "Stratification 1":
	["Patient Characteristic Birthdate"] Birthdate
		with "Index Depression Assessment" IndexAssessment
			such that Global."CalendarAgeInYearsAt"(Birthdate.birthDatetime, IndexAssessment.authorDatetime)>= 12
				and Global."CalendarAgeInYearsAt"(Birthdate.birthDatetime, IndexAssessment.authorDatetime)< 18

define "Stratification 2":
	["Patient Characteristic Birthdate"] Birthdate
		with "Index Depression Assessment" IndexAssessment
			such that Global."CalendarAgeInYearsAt"(Birthdate.birthDatetime, IndexAssessment.authorDatetime)>= 18

define "Index Depression Assessment":
	( First(["Assessment, Performed": "Patient Health Questionnaire 9 item (PHQ-9) total score [Reported]"] DepressionAssessment
				with "Depression Encounter" DepressionEncounter
					such that DepressionAssessment.authorDatetime during DepressionEncounter.relevantPeriod
				where DepressionAssessment.result > 9
					and DepressionAssessment.authorDatetime is not null
				sort by authorDatetime
		)
	)

define "Follow-Up Assessment Period":
	"Index Depression Assessment" FirstIndexAssessment
		return Interval[FirstIndexAssessment.authorDatetime + 12 months - 60 days, FirstIndexAssessment.authorDatetime + 12 months + 60 days]

define "Numerator":
	Last(["Assessment, Performed": "Patient Health Questionnaire 9 item (PHQ-9) total score [Reported]"] DepressionAssessment
			where DepressionAssessment.authorDatetime in "Follow-Up Assessment Period"
			sort by authorDatetime
	).result < 5

define "Expired":
	["Patient Characteristic Expired"] Deceased
		where Deceased.expiredDatetime occurs before end of "Follow-Up Assessment Period"

define "Palliative Care Order":
	["Intervention, Order": "Palliative Care"] PalliativeCare
		where PalliativeCare.authorDatetime occurs before end of "Follow-Up Assessment Period"

define "Disorder Diagnoses":
	( ["Diagnosis": "Bipolar Disorder"]
		union ["Diagnosis": "Personality Disorder"]
		union ["Diagnosis": "Schizophrenia or Psychotic Disorder"]
		union ["Diagnosis": "Pervasive Developmental Disorder"] ) DisorderDiagnoses
		where DisorderDiagnoses.prevalencePeriod starts before end of "Follow-Up Assessment Period"

define "Encounter Palliative Care":
	["Encounter, Performed": "Palliative care encounter"] PalliativeCareEncounter
		where PalliativeCareEncounter.relevantPeriod starts before end of "Follow-Up Assessment Period"

define "Long Term Care":
	["Encounter, Performed": "Care Services in Long-Term Residential Facility"] EncounterLongTermCare
		where EncounterLongTermCare.relevantPeriod starts before end of "Follow-Up Assessment Period"
