library InternationalProstateSymptomScoreIPSSorAmericanUrologicalAssociationSymptomIndexAUASIchange612monthsafterdiagnosisofBenignProstaticHyperplasia version '1.0.001'

using QDM version '5.3'

codesystem "AdministrativeGender:HL7V3.0_2017-07": 'urn:oid:2.16.840.1.113883.5.1' version 'urn:hl7:version:HL7V3.0_2017-07'

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "American Urological Association Symptom Index (AUASI)": 'urn:oid:2.16.840.1.113762.1.4.1164.55'
valueset "Benign prostatic hyperplasia with lower urinary tract symptoms": 'urn:oid:2.16.840.1.113762.1.4.1164.49'
valueset "Diabetes Mellitus, poorly controlled": 'urn:oid:2.16.840.1.113762.1.4.1164.46'
valueset "Hospital Services for urology care": 'urn:oid:2.16.840.1.113762.1.4.1164.64'
valueset "International Prostate Symptom Score (IPSS)": 'urn:oid:2.16.840.1.113762.1.4.1164.54'
valueset "Obesity": 'urn:oid:2.16.840.1.113762.1.4.1164.45'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Quality of Life Question for Urology Symptoms": 'urn:oid:2.16.840.1.113762.1.4.1164.56'
valueset "Urinary retention": 'urn:oid:2.16.840.1.113762.1.4.1164.52'

code "Male": 'M' from "AdministrativeGender:HL7V3.0_2017-07" display 'Male'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Qualifying Encounter":
	["Encounter, Performed": "Office Visit"] Encounter
		where Encounter.relevantPeriod during "Measurement Period"

define "AUA Symptom Index and Quality of Life Assessment":
	from
		["Assessment, Performed": "American Urological Association Symptom Index (AUASI)"] AUASI,
		["Assessment, Performed": "Quality of Life Question for Urology Symptoms"] QOL
		let AUASIresult: ( AUASI.result as Integer ) + ( QOL.result as Integer )
		where AUASI.authorDatetime same day as QOL.authorDatetime
			and AUASI.result is not null
			and QOL.result is not null
		return {
			authorDatetime: AUASI.authorDatetime,
			result: AUASIresult
		}

define "Urinary Symptom Score Improvement":
	from
		"First Urinary Symptom Score within 1 Month of Initial BPH Diagnosis" FirstUSS,
		"Second Urinary Symptom Score 6 to 12 Months After Initial BPH Diagnosis" SecondUSS
		let USSChange: FirstUSS.result - SecondUSS.result
		return USSChange

define "Denominator":
	"Initial Population"

define "Denominator Exclusions":
	exists ( "Urinary Retention within 365 Days of Initial BPH Diagnosis" )
		or exists ( "Hospitalization within 30 Days of Initial BPH Diagnosis" )

define "Documented IPSS":
	["Assessment, Performed": "International Prostate Symptom Score (IPSS)"] IPSSDone
		where IPSSDone.result is not null
		return {
			authorDatetime: ( IPSSDone.authorDatetime ),
			result: ( IPSSDone.result as Integer )
		}

define "Urinary Symptom Score":
	"Documented IPSS"
		union "AUA Symptom Index and Quality of Life Assessment"

define "Risk Adjustment for Poorly Controlled Diabetes Mellitus During Measurement Period":
	["Diagnosis": "Diabetes Mellitus, poorly controlled"] DM
		where DM.prevalencePeriod overlaps "Measurement Period"

define "Risk Adjustment for Obesity During Measurement Period":
	["Diagnosis": "Obesity"] Obese
		where Obese.prevalencePeriod overlaps "Measurement Period"

define "Patient is Male":
	["Patient Characteristic Sex": "Male"]

define "Initial BPH Diagnosis Within 6 Months Before or After Start of Measurement Period":
	["Diagnosis": "Benign prostatic hyperplasia with lower urinary tract symptoms"] NewBPH
		where NewBPH.prevalencePeriod starts within 6 months of start of "Measurement Period"
			or NewBPH.prevalencePeriod starts 6 months or less on or after start of "Measurement Period"

define "Hospitalization within 30 Days of Initial BPH Diagnosis":
	"Initial BPH Diagnosis Within 6 Months Before or After Start of Measurement Period" InitialBPH
		with ["Encounter, Performed": "Hospital Services for urology care"] Hospital
			such that InitialBPH.prevalencePeriod starts 30 days or less on or after end of Hospital.relevantPeriod
				and Hospital.relevantPeriod during "Measurement Period"

define "Urinary Retention within 365 Days of Initial BPH Diagnosis":
	["Diagnosis": "Urinary retention"] UrinaryRetention
		with "Initial BPH Diagnosis Within 6 Months Before or After Start of Measurement Period" NewBPH
			such that UrinaryRetention.prevalencePeriod starts 365 days or less on or before day of start of NewBPH.prevalencePeriod

define "First Urinary Symptom Score within 1 Month of Initial BPH Diagnosis":
	First("Urinary Symptom Score" InitialUSS
			with "Initial BPH Diagnosis Within 6 Months Before or After Start of Measurement Period" InitialBPH
				such that InitialUSS.authorDatetime 1 month or less on or after day of start of InitialBPH.prevalencePeriod
			sort by authorDatetime
	)

define "Initial Population":
	exists ( "Patient is Male" )
		and exists ( "Qualifying Encounter" )
		and exists ( "Initial BPH Diagnosis Within 6 Months Before or After Start of Measurement Period" )
		and "First Urinary Symptom Score within 1 Month of Initial BPH Diagnosis" is not null
		and "Second Urinary Symptom Score 6 to 12 Months After Initial BPH Diagnosis" is not null

define "Change in Urinary Symptom Score":
	( "Urinary Symptom Score Improvement" USSChange
			where USSChange >= 3
	) is not null

define "Numerator":
	"Change in Urinary Symptom Score"

define "Second Urinary Symptom Score 6 to 12 Months After Initial BPH Diagnosis":
	First("Urinary Symptom Score" InitialUSS
			with "Initial BPH Diagnosis Within 6 Months Before or After Start of Measurement Period" NewBPH
				such that months between start of NewBPH.prevalencePeriod and InitialUSS.authorDatetime in Interval[6, 12]
			sort by authorDatetime
	)
