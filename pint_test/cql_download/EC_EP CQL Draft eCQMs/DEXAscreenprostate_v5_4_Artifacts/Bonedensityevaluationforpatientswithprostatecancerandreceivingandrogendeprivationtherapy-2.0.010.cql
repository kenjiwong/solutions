library Bonedensityevaluationforpatientswithprostatecancerandreceivingandrogendeprivationtherapy version '2.0.010'

using QDM version '5.3'

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "DEXA Dual Energy Xray Absorptiometry, Bone Density for Urology Care": 'urn:oid:2.16.840.1.113762.1.4.1151.38'
valueset "Injection Leuprolide Acetate": 'urn:oid:2.16.840.1.113762.1.4.1151.16'
valueset "Office Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1001'
valueset "Prostate Cancer": 'urn:oid:2.16.840.1.113883.3.526.3.319'
valueset "Male": 'urn:oid:2.16.840.1.113883.3.560.100.1'
valueset "Patient Reason refused": 'urn:oid:2.16.840.1.113883.3.600.791'
valueset "Androgen deprivation therapy for Urology Care": 'urn:oid:2.16.840.1.113762.1.4.1151.48'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Patient is Male":
	exists ["Patient Characteristic Sex": "Male"]

define "Qualifying Encounter":
	["Encounter, Performed": "Office Visit"] Encounter
		where Encounter.relevantPeriod during "Measurement Period"

define "Initial Population":
	"Patient is Male"
		and exists "Qualifying Encounter"
		and "First Androgen Deprivation Therapy" is not null

define "Denominator":
	"Initial Population"

define "Has Bone Density Scan Ordered or Performed":
	["Diagnostic Study, Order": "DEXA Dual Energy Xray Absorptiometry, Bone Density for Urology Care"] DEXAOrdered
		union ( ["Diagnostic Study, Performed": "DEXA Dual Energy Xray Absorptiometry, Bone Density for Urology Care"] DEXAPerformed
				return "Diagnostic Study, Order" { authorDatetime: start of DEXAPerformed.relevantPeriod }
		)

define "Numerator":
	exists "Baseline DEXA Scan Two Years Prior to the Start of or Less than Three Months After the Start of ADT"

define "Denominator Exception":
	exists "No Bone Density Scan Ordered Due to Patient Refusal"

define "No Bone Density Scan Ordered Due to Patient Refusal":
	( ["Diagnostic Study, Not Ordered": "DEXA Dual Energy Xray Absorptiometry, Bone Density for Urology Care"] DEXANotOrdered
			where DEXANotOrdered.negationRationale in "Patient Reason refused"
	)
		union ( ["Diagnostic Study, Not Performed": "DEXA Dual Energy Xray Absorptiometry, Bone Density for Urology Care"] DEXANotPerformed
				where DEXANotPerformed.negationRationale in "Patient Reason refused"
		)

define "Prostate Cancer Diagnosis":
	["Diagnosis": "Prostate Cancer"] ProstateCancer
		where ProstateCancer.prevalencePeriod starts same day or before end "Measurement Period"

define "First Androgen Deprivation Therapy":
	First(["Medication, Active": "Androgen deprivation therapy for Urology Care"] InitialADTTherapy
			with "Prostate Cancer Diagnosis" ProstateCancer
				such that InitialADTTherapy.relevantPeriod starts on or after start of ProstateCancer.prevalencePeriod
			with ["Procedure, Order": "Injection Leuprolide Acetate"] TwelveMonthADTTherapy
				such that InitialADTTherapy.relevantPeriod includes TwelveMonthADTTherapy.authorDatetime
			sort by start of relevantPeriod
	)

define "Baseline DEXA Scan Two Years Prior to the Start of or Less than Three Months After the Start of ADT":
	"Has Bone Density Scan Ordered or Performed" DEXAscan
		with "First Androgen Deprivation Therapy" FirstADT
			such that DEXAscan.authorDatetime 3 months or less after start FirstADT.relevantPeriod
				or DEXAscan.authorDatetime 2 years or less before start of FirstADT.relevantPeriod
