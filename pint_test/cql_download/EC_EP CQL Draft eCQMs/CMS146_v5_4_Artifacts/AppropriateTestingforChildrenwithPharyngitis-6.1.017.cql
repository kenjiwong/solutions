library AppropriateTestingforChildrenwithPharyngitis version '6.1.017'

using QDM version '5.3'

include MATGlobalCommonFunctions version '2.0.000' called Global
include Hospice version '1.0.000' called Hospice

valueset "ONC Administrative Sex": 'urn:oid:2.16.840.1.113762.1.4.1'
valueset "Race": 'urn:oid:2.16.840.1.114222.4.11.836'
valueset "Ethnicity": 'urn:oid:2.16.840.1.114222.4.11.837'
valueset "Payer": 'urn:oid:2.16.840.1.114222.4.11.3591'
valueset "Acute Pharyngitis": 'urn:oid:2.16.840.1.113883.3.464.1003.102.12.1011'
valueset "Acute Tonsillitis": 'urn:oid:2.16.840.1.113883.3.464.1003.102.12.1012'
valueset "Antibiotic Medications for Pharyngitis": 'urn:oid:2.16.840.1.113883.3.464.1003.196.12.1001'
valueset "Group A Streptococcus Test": 'urn:oid:2.16.840.1.113883.3.464.1003.198.12.1012'
valueset "Ambulatory/ED Visit": 'urn:oid:2.16.840.1.113883.3.464.1003.101.12.1061'

parameter "Measurement Period" Interval<DateTime>

context Patient

define "SDE Ethnicity":
	["Patient Characteristic Ethnicity": "Ethnicity"]

define "SDE Payer":
	["Patient Characteristic Payer": "Payer"]

define "SDE Race":
	["Patient Characteristic Race": "Race"]

define "SDE Sex":
	["Patient Characteristic Sex": "ONC Administrative Sex"]

define "Denominator":
	"Initial Population"

define "Pharyngitis or Tonsillitis":
	( ["Diagnosis": "Acute Pharyngitis"]
			union ["Diagnosis": "Acute Tonsillitis"]
	)

define "In Hospice":
	"Initial Population" EligibleEncounters
		where Hospice."Has Hospice"

define "Denominator Exclusions":
	"In Hospice"
		union "Antibiotic Active In the 30 Days Prior"

define "Encounter With Pharyngitis or Tonsillitis":
	from
		"Encounter With Antibiotic Ordered Within Three Days" VisitWithAntibiotic,
		"Pharyngitis or Tonsillitis" AcutePharyngitisTonsillitis
		where AcutePharyngitisTonsillitis.prevalencePeriod starts during VisitWithAntibiotic.relevantPeriod

define "Group A Streptococcus Lab Test":
	["Laboratory Test, Performed": "Group A Streptococcus Test"] GroupAStreptococcus
		where GroupAStreptococcus.result is not null

define "Encounter With Antibiotic Ordered Within Three Days":
	["Encounter, Performed": "Ambulatory/ED Visit"] EDOrAmbulatoryVisit
		with ["Medication, Order": "Antibiotic Medications for Pharyngitis"] AntibioticOrdered
			such that ( EDOrAmbulatoryVisit.relevantPeriod starts 3 days or less on or before AntibioticOrdered.authorDatetime )
		where EDOrAmbulatoryVisit.relevantPeriod during "Measurement Period"

define "Numerator":
	from
		"Group A Streptococcus Lab Test" GroupAStreptococcusTest,
		["Encounter, Performed": "Ambulatory/ED Visit"] AmbulatoryEncounter,
		"Encounter With Pharyngitis or Tonsillitis" EncounterWithPharyngitis
		where ( start of GroupAStreptococcusTest.relevantPeriod 3 days or less on or before day of end of AmbulatoryEncounter.relevantPeriod
				or start of GroupAStreptococcusTest.relevantPeriod 3 days or less on or after day of end of AmbulatoryEncounter.relevantPeriod
		)
			and EncounterWithPharyngitis.VisitWithAntibiotic.relevantPeriod includes AmbulatoryEncounter.relevantPeriod
		return EncounterWithPharyngitis.VisitWithAntibiotic

define "Initial Population":
	"Encounter With Pharyngitis or Tonsillitis" EncounterWithPharyngitis
		with ["Patient Characteristic Birthdate"] BirthDate
			such that Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")>= 3
				and Global."CalendarAgeInYearsAt"(BirthDate.birthDatetime, start of "Measurement Period")< 18
		return EncounterWithPharyngitis.VisitWithAntibiotic

define "Antibiotic Active In the 30 Days Prior":
	"Encounter With Pharyngitis or Tonsillitis" EncounterWithPharyngitis
		with ["Medication, Active": "Antibiotic Medications for Pharyngitis"] ActiveAntibiotic
			such that ActiveAntibiotic.relevantPeriod overlaps Interval[Global."ToDate"((start of EncounterWithPharyngitis.AcutePharyngitisTonsillitis.prevalencePeriod)- 30 days), Global."ToDate"(start of EncounterWithPharyngitis.AcutePharyngitisTonsillitis.prevalencePeriod))
		return EncounterWithPharyngitis.VisitWithAntibiotic
