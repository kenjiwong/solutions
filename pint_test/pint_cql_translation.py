from pint import UnitRegistry

dashes = '-' * 150

class pint_test():

    def __init__(self):
        # Initialize unit registry
        ureg = UnitRegistry()
        Q_ = ureg.Quantity

        # Load file into unit registry
        ureg.load_definitions('pint_cql_units.txt')

        # Define quantity
        self.quantity = Q_

    def test_new_units(self):
        ##### Time #####
        # Time Test Value - Seconds
        time_test_value = 31536000.0

        # unit = "min"
        self.create_test(time_test_value, 's', 'min', 525600)
        # unit = "minutes"
        self.create_test(time_test_value, 's', 'minutes', 525600)
        # unit = "hour"
        self.create_test(time_test_value, 's', 'hour', 8760)
        # unit = "hours"
        self.create_test(time_test_value, 's', 'hours', 8760)
        # unit = "day"
        self.create_test(time_test_value, 's', 'day', 365)
        # unit = "days"
        self.create_test(time_test_value, 's', 'days', 365)
        # unit = "d"
        self.create_test(time_test_value, 's', 'd', 365)
        # unit = "wk"
        self.create_test(time_test_value, 's', 'wk', 52.1428571429)
        # unit = "weeks"
        self.create_test(time_test_value, 's', 'weeks', 52.1428571429)
        # unit = "month"
        self.create_test(time_test_value, 's', 'month', 12.1666666667)
        # unit = "months"
        self.create_test(time_test_value, 's', 'months', 12.1666666667)
        # unit = "mo"
        self.create_test(time_test_value, 's', 'mo', 12.1666666667)
        # unit = "year"
        self.create_test(time_test_value, 's', 'year', 1)
        # unit = "years"
        self.create_test(time_test_value, 's', 'years', 1)
        # unit = "a"
        self.create_test(time_test_value, 's', 'a', 1)

        ##### Abstract #####
        # Abstract Test Value - None
        abstract_test_value = 1.54

        # unit = "%"
        self.create_test(abstract_test_value, 'abstract_value', 'percent', 154)

        #### Density #####
        # Density Test Value - Grams/Milliliters
        density_test_value = 27.12

        # unit = "ng/mL"
        self.create_test(density_test_value, 'g/mL', 'ng/mL', 27120000)
        # unit = "mg/dL"
        self.create_test(density_test_value, 'g/mL', 'mg/dL', 2712000)

        ##### Pressure #####
        # Pressure Test Value - Pascal
        pressure_test_value = 281937.1

        # unit = "mm[Hg]"
        self.create_test(pressure_test_value, 'pascal', 'mm[Hg]', 2114.702205)
        # unit = "mmHg"
        self.create_test(pressure_test_value, 'pascal', 'mmHg', 2114.702205)

        ##### Area Density #####
        # Area Density Test Value = grams per squared kilometer
        area_density_test_value = 29831.123

        # unit = "kg/m2"
        self.create_test(area_density_test_value, 'g/km2', 'kg/m2', 0.00002983112)

        ##### Per Volume #####
        # Per Volume Test Value = 1/liters
        per_volume_test_value = 2

        # unit = "per mm3"
        self.create_test(per_volume_test_value, 'per_l', 'per_mm3', 0.000002)

        ##### Beats Per Minute #####
        # Beats per minute test value = beats per minute
        beats_per_minute_test_value = 132

        # unit = "bpm"
        self.create_test(beats_per_minute_test_value, 'bpm', 'bpm', 132)
        # unit = "{Beats}/min"
        self.create_test(beats_per_minute_test_value, 'bpm', '{Beats}/min', 132)
        # Additional test
        self.create_test(beats_per_minute_test_value, 'bpm', '{Beats}/sec', 2.2)

        ##### Broken #####
        # unit = "per mm3" - Reason: Just assigned to an abstract value at this point. Unable to create conversion
        # self.create_test(per_volume_test_value, 'per_volume', 'per_mm3', 0.1)
        # unit = "/mm3" - Reason: Does not like special character
        # self.create_test(per_volume_test_value, 'per_volume', '/mm3', 0.1)
        # unit = "h" - Reason: Planck's Constant
        # self.create_test(time_test_value, 's', 'h', 12)
        # unit = "%" - Reason: Does not like special character
        # self.create_test(abstract_test_value, 'abstract_value', '%', 154)
        # unit = "{H.B.}/min" - Reason: Assumes byte by henry by minute. Unsure where abbreviations are coming from
        # self.create_test(beats_per_minute_test_value, 'bpm', '{H.B.}/min', 132)

    def create_test(self, test_value, original_unit, new_unit, expected_value):
        quantity_1 = self.quantity(test_value, original_unit)
        quantity_2 = quantity_1.to(new_unit)

        print('{0} to {1}: {2} --> {3}'.format(original_unit, new_unit, quantity_1, quantity_1.to(new_unit)))
        print('{0} to {1}: {2} --> {3}'.format(new_unit, original_unit, quantity_2, quantity_2.to(original_unit)))
        print('Expected Value: {0} {1}'.format(expected_value, new_unit))
        print('{0} Test Completed \n'.format(new_unit))

if __name__ == '__main__':
    print(dashes)
    cql_test = pint_test()
    cql_test.test_new_units()