import xml.etree.cElementTree as ET
import os

config = {
    'CMS135a_duplications': 5,
    'CMS135a_duplications_starting_point': 10000,
    'CMS135b_duplications': 5,
    'CMS135b_duplications_starting_point': 20000,
}


CMS135a = ET.parse('95014.xml')
CMS135a_root = CMS135a.getroot()

CMS135b = ET.parse('95003.xml')
CMS135b_root = CMS135b.getroot()

for i in range(config['CMS135a_duplications']):
    base = CMS135a_root
    for child in base:
        if child.tag == '{urn:hl7-org:v3}id':
            child.attrib['extension'] = str(config['CMS135a_duplications_starting_point'])
            base_file_text = ET.tostring(base)
            file = open('CMS135a_duplicates/{}.xml'.format(config['CMS135a_duplications_starting_point']), "w")
            file.write(base_file_text)
            config['CMS135a_duplications_starting_point'] += 1



for i in range(config['CMS135b_duplications']):
    base = CMS135b_root
    for child in base:
        if child.tag == '{urn:hl7-org:v3}id':
            child.attrib['extension'] = str(config['CMS135b_duplications_starting_point'])
            base_file_text = ET.tostring(base)
            file = open('CMS135b_duplicates/{}.xml'.format(config['CMS135b_duplications_starting_point']), "w")
            file.write(base_file_text)
            config['CMS135b_duplications_starting_point'] += 1